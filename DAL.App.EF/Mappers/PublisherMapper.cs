using System;
using Contracts.DAL.Base.Mappers;

namespace DAL.App.EF.Mappers
{
    public class PublisherMapper : IBaseDALMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(DAL.App.DTO.Publisher))
            {
                return MapFromDomain((Domain.Publisher) inObject) as TOutObject;
            }
            
            if (typeof(TOutObject) == typeof(Domain.Publisher))
            {
                return MapFromDAL((DAL.App.DTO.Publisher) inObject) as TOutObject;
            }
            
            throw new InvalidCastException("No conversion!");
        }

        public static DAL.App.DTO.Publisher MapFromDomain(Domain.Publisher publisher)
        {
            if (publisher == null) return null;
            
            var res = new DAL.App.DTO.Publisher
            {
                Id = publisher.Id,
                PublisherName = publisher.PublisherName,
                //Games = publisher.Games
            };

            return res;
        }

        public static Domain.Publisher MapFromDAL(DAL.App.DTO.Publisher publisher)
        {
            if (publisher == null) return null;
            
            var res = new Domain.Publisher
            {
                Id = publisher.Id,
                PublisherName = publisher.PublisherName,
                //Games = publisher.Games
            };

            return res;
        }
    }
}