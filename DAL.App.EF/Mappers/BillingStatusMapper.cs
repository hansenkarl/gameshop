using System;
using Contracts.DAL.Base.Mappers;

namespace DAL.App.EF.Mappers
{
    public class BillingStatusMapper : IBaseDALMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(DAL.App.DTO.BillingStatus))
            {
                return MapFromDomain((Domain.BillingStatus) inObject) as TOutObject;
            }
            
            if (typeof(TOutObject) == typeof(Domain.BillingStatus))
            {
                return MapFromDAL((DAL.App.DTO.BillingStatus) inObject) as TOutObject;
            }
            
            throw new InvalidCastException("No conversion!");
        }

        public static DAL.App.DTO.BillingStatus MapFromDomain(Domain.BillingStatus billingStatus)
        {
            if (billingStatus == null) return null;
            
            var res = new DAL.App.DTO.BillingStatus
            {
                Id = billingStatus.Id,
                StatusName = billingStatus.StatusName,
            };
            
            return res;
        }

        public static Domain.BillingStatus MapFromDAL(DAL.App.DTO.BillingStatus billingStatus)
        {
            if (billingStatus == null) return null;
            
            var res = new Domain.BillingStatus
            {
                Id = billingStatus.Id,
                StatusName = billingStatus.StatusName,
            };
            
            return res;
        }
    }
}