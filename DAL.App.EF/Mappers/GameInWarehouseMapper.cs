using System;
using Contracts.DAL.Base.Mappers;

namespace DAL.App.EF.Mappers
{
    public class GameInWarehouseMapper : IBaseDALMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(DAL.App.DTO.GameInWarehouse))
            {
                return MapFromDomain((Domain.GameInWarehouse) inObject) as TOutObject;
            }
            
            if (typeof(TOutObject) == typeof(Domain.GameInWarehouse))
            {
                return MapFromDAL((DAL.App.DTO.GameInWarehouse) inObject) as TOutObject;
            }
            
            throw new InvalidCastException("No conversion!");
        }

        public static DAL.App.DTO.GameInWarehouse MapFromDomain(Domain.GameInWarehouse gameInWarehouse)
        {
            if (gameInWarehouse == null) return null;
            
            var res = new DAL.App.DTO.GameInWarehouse
            {
                Id = gameInWarehouse.Id,
                Amount = gameInWarehouse.Amount,
                Added = gameInWarehouse.Added,
                GameId = gameInWarehouse.GameId,
                Game = GameMapper.MapFromDomain(gameInWarehouse.Game),
                WarehouseId = gameInWarehouse.WarehouseId,
                Warehouse = WarehouseMapper.MapFromDomain(gameInWarehouse.Warehouse)
            };

            return res;
        }

        public static Domain.GameInWarehouse MapFromDAL(DAL.App.DTO.GameInWarehouse gameInWarehouse)
        {
            if (gameInWarehouse == null) return null;
            
            var res = new Domain.GameInWarehouse
            {
                Id = gameInWarehouse.Id,
                Amount = gameInWarehouse.Amount,
                Added = gameInWarehouse.Added,
                GameId = gameInWarehouse.GameId,
                Game = GameMapper.MapFromDAL(gameInWarehouse.Game),
                WarehouseId = gameInWarehouse.WarehouseId,
                Warehouse = WarehouseMapper.MapFromDAL(gameInWarehouse.Warehouse)
            };

            return res;
        }
    }
}