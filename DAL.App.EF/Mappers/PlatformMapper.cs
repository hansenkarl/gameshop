using System;
using Contracts.DAL.Base.Mappers;

namespace DAL.App.EF.Mappers
{
    public class PlatformMapper : IBaseDALMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(DAL.App.DTO.Platform))
            {
                return MapFromDomain((Domain.Platform) inObject) as TOutObject;
            }
            
            if (typeof(TOutObject) == typeof(Domain.Platform))
            {
                return MapFromDAL((DAL.App.DTO.Platform) inObject) as TOutObject;
            }
            
            throw new InvalidCastException("No conversion!");
        }

        public static DAL.App.DTO.Platform MapFromDomain(Domain.Platform platform)
        {
            if (platform == null) return null;
            
            var res = new DAL.App.DTO.Platform
            {
                Id = platform.Id,
                PlatformName = platform.PlatformName,
                /*Games = platform.Games,
                Reviews = platform.Reviews*/
            };

            return res;
        }

        public static Domain.Platform MapFromDAL(DAL.App.DTO.Platform platform)
        {
            if (platform == null) return null;
            
            var res = new Domain.Platform
            {
                Id = platform.Id,
                PlatformName = platform.PlatformName,
                /*Games = platform.Games,
                Reviews = platform.Reviews*/
            };

            return res;
        }
    }
}