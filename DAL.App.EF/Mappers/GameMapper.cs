using System;
using Contracts.DAL.Base.Mappers;

namespace DAL.App.EF.Mappers
{
    public class GameMapper : IBaseDALMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(DAL.App.DTO.Game))
            {
                return MapFromDomain((Domain.Game) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(Domain.Game))
            {
                return MapFromDAL((DAL.App.DTO.Game) inObject) as TOutObject;
            }

            throw new InvalidCastException("No conversion!");
        }

        public static DAL.App.DTO.Game MapFromDomain(Domain.Game game)
        {
            if (game == null) return null;
            
            var res = new DAL.App.DTO.Game
            {
                Id = game.Id,
                GameTitle = game.GameTitle,
                Price = game.Price,
                Pegi = game.Pegi,
                GenreId = game.GenreId,
                Genre = GenreMapper.MapFromDomain(game.Genre),
                PlatformId = game.PlatformId,
                Platform = PlatformMapper.MapFromDomain(game.Platform),
                PublisherId = game.PublisherId,
                Publisher = PublisherMapper.MapFromDomain(game.Publisher),
            };

            return res;
        }

        public static Domain.Game MapFromDAL(DAL.App.DTO.Game game)
        {
            if (game == null) return null;
            
            var res = new Domain.Game
            {
                Id = game.Id,
                GameTitle = game.GameTitle,
                Price = game.Price,
                Pegi = game.Pegi,
                GenreId = game.GenreId,
                Genre = GenreMapper.MapFromDAL(game.Genre),
                PlatformId = game.PlatformId,
                Platform = PlatformMapper.MapFromDAL(game.Platform),
                PublisherId = game.PublisherId,
                Publisher = PublisherMapper.MapFromDAL(game.Publisher),
            };

            return res;
        }
    }
}