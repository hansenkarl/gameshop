using System;
using Contracts.DAL.Base.Mappers;

namespace DAL.App.EF.Mappers
{
    public class ReviewMapper : IBaseDALMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(DAL.App.DTO.Review))
            {
                return MapFromDomain((Domain.Review) inObject) as TOutObject;
            }
            
            if (typeof(TOutObject) == typeof(Domain.Review))
            {
                return MapFromDAL((DAL.App.DTO.Review) inObject) as TOutObject;
            }
            
            throw new InvalidCastException("No conversion!");
        }

        public static DAL.App.DTO.Review MapFromDomain(Domain.Review review)
        {
            if (review == null) return null;
            
            var res = new DAL.App.DTO.Review
            {
                Id = review.Id,
                Rating = review.Rating,
                Comment = review.Comment,
                Added = review.Added,
                AppUserId = review.AppUserId,
                AppUser = AppUserMapper.MapFromDomain(review.AppUser),
                GameId = review.GameId,
                Game = GameMapper.MapFromDomain(review.Game),
                PlatformId = review.PlatformId,
                Platform = PlatformMapper.MapFromDomain(review.Platform)
            };

            return res;
        }

        public static Domain.Review MapFromDAL(DAL.App.DTO.Review review)
        {
            if (review == null) return null;
            
            var res = new Domain.Review
            {
                Id = review.Id,
                Rating = review.Rating,
                Comment = review.Comment,
                Added = review.Added,
                AppUserId = review.AppUserId,
                AppUser = AppUserMapper.MapFromDAL(review.AppUser),
                GameId = review.GameId,
                Game = GameMapper.MapFromDAL(review.Game),
                PlatformId = review.PlatformId,
                Platform = PlatformMapper.MapFromDAL(review.Platform)
            };

            return res;
        }
    }
}