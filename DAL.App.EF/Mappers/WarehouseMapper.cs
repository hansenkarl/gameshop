using System;
using Contracts.DAL.Base.Mappers;

namespace DAL.App.EF.Mappers
{
    public class WarehouseMapper : IBaseDALMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(DAL.App.DTO.Warehouse))
            {
                return MapFromDomain((Domain.Warehouse) inObject) as TOutObject;
            }
            
            if (typeof(TOutObject) == typeof(Domain.Warehouse))
            {
                return MapFromDAL((DAL.App.DTO.Warehouse) inObject) as TOutObject;
            }
            
            throw new InvalidCastException("No conversion!");
        }

        public static DAL.App.DTO.Warehouse MapFromDomain(Domain.Warehouse warehouse)
        {
            if (warehouse == null) return null;
            
            var res = new DAL.App.DTO.Warehouse
            {
                Id = warehouse.Id,
                WarehouseName = warehouse.WarehouseName,
            };

            return res;
        }

        public static Domain.Warehouse MapFromDAL(DAL.App.DTO.Warehouse warehouse)
        {
            if (warehouse == null) return null;
            
            var res = new Domain.Warehouse
            {
                Id = warehouse.Id,
                WarehouseName = warehouse.WarehouseName,
            };

            return res;
        }
    }
}