using System;
using Contracts.DAL.Base.Mappers;

namespace DAL.App.EF.Mappers
{
    public class CreditCardMapper : IBaseDALMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(DAL.App.DTO.CreditCard))
            {
                return MapFromDomain((Domain.CreditCard) inObject) as TOutObject;
            }
            
            if (typeof(TOutObject) == typeof(Domain.CreditCard))
            {
                return MapFromDAL((DAL.App.DTO.CreditCard) inObject) as TOutObject;
            }
            
            throw new InvalidCastException("No conversion!");
        }

        public static DAL.App.DTO.CreditCard MapFromDomain(Domain.CreditCard creditCard)
        {
            if (creditCard == null) return null;
            
            var res = new DAL.App.DTO.CreditCard
            {
                Id = creditCard.Id,
                CreditCardNr = creditCard.CreditCardNr,
                ValidThru = creditCard.ValidThru,
                AppUserId = creditCard.AppUserId,
                AppUser = AppUserMapper.MapFromDomain(creditCard.AppUser)
            };

            return res;
        }

        public static Domain.CreditCard MapFromDAL(DAL.App.DTO.CreditCard creditCard)
        {
            if (creditCard == null) return null;
            
            var res = new Domain.CreditCard
            {
                Id = creditCard.Id,
                CreditCardNr = creditCard.CreditCardNr,
                ValidThru = creditCard.ValidThru,
                AppUserId = creditCard.AppUserId,
                AppUser = AppUserMapper.MapFromDAL(creditCard.AppUser)
            };

            return res;
        }
    }
}