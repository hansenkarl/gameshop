using System;
using Contracts.DAL.Base.Mappers;

namespace DAL.App.EF.Mappers
{
    public class GenreMapper : IBaseDALMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(DAL.App.DTO.Genre))
            {
                return MapFromDomain((Domain.Genre) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(Domain.Genre))
            {
                return MapFromDAL((DAL.App.DTO.Genre) inObject) as TOutObject;
            }

            throw new InvalidCastException("No conversion!");
        }

        public static DAL.App.DTO.Genre MapFromDomain(Domain.Genre genre)
        {
            if (genre == null) return null;
            
            var res = new DAL.App.DTO.Genre
            {
                Id = genre.Id,
                GenreName = genre.GenreName,
                //Games = genre.Games
            };
            
            return res;
        }

        public static Domain.Genre MapFromDAL(DAL.App.DTO.Genre genre)
        {
            if (genre == null) return null;
            
            var res = new Domain.Genre
            {
                Id = genre.Id,
                GenreName = genre.GenreName,
                //Games = genre.Games
            };
            
            return res;
        }
    }
}