using System;
using System.Runtime.InteropServices;
using Contracts.DAL.Base.Mappers;

namespace DAL.App.EF.Mappers
{
    public class BillingMapper : IBaseDALMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(DAL.App.DTO.Billing))
            {
                return MapFromDomain((Domain.Billing) inObject) as TOutObject;
            }
            
            if (typeof(TOutObject) == typeof(Domain.Billing))
            {
                return MapFromDAL((DAL.App.DTO.Billing) inObject) as TOutObject;
            }
            
            throw new InvalidCastException("No conversion!");
        }

        public static DAL.App.DTO.Billing MapFromDomain(Domain.Billing billing, [Optional] DAL.App.DTO.Basket basket)
        {
            if (billing == null) return null;
            
            var res = new DAL.App.DTO.Billing
            {
                Id = billing.Id,
                SumWithoutTax = billing.SumWithoutTax,
                Tax = billing.Tax,
                SumTotal = billing.SumTotal,
                Created = billing.Created,
                BasketId = billing.BasketId,
                Basket = null,
                CreditCardId = billing.CreditCardId,
                CreditCard = CreditCardMapper.MapFromDomain(billing.CreditCard),
                BillingStatusId = billing.BillingStatusId,
                BillingStatus = BillingStatusMapper.MapFromDomain(billing.BillingStatus)
            };

            res.Basket = basket ?? BasketMapper.MapFromDomain(billing.Basket, res);

            return res;
        }

        public static Domain.Billing MapFromDAL(DAL.App.DTO.Billing billing, [Optional] Domain.Basket basket)
        {
            if (billing == null) return null;
            
            var res = new Domain.Billing
            {
                Id = billing.Id,
                SumWithoutTax = billing.SumWithoutTax,
                Tax = billing.Tax,
                SumTotal = billing.SumTotal,
                Created = billing.Created,
                BasketId = billing.BasketId,
                Basket = null,
                CreditCardId = billing.CreditCardId,
                CreditCard = CreditCardMapper.MapFromDAL(billing.CreditCard),
                BillingStatusId = billing.BillingStatusId,
                BillingStatus = BillingStatusMapper.MapFromDAL(billing.BillingStatus)
            };

            res.Basket = basket ?? BasketMapper.MapFromDAL(billing.Basket, res);

            return res;
        }
    }
}