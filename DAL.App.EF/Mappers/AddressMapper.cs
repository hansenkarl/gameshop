using System;
using Contracts.DAL.Base.Mappers;

namespace DAL.App.EF.Mappers
{
    public class AddressMapper : IBaseDALMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(DAL.App.DTO.Address))
            {
                return MapFromDomain((Domain.Address) inObject) as TOutObject;
            }
            
            if (typeof(TOutObject) == typeof(Domain.Address))
            {
                return MapFromDAL((DAL.App.DTO.Address) inObject) as TOutObject;
            }
            
            throw new InvalidCastException("No conversion!");
        }

        public static DAL.App.DTO.Address MapFromDomain(Domain.Address address)
        {
            var res = new DAL.App.DTO.Address
            {
                Id = address.Id,
                Country = address.Country,
                County = address.County,
                City = address.City,
                Street = address.Street,
                HouseNr = address.HouseNr,
                ZipCode = address.ZipCode,
                AppUserId = address.AppUserId,
                AppUser = AppUserMapper.MapFromDomain(address.AppUser)
            };

            return res;
        }

        public static Domain.Address MapFromDAL(DAL.App.DTO.Address address)
        {
            var res = new Domain.Address
            {
                Id = address.Id,
                Country = address.Country,
                County = address.County,
                City = address.City,
                Street = address.Street,
                HouseNr = address.HouseNr,
                ZipCode = address.ZipCode,
                AppUserId = address.AppUserId,
                AppUser = AppUserMapper.MapFromDAL(address.AppUser)
            };

            return res;
        }
    }
}