using System;
using Contracts.DAL.Base.Mappers;

namespace DAL.App.EF.Mappers
{
    public class GameInBasketMapper : IBaseDALMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(DAL.App.DTO.GameInBasket))
            {
                return MapFromDomain((Domain.GameInBasket) inObject) as TOutObject;
            }
            
            if (typeof(TOutObject) == typeof(Domain.GameInBasket))
            {
                return MapFromDAL((DAL.App.DTO.GameInBasket) inObject) as TOutObject;
            }
            
            throw new InvalidCastException("No conversion!");
        }

        public static DAL.App.DTO.GameInBasket MapFromDomain(Domain.GameInBasket gameInBasket)
        {
            if (gameInBasket == null) return null;
            
            var res = new DAL.App.DTO.GameInBasket
            {
                Id = gameInBasket.Id,
                Amount = gameInBasket.Amount,
                GameId = gameInBasket.GameId,
                Game = GameMapper.MapFromDomain(gameInBasket.Game),
                BasketId = gameInBasket.BasketId,
                Basket = BasketMapper.MapFromDomain(gameInBasket.Basket)
            };

            return res;
        }

        public static Domain.GameInBasket MapFromDAL(DAL.App.DTO.GameInBasket gameInBasket)
        {
            if (gameInBasket == null) return null;
            
            var res = new Domain.GameInBasket
            {
                Id = gameInBasket.Id,
                Amount = gameInBasket.Amount,
                GameId = gameInBasket.GameId,
                Game = GameMapper.MapFromDAL(gameInBasket.Game),
                BasketId = gameInBasket.BasketId,
                Basket = BasketMapper.MapFromDAL(gameInBasket.Basket)
            };

            return res;
        }
    }
}