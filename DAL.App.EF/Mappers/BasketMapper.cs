using System;
using System.Runtime.InteropServices;
using Contracts.DAL.Base.Mappers;

namespace DAL.App.EF.Mappers
{
    public class BasketMapper : IBaseDALMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(DAL.App.DTO.Basket))
            {
                return MapFromDomain((Domain.Basket) inObject) as TOutObject;
            }
            
            if (typeof(TOutObject) == typeof(Domain.Basket))
            {
                return MapFromDAL((DAL.App.DTO.Basket) inObject) as TOutObject;
            }
            
            throw new InvalidCastException("No conversion!");
        }

        public static DAL.App.DTO.Basket MapFromDomain(Domain.Basket basket, [Optional] DAL.App.DTO.Billing billing)
        {
            if (basket == null) return null;
            
            var res = new DAL.App.DTO.Basket
            {
                Id = basket.Id,
                Created = basket.Created,
                AppUserId = basket.AppUserId,
                AppUser = AppUserMapper.MapFromDomain(basket.AppUser),
                BillingId = basket.BillingId,
                Billing = null,
                BasketStatusId = basket.BasketStatusId,
                BasketStatus = BasketStatusMapper.MapFromDomain(basket.BasketStatus)
            };

            res.Billing = billing ?? BillingMapper.MapFromDomain(basket.Billing, res);

            return res;
        }

        public static Domain.Basket MapFromDAL(DAL.App.DTO.Basket basket, [Optional] Domain.Billing billing)
        {
            if (basket == null) return null;
            
            var res = new Domain.Basket
            {
                Id = basket.Id,
                Created = basket.Created,
                AppUserId = basket.AppUserId,
                AppUser = AppUserMapper.MapFromDAL(basket.AppUser),
                BillingId = basket.BillingId,
                Billing = null,
                BasketStatusId = basket.BasketStatusId,
                BasketStatus = BasketStatusMapper.MapFromDAL(basket.BasketStatus)
            };

            res.Billing = billing ?? BillingMapper.MapFromDAL(basket.Billing, res);

            return res;
        }
    }
}