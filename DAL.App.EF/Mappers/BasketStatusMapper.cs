using System;
using Contracts.DAL.Base.Mappers;

namespace DAL.App.EF.Mappers
{
    public class BasketStatusMapper : IBaseDALMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(DAL.App.DTO.BasketStatus))
            {
                return MapFromDomain((Domain.BasketStatus) inObject) as TOutObject;
            }
            
            if (typeof(TOutObject) == typeof(Domain.BasketStatus))
            {
                return MapFromDAL((DAL.App.DTO.BasketStatus) inObject) as TOutObject;
            }
            
            throw new InvalidCastException("No conversion!");
        }

        public static DAL.App.DTO.BasketStatus MapFromDomain(Domain.BasketStatus basketStatus)
        {
            if (basketStatus == null) return null;
            
            var res = new DAL.App.DTO.BasketStatus
            {
                Id = basketStatus.Id,
                StatusName = basketStatus.StatusName,
                //Baskets = basketStatus.Baskets
            };
            
            return res;
        }

        public static Domain.BasketStatus MapFromDAL(DAL.App.DTO.BasketStatus basketStatus)
        {
            if (basketStatus == null) return null;
            
            var res = new Domain.BasketStatus
            {
                Id = basketStatus.Id,
                StatusName = basketStatus.StatusName,
                //Baskets = basketStatus.Baskets
            };
            
            return res;
        }
    }
}