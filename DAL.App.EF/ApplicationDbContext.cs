﻿using System.Linq;
using Domain;
using Domain.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF
{
    // Force identity db context to use our AppUser and AppRole - with int as PK type
    public class ApplicationDbContext : IdentityDbContext<AppUser, AppRole, int>
    {
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Basket> Baskets { get; set; }
        public DbSet<BasketStatus> BasketStatuses { get; set; }
        public DbSet<Billing> Billings { get; set; }
        public DbSet<BillingStatus> BillingStatuses { get; set; }
        public DbSet<CreditCard> CreditCards { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<GameInBasket> GamesInBaskets { get; set; }
        public DbSet<GameInWarehouse> GamesInWarehouses { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Platform> Platforms { get; set; }
        public DbSet<Publisher> Publishers { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<Warehouse> Warehouses { get; set; }
        
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        // maps the keys of identity tables
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Basket>()
                .HasOne(e => e.Billing)
                .WithOne(e => e.Basket)
                .HasForeignKey<Billing>(e => e.BasketId);
            
            // disable cascade delete
            foreach (var relationship in builder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
        }
    }
}