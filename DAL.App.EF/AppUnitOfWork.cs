using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base;
using Contracts.DAL.Base.Helpers;
using DAL.Base.EF;

namespace DAL.App.EF
{
    public class AppUnitOfWork : BaseUnitOfWork<ApplicationDbContext>, IAppUnitOfWork

    {
        //private readonly ApplicationDbContext _applicationDbContext;

        public AppUnitOfWork(ApplicationDbContext dbContext, IBaseRepositoryProvider repositoryProvider) : base(
            dbContext,
            repositoryProvider)
        {
        }

        public IAddressRepository Addresses =>
            RepositoryProvider.GetRepository<IAddressRepository>();

        public IAppUserRepository AppUsers =>
            RepositoryProvider.GetRepository<IAppUserRepository>();

        public IAppRoleRepository AppRoles =>
            RepositoryProvider.GetRepository<IAppRoleRepository>();
        
        public IBasketRepository Baskets =>
            RepositoryProvider.GetRepository<IBasketRepository>();

        public IBasketStatusRepository BasketStatuses =>
            RepositoryProvider.GetRepository<IBasketStatusRepository>();

        public IBillingRepository Billings =>
            RepositoryProvider.GetRepository<IBillingRepository>();

        public IBillingStatusRepository BillingStatuses =>
            RepositoryProvider.GetRepository<IBillingStatusRepository>();

        public ICreditCardRepository CreditCards =>
            RepositoryProvider.GetRepository<ICreditCardRepository>();

        public IGameRepository Games =>
            RepositoryProvider.GetRepository<IGameRepository>();

        public IGameInBasketRepository GameInBaskets =>
            RepositoryProvider.GetRepository<IGameInBasketRepository>();

        public IGameInWarehouseRepository GameInWarehouses =>
            RepositoryProvider.GetRepository<IGameInWarehouseRepository>();

        public IGenreRepository Genres =>
            RepositoryProvider.GetRepository<IGenreRepository>();

        public IPlatformRepository Platforms =>
            RepositoryProvider.GetRepository<IPlatformRepository>();

        public IPublisherRepository Publishers =>
            RepositoryProvider.GetRepository<IPublisherRepository>();

        public IReviewRepository Reviews =>
            RepositoryProvider.GetRepository<IReviewRepository>();

        public IWarehouseRepository Warehouses =>
            RepositoryProvider.GetRepository<IWarehouseRepository>();
    }
}