using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Helpers;
using DAL.Base.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class GenreRepository : BaseRepository<DAL.App.DTO.Genre, Domain.Genre, ApplicationDbContext>,
        IGenreRepository
    {
        public GenreRepository(ApplicationDbContext repositoryDbContext) : base(repositoryDbContext, new GenreMapper())
        {
        }

        public new async Task<List<DAL.App.DTO.Genre>> AllAsync()
        {
            return await RepositoryDbSet.Select(genre => GenreMapper.MapFromDomain(genre)).ToListAsync();
        }

        public new async Task<DAL.App.DTO.Genre> FindAsync(params object[] id)
        {
            var genre = await RepositoryDbSet.FindAsync(id);

            return GenreMapper.MapFromDomain(genre);
        }
        
        public async Task<DAL.App.DTO.Genre> GetGenre(int id)
        {
            return GenreMapper.MapFromDomain(await RepositoryDbSet.AsNoTracking().Where(g => g.Id == id).FirstAsync());
        }
    }
}