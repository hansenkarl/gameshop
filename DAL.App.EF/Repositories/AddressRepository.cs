using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;


namespace DAL.App.EF.Repositories
{
    public class AddressRepository : BaseRepository<DAL.App.DTO.Address, Domain.Address, ApplicationDbContext>,
        IAddressRepository
    {
        public AddressRepository(ApplicationDbContext repositoryDbContext) : base(repositoryDbContext,
            new AddressMapper())
        {
        }

        public async Task<List<Address>> AllForUserAsync(int userId)
        {
            return await RepositoryDbSet
                .Include(address => address.AppUser)
                .Where(address => address.AppUserId == userId)
                .Select(address => AddressMapper.MapFromDomain(address))
                .ToListAsync();
        }

        public async Task<Address> FindForUserAsync(int id, int userId)
        {
            var address = await RepositoryDbSet
                .FirstOrDefaultAsync(a => a.Id == id && a.AppUserId == userId);
            return AddressMapper.MapFromDomain(address);
        }

        public async Task<bool> BelongsToUserAsync(int id, int userId)
        {
            return await RepositoryDbSet
                .AnyAsync(address => address.Id == id && address.AppUserId == userId);
        }
    }
}