using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Helpers;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class CreditCardRepository : BaseRepository<DAL.App.DTO.CreditCard, Domain.CreditCard, ApplicationDbContext>,
        ICreditCardRepository
    {
        public CreditCardRepository(ApplicationDbContext repositoryDbContext) : base(repositoryDbContext,
            new CreditCardMapper())
        {
        }

        public async Task<List<CreditCard>> AllForUserAsync(int userId)
        {
            return await RepositoryDbSet
                .Include(x => x.AppUser)
                .Where(x => x.AppUserId == userId)
                .Select(creditCard => CreditCardMapper.MapFromDomain(creditCard))
                .ToListAsync();
        }

        public async Task<CreditCard> FindForUserAsync(int id, int userId)
        {
            var creditCard = await RepositoryDbSet
                .FirstOrDefaultAsync(card => card.Id == id && card.AppUserId == userId);
            return CreditCardMapper.MapFromDomain(creditCard);
        }

        public async Task<bool> BelongsToUserAsync(int id, int userId)
        {
            return await RepositoryDbSet
                .AnyAsync(creditCard => creditCard.Id == id && creditCard.AppUserId == userId);
        }
    }
}