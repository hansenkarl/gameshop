using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Helpers;
using DAL.Base.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class WarehouseRepository : BaseRepository<DAL.App.DTO.Warehouse, Domain.Warehouse, ApplicationDbContext>,
        IWarehouseRepository
    {
        public WarehouseRepository(ApplicationDbContext repositoryDbContext) : base(repositoryDbContext,
            new WarehouseMapper())
        {
        }

        public new async Task<List<DAL.App.DTO.Warehouse>> AllAsync()
        {
            return await RepositoryDbSet.Select(warehouse => WarehouseMapper.MapFromDomain(warehouse)).ToListAsync();
        }

        public new async Task<DAL.App.DTO.Warehouse> FindAsync(params object[] id)
        {
            var warehouse = await RepositoryDbSet.FindAsync(id);

            return WarehouseMapper.MapFromDomain(warehouse);
        }
    }
}