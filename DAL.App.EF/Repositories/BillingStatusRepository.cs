using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class BillingStatusRepository :
        BaseRepository<DAL.App.DTO.BillingStatus, Domain.BillingStatus, ApplicationDbContext>, IBillingStatusRepository
    {
        public BillingStatusRepository(ApplicationDbContext repositoryDbContext) : base(repositoryDbContext,
            new BillingStatusMapper())
        {
        }

        public new async Task<List<DAL.App.DTO.BillingStatus>> AllAsync()
        {
            return await RepositoryDbSet
                .Select(billingStatus => BillingStatusMapper.MapFromDomain(billingStatus))
                .ToListAsync();
        }

        public new async Task<DAL.App.DTO.BillingStatus> FindAsync(params object[] id)
        {
            var billingStatus = await RepositoryDbSet.FindAsync(id);

            return BillingStatusMapper.MapFromDomain(billingStatus);
        }
        
    }
}