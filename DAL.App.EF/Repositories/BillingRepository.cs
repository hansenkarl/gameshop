using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;
using Billing = DAL.App.DTO.Billing;

namespace DAL.App.EF.Repositories
{
    public class BillingRepository : BaseRepository<DAL.App.DTO.Billing, Domain.Billing, ApplicationDbContext>,
        IBillingRepository
    {
        public BillingRepository(ApplicationDbContext repositoryDbContext) : base(repositoryDbContext,
            new BillingMapper())
        {
        }

        public async Task<List<Billing>> AllForUserAsync(int userId)
        {
            return await RepositoryDbSet
                .Include(billing => billing.Basket)
                .Include(billing => billing.BillingStatus)
                .Include(billing => billing.CreditCard)
                .Where(billing => billing.Basket.AppUserId == userId)
                .Select(billing => BillingMapper.MapFromDomain(billing, null))
                .ToListAsync();
        }

        public async Task<Billing> FindForUserAsync(int id, int userId)
        {
            var billing = await RepositoryDbSet
                .Include(b => b.Basket)
                .Include(b => b.BillingStatus)
                .Include(b => b.CreditCard)
                .FirstOrDefaultAsync(b => b.Id == id && b.Basket.AppUserId == userId);
            return BillingMapper.MapFromDomain(billing);
        }

        public async Task<bool> BelongsToUserAsync(int id, int userId)
        {
            return await RepositoryDbSet.AnyAsync(billing =>
                billing.Id == id && billing.Basket.AppUserId == userId);
        }

        public async Task<Billing> GetBilling(int id)
        {
            return BillingMapper.MapFromDomain(await RepositoryDbSet.AsNoTracking().Where(b => b.Id == id).FirstAsync());
        }


        public async Task<Billing> CreateBilling()
        {
            var billing = new Domain.Billing
            {
                SumWithoutTax = 0.00m,
                Tax = 0.20m,
                SumTotal = 0.00m,
                Created = DateTime.Now,
                CreditCardId = null,
                BillingStatusId = 1
            };
            var inProgress = await RepositoryDbContext.AddAsync(billing);
            await RepositoryDbContext.SaveChangesAsync();
            RepositoryDbContext.Entry(billing).State = EntityState.Detached;
            return BillingMapper.MapFromDomain(inProgress.Entity);
        }
    }
}