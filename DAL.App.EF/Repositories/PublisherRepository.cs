using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Helpers;
using DAL.Base.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class PublisherRepository : BaseRepository<DAL.App.DTO.Publisher, Domain.Publisher, ApplicationDbContext>,
        IPublisherRepository
    {
        public PublisherRepository(ApplicationDbContext repositoryDbContext) : base(repositoryDbContext,
            new PublisherMapper())
        {
        }

        public new async Task<List<DAL.App.DTO.Publisher>> AllAsync()
        {
            return await RepositoryDbSet.Select(publisher => PublisherMapper.MapFromDomain(publisher)).ToListAsync();
        }

        public new async Task<DAL.App.DTO.Publisher> FindAsync(params object[] id)
        {
            var publisher = await RepositoryDbSet.FindAsync(id);

            return PublisherMapper.MapFromDomain(publisher);
        }

        public async Task<DAL.App.DTO.Publisher> GetPublisher(int id)
        {
            return PublisherMapper.MapFromDomain(await RepositoryDbSet.AsNoTracking().Where(p => p.Id == id)
                .FirstAsync());
        }
    }
}