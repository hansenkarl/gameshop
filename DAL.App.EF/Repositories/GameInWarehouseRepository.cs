using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class GameInWarehouseRepository :
        BaseRepository<DAL.App.DTO.GameInWarehouse, Domain.GameInWarehouse, ApplicationDbContext>,
        IGameInWarehouseRepository
    {
        public GameInWarehouseRepository(ApplicationDbContext repositoryDbContext) : base(repositoryDbContext,
            new GameInWarehouseMapper())
        {
        }

        public new async Task<List<DAL.App.DTO.GameInWarehouse>> AllAsync()
        {
            return await RepositoryDbSet
                .Include(gameInWarehouse => gameInWarehouse.Game)
                .Include(gameInWarehouse => gameInWarehouse.Warehouse)
                .Select(gameInWarehouse => GameInWarehouseMapper.MapFromDomain(gameInWarehouse))
                .ToListAsync();
        }

        public new async Task<DAL.App.DTO.GameInWarehouse> FindAsync(params object[] id)
        {
            var gameInWarehouse = await RepositoryDbSet.FindAsync(id);

            if (gameInWarehouse != null)
            {
                await RepositoryDbContext
                    .Entry(gameInWarehouse)
                    .Reference(x => x.Game)
                    .LoadAsync();

                await RepositoryDbContext
                    .Entry(gameInWarehouse)
                    .Reference(x => x.Warehouse)
                    .LoadAsync();
            }

            return GameInWarehouseMapper.MapFromDomain(gameInWarehouse);
        }
    }
}