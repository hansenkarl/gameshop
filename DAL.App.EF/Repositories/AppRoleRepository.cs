using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Repositories;

namespace DAL.App.EF.Repositories
{
    public class AppRoleRepository : BaseRepository<DAL.App.DTO.Identity.AppRole, Domain.Identity.AppRole,
    ApplicationDbContext>, IAppRoleRepository
    {
        public AppRoleRepository(ApplicationDbContext repositoryDbContext) : base(repositoryDbContext, new AppRoleMapper()){}
    }
}