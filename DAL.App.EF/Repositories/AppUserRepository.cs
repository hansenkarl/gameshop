using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Repositories;

namespace DAL.App.EF.Repositories
{
    public class AppUserRepository : BaseRepository<DAL.App.DTO.Identity.AppUser, Domain.Identity.AppUser,
        ApplicationDbContext>, IAppUserRepository
    {
        public AppUserRepository(ApplicationDbContext repositoryDbContext) : base(
            repositoryDbContext, new AppUserMapper())
        {
        }
    }
}