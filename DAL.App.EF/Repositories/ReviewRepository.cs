using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Helpers;
using DAL.Base.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class ReviewRepository : BaseRepository<DAL.App.DTO.Review, Domain.Review, ApplicationDbContext>,
        IReviewRepository
    {
        public ReviewRepository(ApplicationDbContext repositoryDbContext) : base(repositoryDbContext,
            new ReviewMapper())
        {
        }
        
        public new async Task<List<DAL.App.DTO.Review>> AllAsync()
        {
            return await RepositoryDbSet
                .Include(x => x.AppUser)
                .Include(x => x.Game)
                .Include(x => x.Platform)
                .Select(review => ReviewMapper.MapFromDomain(review))
                .ToListAsync();
        }

        public new async Task<DAL.App.DTO.Review> FindAsync(params object[] id)
        {
            var review = await RepositoryDbSet.FindAsync(id);

            if (review != null)
            {
                await RepositoryDbContext
                    .Entry(review)
                    .Reference(x => x.AppUser)
                    .LoadAsync();

                await RepositoryDbContext
                    .Entry(review)
                    .Reference(x => x.Game)
                    .LoadAsync();
                
                await RepositoryDbContext
                    .Entry(review)
                    .Reference(x => x.Platform)
                    .LoadAsync();
            }

            return ReviewMapper.MapFromDomain(review);
        }
    }
}