using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class BasketStatusRepository :
        BaseRepository<DAL.App.DTO.BasketStatus, Domain.BasketStatus, ApplicationDbContext>, IBasketStatusRepository
    {
        public BasketStatusRepository(ApplicationDbContext repositoryDbContext) : base(repositoryDbContext,
            new BasketStatusMapper())
        {
        }

        public new async Task<List<DAL.App.DTO.BasketStatus>> AllAsync()
        {
            return await RepositoryDbSet
                .Select(status => BasketStatusMapper.MapFromDomain(status))
                .ToListAsync();
        }

        public new async Task<DAL.App.DTO.BasketStatus> FindAsync(params object[] id)
        {
            var basketStatus = await RepositoryDbSet.FindAsync(id);
            return BasketStatusMapper.MapFromDomain(basketStatus);
        }
    }
}