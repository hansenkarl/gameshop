using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Helpers;
using DAL.Base.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class GameRepository : BaseRepository<DAL.App.DTO.Game, Domain.Game, ApplicationDbContext>, IGameRepository
    {
        public GameRepository(ApplicationDbContext repositoryDbContext) : base(repositoryDbContext, new GameMapper())
        {
        }

        public new async Task<List<DAL.App.DTO.Game>> AllAsync()
        {
            return await RepositoryDbSet
                .Include(game => game.Genre)
                .Include(game => game.Platform)
                .Include(game => game.Publisher)
                .Select(game => GameMapper.MapFromDomain(game))
                .ToListAsync();
        }

        public new async Task<DAL.App.DTO.Game> FindAsync(params object[] id)
        {
            var game = await RepositoryDbSet.FindAsync(id);

            if (game != null)
            {
                await RepositoryDbContext
                    .Entry(game)
                    .Reference(x => x.Genre)
                    .LoadAsync();
                
                await RepositoryDbContext
                    .Entry(game)
                    .Reference(x => x.Platform)
                    .LoadAsync();
                
                await RepositoryDbContext
                    .Entry(game)
                    .Reference(x => x.Publisher)
                    .LoadAsync();
            }

            return GameMapper.MapFromDomain(game);
        }
    }
}