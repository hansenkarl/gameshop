using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Helpers;
using DAL.Base.EF.Repositories;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class PlatformRepository : BaseRepository<DAL.App.DTO.Platform, Domain.Platform, ApplicationDbContext>,
        IPlatformRepository
    {
        public PlatformRepository(ApplicationDbContext repositoryDbContext) : base(repositoryDbContext,
            new PlatformMapper())
        {
        }

        public new async Task<List<DAL.App.DTO.Platform>> AllAsync()
        {
            return await RepositoryDbSet
                .Select(platform => PlatformMapper.MapFromDomain(platform))
                .ToListAsync();
        }

        public new async Task<DAL.App.DTO.Platform> FindAsync(params object[] id)
        {
            var platform = await RepositoryDbSet.FindAsync(id);

            return PlatformMapper.MapFromDomain(platform);
        }

        public async Task<DAL.App.DTO.Platform> GetPlatform(int id)
        {
            return PlatformMapper.MapFromDomain(
                await RepositoryDbSet.AsNoTracking().Where(p => p.Id == id).FirstAsync());
        }
    }
}