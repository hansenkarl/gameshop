using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;
using GameInBasket = DAL.App.DTO.GameInBasket;

namespace DAL.App.EF.Repositories
{
    public class GameInBasketRepository :
        BaseRepository<DAL.App.DTO.GameInBasket, Domain.GameInBasket, ApplicationDbContext>, IGameInBasketRepository
    {
        public GameInBasketRepository(ApplicationDbContext repositoryDbContext) : base(repositoryDbContext,
            new GameInBasketMapper())
        {
        }

        public async Task<List<GameInBasket>> AllForUserAsync(int userId)
        {
            return await RepositoryDbSet
                .Include(gameInBasket => gameInBasket.Basket)
                .Include(gameInBasket => gameInBasket.Game)
                .Where(gameInBasket => gameInBasket.Basket.AppUserId == userId)
                .Select(gameInBasket => GameInBasketMapper.MapFromDomain(gameInBasket))
                .ToListAsync();
        }

        public async Task<GameInBasket> FindForUserAsync(int id, int userId)
        {
            var gameInBasket = await RepositoryDbSet
                .Include(gameInBasket1 => gameInBasket1.Basket)
                .Include(gameInBasket1 => gameInBasket1.Game)
                .FirstOrDefaultAsync(
                    gameInBasket1 => gameInBasket1.Id == id && gameInBasket1.Basket.AppUserId == userId);
            return GameInBasketMapper.MapFromDomain(gameInBasket);
        }

        public async Task<bool> BelongsToUserAsync(int id, int userId)
        {
            return await RepositoryDbSet
                .AnyAsync(gameInBasket => gameInBasket.Id == id && gameInBasket.Basket.AppUserId == userId);
        }
        
        public async Task<List<GameInBasket>> AllForBasketAsync(int basketId) 
        {
            return await RepositoryDbSet
                .Include(gameInBasket => gameInBasket.Basket)
                .Include(gameInBasket => gameInBasket.Game)
                .Where(gameInBasket => gameInBasket.Basket.Id == basketId)
                .Select(gameInBasket => GameInBasketMapper.MapFromDomain(gameInBasket))
                .ToListAsync();
        }
    }
}