using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO;
using DAL.App.EF.Mappers;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF.Repositories
{
    public class BasketRepository : BaseRepository<DAL.App.DTO.Basket, Domain.Basket, ApplicationDbContext>,
        IBasketRepository
    {
        public BasketRepository(ApplicationDbContext repositoryDbContext) : base(repositoryDbContext,
            new BasketMapper())
        {
        }

        public async Task<List<Basket>> AllForUserAsync(int userId)
        {
            return await RepositoryDbSet
                .Include(basket => basket.AppUser)
                .Include(basket => basket.Billing)
                .Include(basket => basket.BasketStatus)
                .Where(basket => basket.AppUserId == userId)
                .Select(basket => BasketMapper.MapFromDomain(basket, null))
                .ToListAsync();
        }

        public async Task<Basket> FindForUserAsync(int id, int userId)
        {
            var baskets = await RepositoryDbSet
                .FirstOrDefaultAsync(b => b.Id == id && b.AppUserId == userId);
            return BasketMapper.MapFromDomain(baskets);
        }

        public async Task<bool> BelongsToUserAsync(int id, int userId)
        {
            return await RepositoryDbSet.AnyAsync(basket => basket.Id == id && basket.AppUserId == userId);
        }

        public async Task<Basket> GetBasket(int id)
        {
            return BasketMapper.MapFromDomain(await RepositoryDbSet.AsNoTracking().Where(b => b.Id == id).FirstAsync());
        }
        
        public async Task<Basket> CreateBasket()
        {
            var basket = new Domain.Basket {AppUserId = 2, BillingId = 1, BasketStatusId = 1};
            var inProgress = await RepositoryDbContext.AddAsync(basket);
            await RepositoryDbContext.SaveChangesAsync();
            RepositoryDbContext.Entry(basket).State = EntityState.Detached;
            return BasketMapper.MapFromDomain(inProgress.Entity);
        }
        
        public async Task<List<Basket>> AllNonCheckedOutUserAsync(List<Basket> baskets)
        {
            var nonCheckedOutBaskets = new List<Basket>();
            
            foreach (var basket in baskets)
            {
                if (basket.BasketStatusId == 1)
                {
                    nonCheckedOutBaskets.Add(basket);
                }
            }
            

            return nonCheckedOutBaskets;
        }
    }
}