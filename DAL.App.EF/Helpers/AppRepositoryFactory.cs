using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base.Helpers;
using DAL.App.EF.Repositories;
using DAL.Base.EF.Helpers;

namespace DAL.App.EF.Helpers
{
    public class AppRepositoryFactory : BaseRepositoryFactory<ApplicationDbContext>
    {
        public AppRepositoryFactory()
        {
            RegisterRepositories();
        }

        private void RegisterRepositories()
        {
            // all the repository creation methods are added to dictionary
            AddToCreationMethods<IAddressRepository>(dataContext => 
                new AddressRepository(dataContext));
            
            AddToCreationMethods<IAppUserRepository>(dataContext => 
                new AppUserRepository(dataContext));
            
            AddToCreationMethods<IAppRoleRepository>(dataContext =>
                new AppRoleRepository(dataContext));

            AddToCreationMethods<IBasketRepository>(dataContext => 
                new BasketRepository(dataContext));
            
            AddToCreationMethods<IBasketStatusRepository>(dataContext => 
                new BasketStatusRepository(dataContext));

            AddToCreationMethods<IBillingRepository>(dataContext => 
                new BillingRepository(dataContext));
            
            AddToCreationMethods<IBillingStatusRepository>(dataContext =>
                new BillingStatusRepository(dataContext));

            AddToCreationMethods<ICreditCardRepository>(dataContext => 
                new CreditCardRepository(dataContext));
            
            AddToCreationMethods<IGameInBasketRepository>(dataContext => 
                new GameInBasketRepository(dataContext));
            
            AddToCreationMethods<IGameInWarehouseRepository>(dataContext => 
                new GameInWarehouseRepository(dataContext));
            
            AddToCreationMethods<IGameRepository>(dataContext => 
                new GameRepository(dataContext));

            AddToCreationMethods<IGenreRepository>(dataContext => 
                new GenreRepository(dataContext));

            AddToCreationMethods<IPlatformRepository>(dataContext => 
                new PlatformRepository(dataContext));
            
            AddToCreationMethods<IPublisherRepository>(dataContext => 
                new PublisherRepository(dataContext));

            AddToCreationMethods<IReviewRepository>(dataContext => 
                new ReviewRepository(dataContext));
            
            AddToCreationMethods<IWarehouseRepository>(dataContext =>
                new WarehouseRepository(dataContext));
        }
    }
}