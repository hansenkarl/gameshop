﻿using System;
using System.Linq;
using System.Security.Claims;
using Domain.Identity;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;

namespace Identity
{
    //extension method for easy logged in user Id retrieval
    public static class IdentityExtensions
    {
        public static int GetUserId(this ClaimsPrincipal principal)
        {
            return GetUserId<int>(principal);
        }

        /*public static int GetUserRoleId()
        {
            
        }*/

        public static TKey GetUserId<TKey>(this ClaimsPrincipal principal)
        {
            if (principal == null)
            {
                throw new ArgumentNullException(nameof(principal));
            }

            string userId = principal.FindFirst(ClaimTypes.NameIdentifier).Value;

            if (typeof(TKey) == typeof(int))
            {
                if (!int.TryParse(userId, out _))
                {
                    throw new ArgumentException("ClaimsPrincipal NameIdentifier is not of the type int!");
                }
            }

            return (TKey) Convert.ChangeType(userId, typeof(TKey));

            // this is tiny bit slower, but handles GUID type also
            // return (TKey) TypeDescriptor.GetConverter(typeof(TKey)).ConvertFromInvariantString(userId);
        }

        public static void IdentityAddDefaultRolesAndUsers(this IApplicationBuilder app,
            UserManager<AppUser> userManager,
            RoleManager<AppRole> roleManager)
        {
            // admin role creation
            if (!roleManager.Roles.Any(r => r.NormalizedName == "ADMIN"))
            {
                var result = roleManager.CreateAsync(new AppRole{Name = "Admin"}).Result;
                if (result == IdentityResult.Success)
                {
                }
            }

            // admin role creation
            var user = userManager.FindByEmailAsync("khanse@ttu.ee").Result;
            if (user == null)
            {
                var result = userManager.CreateAsync(new AppUser()
                {
                    Email = "khanse@ttu.ee",
                    UserName = "khanse@ttu.ee",
                    FirstName = "Karl",
                    LastName = "Hansen"
                }, "Adminkarl").Result;
                if (result == IdentityResult.Success)
                {
                    user = userManager.FindByEmailAsync("khanse@ttu.ee").Result;
                }
            }

            if (user != null && !userManager.IsInRoleAsync(user, "Admin").Result)
            {
                var result = userManager.AddToRoleAsync(user, "Admin").Result;
            }

            // client role creation
            if (!roleManager.Roles.Any(r => r.NormalizedName == "CLIENT"))
            {
                var clientRole = roleManager.CreateAsync(new AppRole() {Name = "Client"}).Result;
                if (clientRole == IdentityResult.Success)
                {
                }
            }
        }
    }
}