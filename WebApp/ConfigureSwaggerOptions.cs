using System;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace WebApp
{
    /// <summary>
    /// Class, which configures the swagger options.
    /// </summary>
    public class ConfigureSwaggerOptions : IConfigureOptions<SwaggerGenOptions>{
        readonly IApiVersionDescriptionProvider _provider;
        /// <summary>
        /// Construct and initialize swagger options.
        /// </summary>
        /// <param name="provider">Defines the behaviour of a provider that discovers and describes API version
        /// information within an application.</param>
        public ConfigureSwaggerOptions(IApiVersionDescriptionProvider provider) =>
            this._provider = provider;

        /// <summary>
        /// Configure swagger options.
        /// </summary>
        /// <param name="options">SwaggerGenOptions</param>
        public void Configure( SwaggerGenOptions options ){
            foreach ( var description in _provider.ApiVersionDescriptions ) {
                options.SwaggerDoc(
                    description.GroupName,
                    new Info(){
                        Title = $"GameStore API {description.ApiVersion}",
                        Version = description.ApiVersion.ToString()
                    }
                );
                // include xml comments (enable creation in csproj file)
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                options.IncludeXmlComments(xmlPath);
            }
        }
    }


}