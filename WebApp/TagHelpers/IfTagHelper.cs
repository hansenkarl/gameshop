using Microsoft.AspNetCore.Razor.TagHelpers;

namespace WebApp.TagHelpers
{
    /// <summary>
    /// Tag helper that enables hiding certain properties (found online).
    /// </summary>
    public class IfTagHelper : TagHelper
    {
        /// <summary>
        /// An order for something.
        /// </summary>
        public override int Order => -1000;

        /// <summary>
        /// Include set element.
        /// </summary>
        [HtmlAttributeName("include-if")]
        public bool Include { get; set; } = true;

        /// <summary>
        /// Exclude set element.
        /// </summary>
        [HtmlAttributeName("exclude-if")]
        public bool Exclude { get; set; } = false;

        /// <summary>
        /// Process output.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="output"></param>
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            // Always strip the outer tag name as we never want <if> to render
            output.TagName = null;

            if (Include && !Exclude)
            {
                return;
            }

            output.SuppressOutput();
        }
    }
}