using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.UI.Services;

namespace WebApp.Helpers
{
    /// <summary>
    /// Make startup think that Email has been sent.
    /// </summary>
    public class EmailSender : IEmailSender
    {
        /// <summary>
        /// Make it look like an email has been sent.
        /// </summary>
        /// <param name="email">an email</param>
        /// <param name="subject">a subject to receive an email</param>
        /// <param name="htmlMessage">a message that has been added into the html</param>
        /// <returns>Task has been completed.</returns>
        public Task SendEmailAsync(string email, string subject, string htmlMessage)
        {
            return Task.CompletedTask;
        }
    }
}