using System.Threading.Tasks;
using BLL.App.DTO;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Mvc;
using Identity;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.Controllers
{
    /// <summary>
    /// Controller containing all the methods for operations with CreditCards.
    /// </summary>
    [Authorize]
    public class CreditCardsController : Controller
    {
        private readonly IAppBLL _bll;

        /// <summary>
        /// Construct and initialize CreditCardsController.
        /// </summary>
        /// <param name="bll"></param>
        public CreditCardsController(IAppBLL bll)
        {
            _bll = bll;
        }

        // GET: CreditCards
        /// <summary>
        /// Get all CreditCards objects for a logged in user.
        /// </summary>
        /// <returns>Array of all the CreditCards that have been created by the logged in user.</returns>
        public async Task<IActionResult> Index()
        {
            return View(await _bll.CreditCards.AllForUserAsync(User.GetUserId()));
        }

        // GET: CreditCards/Details/5
        /// <summary>
        /// Display details regarding a specific credit card.
        /// </summary>
        /// <param name="id">Id of a specific credit card.</param>
        /// <returns>A specific credit card with all its details.</returns>
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var creditCard = await _bll.CreditCards.FindForUserAsync(id.Value, User.GetUserId());
            if (creditCard == null)
            {
                return NotFound();
            }
            
            if (!await _bll.CreditCards.BelongsToUserAsync(id.Value, User.GetUserId()))
            {
                return Forbid();
            }

            return View(creditCard);
        }

        // GET: CreditCards/Create
        /// <summary>
        /// Get the view for creating a new credit card.
        /// </summary>
        /// <returns>The view, where credit card can be created.</returns>
        public IActionResult Create()
        {
            return View();
        }

        // POST: CreditCards/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// Create a new credit card object.
        /// </summary>
        /// <param name="creditCard">A credit card containing all the fields required.</param>
        /// <returns>Redirects to credit cards' main page or the same view, if the form was faulty.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CreditCardNr,ValidThru,Id")]
            CreditCard creditCard)
        {
            creditCard.AppUserId = User.GetUserId();
            
            if (ModelState.IsValid)
            {
                _bll.CreditCards.Add(creditCard);
                await _bll.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            
            return View(creditCard);
        }

        // GET: CreditCards/Edit/5
        /// <summary>
        /// Get a credit card for editing.
        /// </summary>
        /// <param name="id">The id of a credit card to be edited.</param>
        /// <returns>The edit view or NotFound, if given id is null or forbids
        /// deletion, if credit card does not belong to a currently logged in user.</returns>
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var creditCard = await _bll.CreditCards.FindForUserAsync(id.Value, User.GetUserId());
            if (creditCard == null)
            {
                return NotFound();
            }
            
            if (!await _bll.CreditCards.BelongsToUserAsync(id.Value, User.GetUserId()))
            {
                return Forbid();
            }
            
            return View(creditCard);
        }

        // POST: CreditCards/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// Save changes made to an address.
        /// </summary>
        /// <param name="id">Id of an edited credit card.</param>
        /// <param name="creditCard">The credit card object.</param>
        /// <returns>Redirects to credit cards' main page or stays to the same page, if credit card is not valid or forbids
        /// deletion, if credit card does not belong to a currently logged in user.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CreditCardNr,ValidThru,Id")]
            CreditCard creditCard)
        {
            if (id != creditCard.Id)
            {
                return NotFound();
            }
            
            if (!await _bll.CreditCards.BelongsToUserAsync(id, User.GetUserId()))
            {
                return Forbid();
            }

            creditCard.AppUserId = User.GetUserId();

            if (ModelState.IsValid)
            {
                _bll.CreditCards.Update(creditCard);
                await _bll.SaveChangesAsync();


                return RedirectToAction(nameof(Index));
            }
            
            return View(creditCard);
        }

        // GET: CreditCards/Delete/5
        /// <summary>
        /// Get a credit card to be deleted.
        /// </summary>
        /// <param name="id">Id of a credit card to be deleted.</param>
        /// <returns>The confirmation view or NotFound in case id or credit card is null or forbids deletion, if credit
        /// card does not belong to a currently logged in user.</returns>
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var creditCard = await _bll.CreditCards.FindForUserAsync(id.Value, User.GetUserId());
            if (creditCard == null)
            {
                return NotFound();
            }
            
            if (!await _bll.CreditCards.BelongsToUserAsync(id.Value, User.GetUserId()))
            {
                return Forbid();
            }

            return View(creditCard);
        }

        // POST: CreditCards/Delete/5
        /// <summary>
        /// Save changes after credit card has been deleted.
        /// </summary>
        /// <param name="id">Id of a credit card to be deleted.</param>
        /// <returns>Redirects to credit cards' main page or forbids deletion, if credit card does not belong to a
        /// currently logged in user.</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (!await _bll.CreditCards.BelongsToUserAsync(id, User.GetUserId()))
            {
                return Forbid();
            }
            
            _bll.CreditCards.Remove(id);
            await _bll.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}