using System.Threading.Tasks;
using BLL.App.DTO;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers
{
    /// <summary>
    /// Controller containing all the methods for operations with publishers.
    /// </summary>
    [Authorize]
    public class PublishersController : Controller
    {
        private readonly IAppBLL _bll;

        /// <summary>
        /// Construct and initialize PublishersController.
        /// </summary>
        /// <param name="bll">business logic layer</param>
        public PublishersController(IAppBLL bll)
        {
            _bll = bll;
        }

        // GET: Publishers
        /// <summary>
        /// Get all Publishers objects that have been created.
        /// </summary>
        /// <returns>Array of all the publishers.</returns>
        public async Task<IActionResult> Index()
        {
            return View(await _bll.Publishers.AllAsync());
        }

        // GET: Publishers/Details/5
        /// <summary>
        /// Display details regarding a specific publisher.
        /// </summary>
        /// <param name="id">Id of a specific publisher.</param>
        /// <returns>A specific publisher with all its details.</returns>
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var publisher = await _bll.Publishers.FindAsync(id);
            if (publisher == null)
            {
                return NotFound();
            }

            return View(publisher);
        }

        // GET: Publishers/Create
        /// <summary>
        /// Get the view for creating a new publisher.
        /// </summary>
        /// <returns>The view, where publisher can be created.</returns>
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Publishers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// Create a new publisher object.
        /// </summary>
        /// <param name="publisher">A publisher containing all the fields required.</param>
        /// <returns>Redirects to publishers' main page or the same view, if the form was faulty.</returns>
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PublisherName,Id")] Publisher publisher)
        {
            if (ModelState.IsValid)
            {
                _bll.Publishers.Add(publisher);
                await _bll.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            return View(publisher);
        }

        // GET: Publishers/Edit/5
        /// <summary>
        /// Get a publisher for editing.
        /// </summary>
        /// <param name="id">The id of a publisher to be edited.</param>
        /// <returns>The edit view or NotFound, if given id is null.</returns>
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var publisher = await _bll.Publishers.FindAsync(id);
            if (publisher == null)
            {
                return NotFound();
            }

            return View(publisher);
        }

        // POST: Publishers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// Save changes made to a publisher.
        /// </summary>
        /// <param name="id">Id of an edited publisher.</param>
        /// <param name="publisher">The publisher object.</param>
        /// <returns>Redirects to publishers' main page or stays to the same page, if publisher is not valid.</returns>
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PublisherName,Id")] Publisher publisher)
        {
            if (id != publisher.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                _bll.Publishers.Update(publisher);
                await _bll.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }

            return View(publisher);
        }

        // GET: Publishers/Delete/5
        /// <summary>
        /// Get a publisher to be deleted.
        /// </summary>
        /// <param name="id">Id of an publisher to be deleted.</param>
        /// <returns>The confirmation view or NotFound in case id or publisher is null or forbids deletion, if publisher
        /// does not belong to a currently logged in user.</returns>
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var publisher = await _bll.Publishers.FindAsync(id);
            if (publisher == null)
            {
                return NotFound();
            }

            return View(publisher);
        }

        // POST: Publishers/Delete/5
        /// <summary>
        /// Save changes after publisher has been deleted.
        /// </summary>
        /// <param name="id">Id of a publisher to be deleted.</param>
        /// <returns>Redirects to publishers' main page.</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            _bll.Publishers.Remove(id);
            await _bll.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}