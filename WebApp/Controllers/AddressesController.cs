using System.Threading.Tasks;
using BLL.App.DTO;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Mvc;
using Identity;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.Controllers
{
    /// <summary>
    /// Controller containing all the methods for operations with addresses.
    /// </summary>
    [Authorize]
    public class AddressesController : Controller
    {
        private readonly IAppBLL _bll;

        /// <summary>
        /// Construct and initialize AddressesController.
        /// </summary>
        /// <param name="bll">business logic layer</param>
        public AddressesController(IAppBLL bll)
        {
            _bll = bll;
        }

        // GET: Addresses
        /// <summary>
        /// Get all Address objects for a logged in user.
        /// </summary>
        /// <returns>Array of all the addresses that have been created by the logged in user.</returns>
        public async Task<IActionResult> Index()
        {
            return View(await _bll.Addresses.AllForUserAsync(User.GetUserId()));
        }

        // GET: Addresses/Details/5
        /// <summary>
        /// Display details regarding a specific address.
        /// </summary>
        /// <param name="id">Id of a specific address.</param>
        /// <returns>A specific address with all its details.</returns>
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var address = await _bll.Addresses.FindForUserAsync(id.Value, User.GetUserId());
            if (address == null)
            {
                return NotFound();
            }
            
            if (!await _bll.Addresses.BelongsToUserAsync(id.Value, User.GetUserId()))
            {
                return Forbid();
            }

            return View(address);
        }

        // GET: Addresses/Create
        /// <summary>
        /// Get the view for creating a new address.
        /// </summary>
        /// <returns>The view, where address can be created.</returns>
        public IActionResult Create()
        {
            return View();
        }

        // POST: Addresses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// Create a new address object.
        /// </summary>
        /// <param name="address">An address containing all the fields required.</param>
        /// <returns>Redirects to addresses' main page or the same view, if the form was faulty.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Country,County,City,Street,HouseNr,ZipCode")]
            Address address)
        {
            address.AppUserId = User.GetUserId();

            if (ModelState.IsValid)
            {
                _bll.Addresses.Add(address);
                await _bll.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            return View(address);
        }

        // GET: Addresses/Edit/5
        /// <summary>
        /// Get an address for editing.
        /// </summary>
        /// <param name="id">The id of an address to be edited.</param>
        /// <returns>The edit view or NotFound, if given id is null or forbids
        /// deletion, if address does not belong to a currently logged in user.</returns>
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var address = await _bll.Addresses.FindAsync(id);
            if (address == null)
            {
                return NotFound();
            }
            
            if (!await _bll.Addresses.BelongsToUserAsync(id.Value, User.GetUserId()))
            {
                return Forbid();
            }

            address.AppUserId = User.GetUserId();

            return View(address);
        }

        // POST: Addresses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// Save changes made to an address.
        /// </summary>
        /// <param name="id">Id of an edited address.</param>
        /// <param name="address">The address object.</param>
        /// <returns>Redirects to addresses' main page or stays to the same page, if address is not valid or forbids
        /// deletion, if address does not belong to a currently logged in user.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Country,County,City,Street,HouseNr,ZipCode,Id")]
            Address address)
        {
            if (id != address.Id)
            {
                return NotFound();
            }

            address.AppUserId = User.GetUserId();
            
            if (!await _bll.Addresses.BelongsToUserAsync(id, User.GetUserId()))
            {
                return Forbid();
            }
            
            if (ModelState.IsValid)
            {
                _bll.Addresses.Update(address);
                await _bll.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }

            return View(address);
        }

        // GET: Addresses/Delete/5
        /// <summary>
        /// Get an address to be deleted.
        /// </summary>
        /// <param name="id">Id of an address to be deleted.</param>
        /// <returns>The confirmation view or NotFound in case id or address is null or forbids deletion, if address
        /// does not belong to a currently logged in user.</returns>
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var address = await _bll.Addresses.FindForUserAsync(id.Value, User.GetUserId());
            if (address == null)
            {
                return NotFound();
            }

            if (!await _bll.Addresses.BelongsToUserAsync(id.Value, User.GetUserId()))
            {
                return Forbid();
            }
            
            address.AppUserId = User.GetUserId();
            
            return View(address);
        }

        // POST: Addresses/Delete/5
        /// <summary>
        /// Save changes after address has been deleted.
        /// </summary>
        /// <param name="id">Id of an address to be deleted.</param>
        /// <returns>Redirects to addresses' main page or forbids deletion, if address does not belong to a currently
        /// logged in user.</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (!await _bll.Addresses.BelongsToUserAsync(id, User.GetUserId()))
            {
                return Forbid();
            }
            
            _bll.Addresses.Remove(id);
            await _bll.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}