using System.Threading.Tasks;
using BLL.App.DTO;
using Contracts.BLL.App;
using Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebApp.Models;

namespace WebApp.Controllers
{
    /// <summary>
    /// Controller containing all the methods for operations with billings.
    /// </summary>
    public class BillingsController : Controller
    {
        private readonly IAppBLL _bll;

        /// <summary>
        /// Construct and initialize BillingsController.
        /// </summary>
        /// <param name="bll">business logic layer</param>
        public BillingsController(IAppBLL bll)
        {
            _bll = bll;
        }

        // GET: Billings
        /// <summary>
        /// Get all Billings objects for a logged in user.
        /// </summary>
        /// <returns>Array of all the billings that have been created by the logged in user.</returns>
        public async Task<IActionResult> Index()
        {
            return View(await _bll.Billings.AllForUserAsync(User.GetUserId()));
        }

        // GET: Billings/Details/5
        /// <summary>
        /// Display details regarding a specific billing.
        /// </summary>
        /// <param name="id">Id of a specific billing.</param>
        /// <returns>A specific billing with all its details.</returns>
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var billing = await _bll.Billings.FindForUserAsync(id.Value, User.GetUserId());
            if (billing == null)
            {
                return NotFound();
            }
            
            if (!await _bll.Billings.BelongsToUserAsync(id.Value, User.GetUserId()))
            {
                return Forbid();
            }

            return View(billing);
        }

        // GET: Billings/Create
        /// <summary>
        /// Get the view for creating a new billing.
        /// </summary>
        /// <returns>The view, where billing can be created.</returns>
        public IActionResult Create()
        {
            return View();
        }

        // POST: Billings/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// Create a new billing object.
        /// </summary>
        /// <param name="billing">A billing containing all the fields required.</param>
        /// <returns>Redirects to billings' main page or the same view, if the form was faulty
        /// (billing is created automatically for each basket).</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(
            [Bind("SumWithoutTax,Tax,SumTotal,Created,BasketId,CreditCardId,BillingStatusId,Id")]
            Billing billing)
        {
            if (ModelState.IsValid)
            {
                _bll.Billings.Add(billing);
                await _bll.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            return View(billing);
        }

        // GET: Billings/Edit/5
        /// <summary>
        /// Get a billing for editing.
        /// </summary>
        /// <param name="id">The id of a billing to be edited.</param>
        /// <returns>The edit view or NotFound, if given id is null or forbids
        /// deletion, if billing does not belong to a currently logged in user (deleted, if basket is deleted).
        /// </returns>
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var billing = await _bll.Billings.FindAsync(id);
            if (billing == null)
            {
                return NotFound();
            }

            if (!await _bll.Billings.BelongsToUserAsync(id.Value, User.GetUserId()))
            {
                return Forbid();
            }
            
            var billingCheckout = new BillingCheckoutModel()
            {
                Billing = billing,

                CreditCardSelectList = new SelectList(
                    await _bll.CreditCards.AllForUserAsync(User.GetUserId()),
                    nameof(CreditCard.Id),
                    nameof(CreditCard.CreditCardNr))
            };

            return View(billingCheckout);
        }

        // POST: Billings/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        
        /// <summary>
        /// Save changes made to a billing.
        /// </summary>
        /// <param name="id">Id of an edited billing.</param>
        /// <param name="billingCheckoutModel">A model that makes it possible to check out billing and select a
        /// corresponding basket to it and give user a choice to select a credit card.</param>
        /// <returns>Redirects to billings' main page, NotFound in case of an id problem or forbids unauthorized user to
        /// checkout.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, BillingCheckoutModel billingCheckoutModel)
        {
            if (id != billingCheckoutModel.Billing.Id)
            {
                return NotFound();
            }

            if (!await _bll.Billings.BelongsToUserAsync(id, User.GetUserId()))
            {
                return Forbid();
            }

            billingCheckoutModel.Basket =
                await _bll.Baskets.GetBasket(billingCheckoutModel.Billing.BasketId.Value);

            if (billingCheckoutModel.Billing.BillingStatusId != 1) return BadRequest();
            if (billingCheckoutModel.Basket.BasketStatusId != 1) return BadRequest();

            billingCheckoutModel.Billing.BillingStatusId = 2;
            billingCheckoutModel.Basket.BasketStatusId = 2;

            if (ModelState.IsValid)
            {
                _bll.Billings.Update(billingCheckoutModel.Billing);
                _bll.Baskets.Update(billingCheckoutModel.Basket);
                await _bll.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }


            return View(billingCheckoutModel);
        }

        // GET: Billings/Delete/5
        /// <summary>
        /// Get a billing to be deleted.
        /// </summary>
        /// <param name="id">Id of a billing to be deleted.</param>
        /// <returns>The confirmation view or NotFound in case id or address is null or forbids deletion, if billing
        /// does not belong to a currently logged in user (deleting billings does not use this anymore).</returns>
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var billing = await _bll.Billings.FindForUserAsync(id.Value, User.GetUserId());
            if (billing == null)
            {
                return NotFound();
            }
            
            if (!await _bll.Billings.BelongsToUserAsync(id.Value, User.GetUserId()))
            {
                return Forbid();
            }

            return View(billing);
        }

        // POST: Billings/Delete/5
        /// <summary>
        /// Save changes after billing has been deleted.
        /// </summary>
        /// <param name="id">Id of a billing to be deleted.</param>
        /// <returns>Redirects to billings' main page or forbids deletion, if billing does not belong to a currently
        /// logged in user (also unused).</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (!await _bll.Billings.BelongsToUserAsync(id, User.GetUserId()))
            {
                return Forbid();
            }
            
            var billing = await _bll.Billings.FindAsync(id);
            _bll.Billings.Remove(billing);
            await _bll.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}