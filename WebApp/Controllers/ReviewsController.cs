using System;
using System.Threading.Tasks;
using BLL.App.DTO;
using Contracts.BLL.App;
using Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApp.Models;

namespace WebApp.Controllers
{
    /// <summary>
    /// Controller containing all the methods for operations with reviews.
    /// </summary>
    public class ReviewsController : Controller
    {
        private readonly IAppBLL _bll;

        /// <summary>
        /// Construct and initialize ReviewsController.
        /// </summary>
        /// <param name="bll">business logic layer</param>
        public ReviewsController(IAppBLL bll)
        {
            _bll = bll;
        }

        // GET: Reviews
        /// <summary>
        /// Get all Reviews objects that have been created.
        /// </summary>
        /// <returns>Array of all the reviews.</returns>
        public async Task<IActionResult> Index()
        {
            return View(await _bll.Reviews.AllAsync());
        }

        // GET: Reviews/Details/5
        /// <summary>
        /// Display details regarding a specific review.
        /// </summary>
        /// <param name="id">Id of a specific review.</param>
        /// <returns>A specific review with all its details.</returns>
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var review = await _bll.Reviews.FindAsync(id);
            if (review == null)
            {
                return NotFound();
            }

            return View(review);
        }

        // GET: Reviews/Create
        /// <summary>
        /// Get the view for creating a new review.
        /// </summary>
        /// <returns>The view, where review can be created.</returns>
        [Authorize]
        public async Task<IActionResult> Create(int? id)
        {

            var game = await _bll.Games.FindAsync(id);

            var gameReviewModel = new GameReviewModel()
            {
                Game = game,
                
                Review = new Review()
                {
                    GameId = game.Id,
                    AppUserId = User.GetUserId(),
                    PlatformId = game.PlatformId
                }
            };
            
            return View(gameReviewModel);
        }

        // POST: Reviews/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// Create a new review object.
        /// </summary>
        /// <param name="review">A review containing all the fields required.</param>
        /// <returns>Redirects to reviews' main page or the same view, if the form was faulty.</returns>
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Rating,Comment,Added,AppUserId,GameId,PlatformId,Id")]
            Review review)
        {
            var game = await _bll.Games.FindAsync(review.GameId);

            review.Added = DateTime.Now;

            var gameReviewModel = new GameReviewModel
            {
                Game = game,
                
                Review = review
            };
            
            if (ModelState.IsValid)
            {
                _bll.Reviews.Add(review);
                await _bll.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            
            return View(gameReviewModel);
        }

        // GET: Reviews/Edit/5
        /// <summary>
        /// Get a review for editing.
        /// </summary>
        /// <param name="id">The id of a review to be edited.</param>
        /// <returns>The edit view or NotFound, if given id is null.</returns>
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var review = await _bll.Reviews.FindAsync(id);
            if (review == null)
            {
                return NotFound();
            }
            
            return View(review);
        }

        // POST: Reviews/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// Save changes made to a review.
        /// </summary>
        /// <param name="id">Id of an edited review.</param>
        /// <param name="review">The review object.</param>
        /// <returns>Redirects to reviews' main page or stays to the same page, if review is not valid.</returns>
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Rating,Comment,Added,AppUserId,GameId,PlatformId,Id")]
            Review review)
        {
            if (id != review.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                _bll.Reviews.Update(review);
                await _bll.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }
            
            return View(review);
        }

        // GET: Reviews/Delete/5
        /// <summary>
        /// Get a review to be deleted.
        /// </summary>
        /// <param name="id">Id of an review to be deleted.</param>
        /// <returns>The confirmation view or NotFound in case id or review is null.</returns>
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var review = await _bll.Reviews.FindAsync(id);
            if (review == null)
            {
                return NotFound();
            }

            return View(review);
        }

        // POST: Reviews/Delete/5
        /// <summary>
        /// Save changes after review has been deleted.
        /// </summary>
        /// <param name="id">Id of a review to be deleted.</param>
        /// <returns>Redirects to reviews' main page.</returns>
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            _bll.Reviews.Remove(id);
            await _bll.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}