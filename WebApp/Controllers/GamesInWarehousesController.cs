using System;
using System.Threading.Tasks;
using BLL.App.DTO;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebApp.Models;

namespace WebApp.Controllers
{
    /// <summary>
    /// API controller containing all the methods for operations with games in warehouses.
    /// </summary>
    [Authorize(Roles = "Admin")]
    public class GamesInWarehousesController : Controller
    {
        private readonly IAppBLL _bll;

        /// <summary>
        /// Construct and initialize GamesInWarehousesController.
        /// </summary>
        /// <param name="bll">business logic layer</param>
        public GamesInWarehousesController(IAppBLL bll)
        {
            _bll = bll;
        }


        // GET: GamesInWarehouses
        /// <summary>
        ///  Get all GamesInWarehouses objects.
        /// </summary>
        /// <returns>Array of all the gamesInWarehouses.</returns>
        public async Task<IActionResult> Index()
        {
            return View(await _bll.GameInWarehouses.AllAsync());
        }

        // GET: GamesInWarehouses/Details/5
        /// <summary>
        /// Display details regarding a specific address.
        /// </summary>
        /// <param name="id">Id of a specific game in warehouse.</param>
        /// <returns>A specific game in warehouse with all its details.</returns>
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var gameInWarehouse = await _bll.GameInWarehouses.FindAsync(id);
            if (gameInWarehouse == null)
            {
                return NotFound();
            }

            return View(gameInWarehouse);
        }

        // GET: GamesInWarehouses/Create
        /// <summary>
        /// Get the view for creating a new game in warehouse.
        /// </summary>
        /// <returns>The view, where game in warehouse can be created.</returns>
        public async Task<IActionResult> Create()
        {
            var viewModel = new GameRelatedModel
            {
                GameSelectList = new SelectList(
                    await _bll.Games.AllAsync(),
                    nameof(Game.Id),
                    nameof(Game.GameTitle)
                ),
                WarehouseSelectList = new SelectList(
                    await _bll.Warehouses.AllAsync(),
                    nameof(Warehouse.Id),
                    nameof(Warehouse.WarehouseName)),
                GameInWarehouse = new GameInWarehouse {Added = DateTime.Now}
            };


            return View(viewModel);
        }

        // POST: GamesInWarehouses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        
        /// <summary>
        /// Create a new game in warehouse object.
        /// </summary>
        /// <param name="viewModel">A viewModel containing all the required data to create a game in warehouse.</param>
        /// <returns>Redirects to addresses' main page or the same view, if the form was faulty.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(GameRelatedModel viewModel)
        {
            viewModel.GameInWarehouse.Added = DateTime.Now;
            if (ModelState.IsValid)
            {
                _bll.GameInWarehouses.Add(viewModel.GameInWarehouse);
                await _bll.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            viewModel.GameSelectList = new SelectList(
                await _bll.Games.AllAsync(),
                nameof(Game.Id),
                nameof(Game.GameTitle)
            );

            viewModel.WarehouseSelectList = new SelectList(
                await _bll.Warehouses.AllAsync(),
                nameof(Warehouse.Id),
                nameof(Warehouse.WarehouseName)
            );

            return View(viewModel);
        }

        // GET: GamesInWarehouses/Edit/5
        /// <summary>
        /// Get a game in warehouse for editing.
        /// </summary>
        /// <param name="id">The id of a game in warehouse to be edited.</param>
        /// <returns>The edit view or NotFound, if given id is null.</returns>
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var gameInWarehouse = await _bll.GameInWarehouses.FindAsync(id);
            if (gameInWarehouse == null)
            {
                return NotFound();
            }

            return View(gameInWarehouse);
        }

        // POST: GamesInWarehouses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// Save changes made to an address.
        /// </summary>
        /// <param name="id">Id of an edited game in warehouse.</param>
        /// <param name="gameInWarehouse">The game in warehouse object.</param>
        /// <returns>Redirects to games in warehouses' main page or stays to the same page, if game in warehouse
        /// is not valid.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Amount,Added,WarehouseId,GameId,Id")]
            GameInWarehouse gameInWarehouse)
        {
            if (id != gameInWarehouse.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                _bll.GameInWarehouses.Update(gameInWarehouse);
                await _bll.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            /*ViewData["GameId"] = new SelectList(_context.Games, "Id", "GameTitle", gameInWarehouse.GameId);
            ViewData["WarehouseId"] =
                new SelectList(_context.Warehouses, "Id", "WarehouseName", gameInWarehouse.WarehouseId);*/
            return View(gameInWarehouse);
        }

        // GET: GamesInWarehouses/Delete/5
        /// <summary>
        /// Get a game in warehouse to be deleted.
        /// </summary>
        /// <param name="id">Id of an game in warehouse to be deleted.</param>
        /// <returns>The confirmation view or NotFound in case id or game in warehouse is null.</returns>
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var gameInWarehouse = await _bll.GameInWarehouses.FindAsync(id);

            if (gameInWarehouse == null)
            {
                return NotFound();
            }

            return View(gameInWarehouse);
        }

        // POST: GamesInWarehouses/Delete/5
        /// <summary>
        /// Save changes after a game in warehouse has been deleted.
        /// </summary>
        /// <param name="id">Id of a game in warehouse to be deleted.</param>
        /// <returns>Redirects to games in warehouses' main page.</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            _bll.GameInWarehouses.Remove(id);
            await _bll.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}