using System.Threading.Tasks;
using BLL.App.DTO;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace WebApp.Controllers
{
    /// <summary>
    /// Controller containing all the methods for operations with billingStatuses.
    /// </summary>
    [Authorize(Roles = "Admin")]
    public class BillingStatusesController : Controller
    {
        private readonly IAppBLL _bll;

        /// <summary>
        /// Construct and initialize BillingStatusController.
        /// </summary>
        /// <param name="bll">business logic layer</param>
        public BillingStatusesController(IAppBLL bll)
        {
            _bll = bll;
        }

        // GET: BillingStatuses
        /// <summary>
        /// Get all BillingStatuses objects.
        /// </summary>
        /// <returns>Array of all the billing statuses.</returns>
        public async Task<IActionResult> Index()
        {
            return View(await _bll.BillingStatuses.AllAsync());
        }

        // GET: BillingStatuses/Details/5
        /// <summary>
        /// Display details regarding a specific billing status.
        /// </summary>
        /// <param name="id">Id of a specific billing status.</param>
        /// <returns>A specific billing status with all its details.</returns>
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var billingStatus = await _bll.BillingStatuses.FindAsync(id);
            if (billingStatus == null)
            {
                return NotFound();
            }

            return View(billingStatus);
        }

        // GET: BillingStatuses/Create
        /// <summary>
        /// Get the view for creating a new billing status.
        /// </summary>
        /// <returns>The view, where billing status can be created.</returns>
        public IActionResult Create()
        {
            return View();
        }

        // POST: BillingStatuses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// Create a new billing status object.
        /// </summary>
        /// <param name="billingStatus">A billing status containing all the fields required.</param>
        /// <returns>Redirects to billing statuses' main page or the same view, if the form was faulty.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("StatusName,Id")] BillingStatus billingStatus)
        {
            if (ModelState.IsValid)
            {
                _bll.BillingStatuses.Add(billingStatus);
                await _bll.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            return View(billingStatus);
        }

        // GET: BillingStatuses/Edit/5
        /// <summary>
        /// Get a billing status for editing.
        /// </summary>
        /// <param name="id">The id of a billing status to be edited.</param>
        /// <returns>The edit view or NotFound, if given id is null.</returns>
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var billingStatus = await _bll.BillingStatuses.FindAsync(id);
            if (billingStatus == null)
            {
                return NotFound();
            }

            return View(billingStatus);
        }

        // POST: BillingStatuses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// Save changes made to a billing.
        /// </summary>
        /// <param name="id">Id of an edited billing status.</param>
        /// <param name="billingStatus">The billing status object.</param>
        /// <returns>Redirects to billing statuses' main page or stays to the same page, if billing status is not valid.
        /// </returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("StatusName,Id")] BillingStatus billingStatus)
        {
            if (id != billingStatus.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                _bll.BillingStatuses.Update(billingStatus);
                await _bll.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }

            return View(billingStatus);
        }

        // GET: BillingStatuses/Delete/5
        /// <summary>
        /// Get an address to be deleted.
        /// </summary>
        /// <param name="id">Id of a billing status to be deleted.</param>
        /// <returns>The confirmation view or NotFound in case id or billing status is null.</returns>
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var billingStatus = await _bll.BillingStatuses.FindAsync(id);
            if (billingStatus == null)
            {
                return NotFound();
            }

            return View(billingStatus);
        }

        // POST: BillingStatuses/Delete/5
        /// <summary>
        /// Save changes after address has been deleted.
        /// </summary>
        /// <param name="id">Id of a billing status to be deleted.</param>
        /// <returns>Redirects to billing statuses' main page.</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            _bll.BillingStatuses.Remove(id);
            await _bll.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}