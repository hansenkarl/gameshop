using System.Threading.Tasks;
using BLL.App.DTO;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers
{
    /// <summary>
    /// Controller containing all the methods for operations with warehouses.
    /// </summary>
    [Authorize(Roles = "Admin")]
    public class WarehousesController : Controller
    {
        private readonly IAppBLL _bll;

        /// <summary>
        /// Construct and initialize WarehousesController.
        /// </summary>
        /// <param name="bll">business logic layer</param>
        public WarehousesController(IAppBLL bll)
        {
            _bll = bll;
        }

        // GET: Warehouses
        /// <summary>
        /// Get all Warehouses objects that have been created.
        /// </summary>
        /// <returns>Array of all the warehouses.</returns>
        public async Task<IActionResult> Index()
        {
            return View(await _bll.Warehouses.AllAsync());
        }

        // GET: Warehouses/Details/5
        /// <summary>
        /// Display details regarding a specific warehouse.
        /// </summary>
        /// <param name="id">Id of a specific warehouse.</param>
        /// <returns>A specific warehouse with all its details.</returns>
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var warehouse = await _bll.Warehouses.FindAsync(id);
            if (warehouse == null)
            {
                return NotFound();
            }

            return View(warehouse);
        }

        // GET: Warehouses/Create
        /// <summary>
        /// Get the view for creating a new warehouse.
        /// </summary>
        /// <returns>The view, where warehouse can be created.</returns>
        public IActionResult Create()
        {
            return View();
        }

        // POST: Warehouses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// Create a new warehouse object.
        /// </summary>
        /// <param name="warehouse">An warehouse containing all the fields required.</param>
        /// <returns>Redirects to warehouses' main page or the same view, if the form was faulty.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("WarehouseName,Id")] Warehouse warehouse)
        {
            if (ModelState.IsValid)
            {
                _bll.Warehouses.Add(warehouse);
                await _bll.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            return View(warehouse);
        }

        // GET: Warehouses/Edit/5
        /// <summary>
        /// Get a warehouse for editing.
        /// </summary>
        /// <param name="id">The id of a warehouse to be edited.</param>
        /// <returns>The edit view or NotFound, if given id is null.</returns>
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var warehouse = await _bll.Warehouses.FindAsync(id);
            if (warehouse == null)
            {
                return NotFound();
            }

            return View(warehouse);
        }

        // POST: Warehouses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// Save changes made to a warehouse.
        /// </summary>
        /// <param name="id">Id of an edited warehouse.</param>
        /// <param name="warehouse">The warehouse object.</param>
        /// <returns>Redirects to addresses' main page or stays to the same page, if warehouse is not valid.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("WarehouseName,Id")] Warehouse warehouse)
        {
            if (id != warehouse.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                _bll.Warehouses.Update(warehouse);
                await _bll.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            return View(warehouse);
        }

        // GET: Warehouses/Delete/5
        /// <summary>
        /// Get a warehouse to be deleted.
        /// </summary>
        /// <param name="id">Id of a warehouse to be deleted.</param>
        /// <returns>The confirmation view or NotFound in case id or warehouse is null.</returns>
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var warehouse = await _bll.Warehouses.FindAsync(id);
            if (warehouse == null)
            {
                return NotFound();
            }

            return View(warehouse);
        }

        // POST: Warehouses/Delete/5
        /// <summary>
        /// Save changes after warehouse has been deleted.
        /// </summary>
        /// <param name="id">Id of a warehouse to be deleted.</param>
        /// <returns>Redirects to warehouses' main page.</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var warehouse = await _bll.Warehouses.FindAsync(id);
            _bll.Warehouses.Remove(warehouse);
            await _bll.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}