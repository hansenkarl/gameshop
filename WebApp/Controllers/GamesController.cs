using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.App.DTO;
using Contracts.BLL.App;
using Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebApp.Models;

namespace WebApp.Controllers
{
    /// <summary>
    /// Controller containing all the methods for operations with games.
    /// </summary>
    public class GamesController : Controller
    {
        private readonly IAppBLL _bll;

        /// <summary>
        /// Construct and initialize GamesController.
        /// </summary>
        /// <param name="bll">business logic layer</param>
        public GamesController(IAppBLL bll)
        {
            _bll = bll;
        }

        // GET: Games
        /// <summary>
        /// Get all Games objects.
        /// </summary>
        /// <returns>Array of all the Games.</returns>
        public async Task<IActionResult> Index()
        {
            return View(await _bll.Games.AllAsync());
        }

        
        // GET: Games/Details/5
        /// <summary>
        /// Display details regarding a specific game and allow putting the game in basket.
        /// </summary>
        /// <param name="id">Id of a specific game.</param>
        /// <returns>A specific game with all its details and data required to put it in a basket.</returns>
        [Authorize]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }


            var game = await _bll.Games.FindAsync(id);
            if (game == null)
            {
                return NotFound();
            }

            var baskets = await _bll.Baskets.AllForUserAsync(User.GetUserId());

            var putGameInBasketModel = new PutGameInBasketModel()
            {
                Game = game,

                GameInBasket = new GameInBasket
                {
                    Id = 0,
                    Amount = 0,
                    GameId = game.Id,
                    BasketId = 0
                },

                BasketSelectList = new SelectList(
                    await _bll.Baskets.AllNonCheckedOutUserAsync(baskets),
                    nameof(BLL.App.DTO.Basket.Id),
                    nameof(BLL.App.DTO.Basket.Created))
            };


            return View(putGameInBasketModel);
        }

        // GET: Games/Create
        /// <summary>
        /// Get the view for creating a new game.
        /// </summary>
        /// <returns>The view, where game can be created.</returns>
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create()
        {
            var viewModel = new GameRelatedModel()
            {
                GenreSelectList = new SelectList(
                    await _bll.Genres.AllAsync(),
                    nameof(BLL.App.DTO.Genre.Id),
                    nameof(BLL.App.DTO.Genre.GenreName)
                ),

                PlatformSelectList = new SelectList(
                    await _bll.Platforms.AllAsync(),
                    nameof(BLL.App.DTO.Platform.Id),
                    nameof(BLL.App.DTO.Platform.PlatformName)
                ),

                PublisherSelectList = new SelectList(
                    await _bll.Publishers.AllAsync(),
                    nameof(BLL.App.DTO.Publisher.Id),
                    nameof(BLL.App.DTO.Publisher.PublisherName))
            };

            return View(viewModel);
        }

        // POST: Games/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// Create a new game object.
        /// </summary>
        /// <param name="viewModel">A viewModel containing required data to create a game.</param>
        /// <returns>Redirects to games' main page or the same view, if the form was faulty.</returns>
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(GameRelatedModel viewModel)
        {
            if (ModelState.IsValid)
            {
                _bll.Games.Add(viewModel.Game);
                await _bll.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            viewModel.GenreSelectList = new SelectList(
                await _bll.Genres.AllAsync(),
                nameof(BLL.App.DTO.Genre.Id),
                nameof(BLL.App.DTO.Genre.GenreName)
            );

            viewModel.PlatformSelectList = new SelectList(
                await _bll.Platforms.AllAsync(),
                nameof(BLL.App.DTO.Platform.Id),
                nameof(BLL.App.DTO.Platform.PlatformName)
            );

            viewModel.PublisherSelectList = new SelectList(
                await _bll.Publishers.AllAsync(),
                nameof(BLL.App.DTO.Publisher.Id),
                nameof(BLL.App.DTO.Publisher.PublisherName)
            );

            return View(viewModel);
        }

        // GET: Games/Edit/5
        /// <summary>
        /// Get a game for editing.
        /// </summary>
        /// <param name="id">The id of a game to be edited.</param>
        /// <returns>The edit view or NotFound, if given id is null.</returns>
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var game = await _bll.Games.FindAsync(id);
            if (game == null)
            {
                return NotFound();
            }

            return View(game);
        }

        // POST: Games/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// Save changes made to a game.
        /// </summary>
        /// <param name="id">Id of an edited game.</param>
        /// <param name="game">The game object.</param>
        /// <returns>Redirects to games' main page or stays to the same page, if game is not valid.</returns>
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("GameTitle,Price,Pegi,GenreId,PlatformId,PublisherId,Id")]
            Game game)
        {
            if (id != game.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                _bll.Games.Update(game);
                await _bll.SaveChangesAsync();


                return RedirectToAction(nameof(Index));
            }

            return View(game);
        }

        // GET: Games/Delete/5
        /// <summary>
        /// Get a game to be deleted.
        /// </summary>
        /// <param name="id">Id of a game to be deleted.</param>
        /// <returns>The confirmation view or NotFound in case id or game is null.</returns>
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var gamesInWarehouses = await _bll.GameInWarehouses.AllAsync();

            foreach (var gameInWarehouse in gamesInWarehouses)
            {
                if (gameInWarehouse.GameId == id) return Forbid();
            }
            
            var gamesInBaskets = await _bll.GameInBaskets.AllAsync();

            foreach (var gameInBasket in gamesInBaskets)
            {
                if (gameInBasket.GameId == id) return Forbid();
            }
            
            var game = await _bll.Games.FindAsync(id);
            if (game == null)
            {
                return NotFound();
            }

            return View(game);
        }

        // POST: Games/Delete/5
        /// <summary>
        /// Save changes after game has been deleted.
        /// </summary>
        /// <param name="id">Id of a game to be deleted.</param>
        /// <returns>Redirects to addresses' main page.</returns>
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var gamesInWarehouses = await _bll.GameInWarehouses.AllAsync();

            foreach (var gameInWarehouse in gamesInWarehouses)
            {
                if (gameInWarehouse.GameId == id) return Forbid();
            }
            
            var gamesInBaskets = await _bll.GameInBaskets.AllAsync();

            foreach (var gameInBasket in gamesInBaskets)
            {
                if (gameInBasket.GameId == id) return Forbid();
            }
            
            _bll.Games.Remove(id);
            await _bll.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}