using System;
using System.Threading.Tasks;
using BLL.App.DTO;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Mvc;
using Identity;
using Microsoft.AspNetCore.Authorization;

namespace WebApp.Controllers
{
    /// <summary>
    /// Controller containing all the methods for operations with baskets.
    /// </summary>
    [Authorize]
    public class BasketsController : Controller
    {
        private readonly IAppBLL _bll;

        /// <summary>
        /// Construct and initialize BasketsController.
        /// </summary>
        /// <param name="bll">business logic layer</param>
        public BasketsController(IAppBLL bll)
        {
            _bll = bll;
        }

        // GET: Baskets
        /// <summary>
        /// Get all Basket objects for a logged in user.
        /// </summary>
        /// <returns>Array of all the baskets that have been created by the logged in user.</returns>
        public async Task<IActionResult> Index()
        {
            return View(await _bll.Baskets.AllForUserAsync(User.GetUserId()));
        }

        // GET: Baskets/Details/5
        /// <summary>
        /// Display details regarding a specific basket.
        /// </summary>
        /// <param name="id">Id of a specific basket.</param>
        /// <returns>A specific basket with all its details.</returns>
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var gamesInBasket = await _bll.GameInBaskets.AllForBasketAsync(id.Value);

            /*if (!await _bll.GameInBaskets.BelongsToUserAsync(id.Value, User.GetUserId()))
            {
                return Forbid();
            }*/

            return View(gamesInBasket);
        }

        // GET: Baskets/Create
        /// <summary>
        /// Get the view for creating a new basket.
        /// </summary>
        /// <returns>View of confirmation.</returns>
        public IActionResult Create()
        {
            return View();
        }

        // POST: Baskets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// Create a new basket object. All the required data is provided by the server.
        /// </summary>
        /// <param name="basket">A basket containing all thee fields required.</param>
        /// <returns>Redirects to baskets' main page or the same view, if the form was faulty (not possible in normal
        /// cases).</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Basket basket)
        {
            if (ModelState.IsValid)
            {
                basket = await _bll.Baskets.CreateBasket();
                var billing = await _bll.Billings.CreateBilling();

                basket.BillingId = billing.Id;
                billing.BasketId = basket.Id;

                basket.AppUserId = User.GetUserId();
                basket.Created = DateTime.Now;

                billing.Created = basket.Created;

                _bll.Baskets.Update(basket);
                _bll.Billings.Update(billing);

                await _bll.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            return View(basket);
        }

        // GET: Baskets/Edit/5
        /// <summary>
        /// Get a basket for editing.
        /// </summary>
        /// <param name="id">The id of an basket to be edited.</param>
        /// <returns>The edit view or NotFound, if given id is null or forbids
        /// deletion, if basket does not belong to a currently logged in user.</returns>
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var basket = await _bll.Baskets.FindAsync(id);
            if (basket == null)
            {
                return NotFound();
            }

            if (!await _bll.Baskets.BelongsToUserAsync(id.Value, User.GetUserId()))
            {
                return Forbid();
            }

            return View(basket);
        }

        // POST: Baskets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// Save changes made to a basket.
        /// </summary>
        /// <param name="id">Id of an edited basket.</param>
        /// <param name="basket">The basket object.</param>
        /// <returns>Redirects to baskets' main page or stays to the same page, if basket is not valid or forbids
        /// deletion, if basket does not belong to a currently logged in user (editing should not be possible).
        /// </returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Created,AppUserId,BillingId,BasketStatusId,Id")]
            Basket basket)
        {
            if (id != basket.Id)
            {
                return NotFound();
            }

            if (!await _bll.Baskets.BelongsToUserAsync(id, User.GetUserId()))
            {
                return Forbid();
            }

            if (ModelState.IsValid)
            {
                _bll.Baskets.Update(basket);
                await _bll.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            return View(basket);
        }

        // GET: Baskets/Delete/5
        /// <summary>
        /// Get a basket to be deleted.
        /// </summary>
        /// <param name="id">Id of a basket to be deleted.</param>
        /// <returns>The confirmation view or NotFound in case id or basket is null or forbids deletion, if basket
        /// does not belong to a currently logged in user.</returns>
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var basket = await _bll.Baskets.FindForUserAsync(id.Value, User.GetUserId());

            if (basket == null)
            {
                return NotFound();
            }

            if (!await _bll.Baskets.BelongsToUserAsync(id.Value, User.GetUserId()))
            {
                return Forbid();
            }

            if (basket.BasketStatusId != 1) return BadRequest();

            return View(basket);
        }

        // POST: Baskets/Delete/5
        /// <summary>
        /// Save changes after basket has been deleted.
        /// </summary>
        /// <param name="id">Id of a basket to be deleted.</param>
        /// <returns>Redirects to baskets' main page or forbids deletion, if basket does not belong to a currently
        /// logged in user.</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var basket = await _bll.Baskets.GetBasket(id);
            var billing = await _bll.Billings.GetBilling((basket.BillingId.Value));
            var gamesInBasket = await _bll.GameInBaskets.AllForBasketAsync(basket.Id);

            if (basket.BasketStatusId != 1) return BadRequest();

            foreach (var game in gamesInBasket)
            {
                _bll.GameInBaskets.Remove(game.Id);
            }

            if (!await _bll.Baskets.BelongsToUserAsync(id, User.GetUserId()))
            {
                return Forbid();
            }

            _bll.Baskets.Remove(basket.Id);
            _bll.Billings.Remove(billing.Id);
            await _bll.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }
    }
}