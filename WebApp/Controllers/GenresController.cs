using System.Threading.Tasks;
using BLL.App.DTO;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers
{
    /// <summary>
    /// Controller containing all the methods for operations with genres.
    /// </summary>
    [Authorize(Roles = "Admin")]
    public class GenresController : Controller
    {
        private readonly IAppBLL _bll;

        /// <summary>
        /// Construct and initialize GenresController.
        /// </summary>
        /// <param name="bll">business logic layer</param>
        public GenresController(IAppBLL bll)
        {
            _bll = bll;
        }


        // GET: Genres
        /// <summary>
        /// Get all Genres objects that have been created.
        /// </summary>
        /// <returns>Array of all the genres.</returns>
        public async Task<IActionResult> Index()
        {
            return View(await _bll.Genres.AllAsync());
        }

        // GET: Genres/Details/5
        /// <summary>
        /// Display details regarding a specific genre.
        /// </summary>
        /// <param name="id">Id of a specific genre.</param>
        /// <returns>A specific genre with all its details.</returns>
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var genre = await _bll.Genres.FindAsync(id);
            if (genre == null)
            {
                return NotFound();
            }

            return View(genre);
        }

        // GET: Genres/Create
        /// <summary>
        /// Get the view for creating a new genre.
        /// </summary>
        /// <returns>The view, where genre can be created.</returns>
        public IActionResult Create()
        {
            return View();
        }

        // POST: Genres/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// Create a new genre object.
        /// </summary>
        /// <param name="genre">A genre containing all the fields required.</param>
        /// <returns>Redirects to genres' main page or the same view, if the form was faulty.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("GenreName,Id")] Genre genre)
        {
            if (ModelState.IsValid)
            {
                _bll.Genres.Add(genre);
                await _bll.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            return View(genre);
        }

        // GET: Genres/Edit/5
        /// <summary>
        /// Get an genre for editing.
        /// </summary>
        /// <param name="id">The id of an genre to be edited.</param>
        /// <returns>The edit view or NotFound, if given id is null.</returns>
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var genre = await _bll.Genres.FindAsync(id);
            if (genre == null)
            {
                return NotFound();
            }

            return View(genre);
        }

        // POST: Genres/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// Save changes made to an genre.
        /// </summary>
        /// <param name="id">Id of an edited genre.</param>
        /// <param name="genre">The address object.</param>
        /// <returns>Redirects to genres' main page or stays to the same page, if genre is not valid.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("GenreName,Id")] Genre genre)
        {
            if (id != genre.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                _bll.Genres.Update(genre);
                await _bll.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }

            return View(genre);
        }

        // GET: Genres/Delete/5
        /// <summary>
        /// Get a genre to be deleted.
        /// </summary>
        /// <param name="id">Id of a genre to be deleted.</param>
        /// <returns>The confirmation view or NotFound in case id or genre is null.</returns>
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var genre = await _bll.Genres.FindAsync(id);
            if (genre == null)
            {
                return NotFound();
            }

            return View(genre);
        }

        // POST: Genres/Delete/5
        /// <summary>
        /// Save changes after genre has been deleted.
        /// </summary>
        /// <param name="id">Id of an genre to be deleted.</param>
        /// <returns>Redirects to genres' main page.</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            _bll.Genres.Remove(id);
            await _bll.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}