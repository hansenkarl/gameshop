using System.Threading.Tasks;
using BLL.App.DTO;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers
{
    /// <summary>
    /// Controller containing all the methods for operations with platforms.
    /// </summary>
    [Authorize]
    public class PlatformsController : Controller
    {
        private readonly IAppBLL _bll;

        /// <summary>
        /// Construct and initialize PlatformsController.
        /// </summary>
        /// <param name="bll">business logic layer</param>
        public PlatformsController(IAppBLL bll)
        {
            _bll = bll;
        }

        // GET: Platforms
        /// <summary>
        /// Get all Platforms objects that have been created.
        /// </summary>
        /// <returns>Array of all the platforms.</returns>
        public async Task<IActionResult> Index()
        {
            return View(await _bll.Platforms.AllAsync());
        }

        // GET: Platforms/Details/5
        /// <summary>
        /// Display details regarding a specific platform.
        /// </summary>
        /// <param name="id">Id of a specific address.</param>
        /// <returns>A specific address with all its details.</returns>
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var platform = await _bll.Platforms.FindAsync(id);
            if (platform == null)
            {
                return NotFound();
            }

            return View(platform);
        }

        // GET: Platforms/Create
        /// <summary>
        /// Get the view for creating a new platform.
        /// </summary>
        /// <returns>The view, where platform can be created.</returns>
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Platforms/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// Create a new platform object.
        /// </summary>
        /// <param name="platform">An platform containing all the fields required.</param>
        /// <returns>Redirects to platforms' main page or the same view, if the form was faulty.</returns>
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PlatformName,Id")] Platform platform)
        {
            if (ModelState.IsValid)
            {
                _bll.Platforms.Add(platform);
                await _bll.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            return View(platform);
        }

        // GET: Platforms/Edit/5
        /// <summary>
        /// Get a platform for editing.
        /// </summary>
        /// <param name="id">The id of a platform to be edited.</param>
        /// <returns>The edit view or NotFound, if given id is null.</returns>
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var platform = await _bll.Platforms.FindAsync(id);
            if (platform == null)
            {
                return NotFound();
            }

            return View(platform);
        }

        // POST: Platforms/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// Save changes made to an address.
        /// </summary>
        /// <param name="id">Id of an edited address.</param>
        /// <param name="platform">The platform object.</param>
        /// <returns>Redirects to platforms' main page or stays to the same page, if platform is not valid.</returns>
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PlatformName,Id")] Platform platform)
        {
            if (id != platform.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                _bll.Platforms.Update(platform);
                await _bll.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }

            return View(platform);
        }

        // GET: Platforms/Delete/5
        /// <summary>
        /// Get an address to be deleted.
        /// </summary>
        /// <param name="id">Id of a platform to be deleted.</param>
        /// <returns>The confirmation view or NotFound in case id or platform is null.</returns>
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var platform = await _bll.Platforms.FindAsync(id);
            if (platform == null)
            {
                return NotFound();
            }

            return View(platform);
        }

        // POST: Platforms/Delete/5
        /// <summary>
        /// Save changes after platform has been deleted.
        /// </summary>
        /// <param name="id">Id of an platform to be deleted.</param>
        /// <returns>Redirects to platform' main page.</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            _bll.Platforms.Remove(id);
            await _bll.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}