using System;
using System.Threading.Tasks;
using BLL.App.DTO;
using Contracts.BLL.App;
using Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers
{
    /// <summary>
    /// Controller containing all the methods for operations with games in baskets.
    /// </summary>
    public class GamesInBasketsController : Controller
    {
        private readonly IAppBLL _bll;

        /// <summary>
        /// Construct and initialize GamesInBasketsController.
        /// </summary>
        /// <param name="bll">business logic layer</param>
        public GamesInBasketsController(IAppBLL bll)
        {
            _bll = bll;
        }


        // GET: GamesInBaskets
        /// <summary>
        /// Get all GamesInBaskets objects that have been created by the logged in user.
        /// </summary>
        /// <returns>Array of all the gamesInBaskets.</returns>
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Index()
        {
            return View(await _bll.GameInBaskets.AllForUserAsync(User.GetUserId()));
        }

        // GET: GamesInBaskets/Details/5
        /// <summary>
        /// Display details regarding a specific game in basket.
        /// </summary>
        /// <param name="id">Id of a specific game in basket.</param>
        /// <returns>A specific game in basket with all its details.</returns>
        [Authorize(Roles = "Admin, Client")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var gameInBasket = await _bll.GameInBaskets.FindForUserAsync(id.Value, User.GetUserId());
            if (gameInBasket == null)
            {
                return NotFound();
            }
            
            if (!await _bll.GameInBaskets.BelongsToUserAsync(id.Value, User.GetUserId()))
            {
                return Forbid();
            }

            return View(gameInBasket);
        }

        // GET: GamesInBaskets/Create
        /// <summary>
        /// Get the view for creating a new game.
        /// </summary>
        /// <returns>The view, where game can be created.</returns>
        [Authorize(Roles = "Admin, Client")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: GamesInBaskets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// Create a new game in basket object.
        /// </summary>
        /// <param name="gameInBasket">A game in basket containing all the fields required.</param>
        /// <returns>Redirects to game in baskets' main page or the same view, if the form was faulty.</returns>
        [Authorize(Roles = "Admin, Client")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Amount,GameId,BasketId,Id")] GameInBasket gameInBasket)
        {
            if (ModelState.IsValid)
            {
                _bll.GameInBaskets.Add(gameInBasket);
                await _bll.SaveChangesAsync();
                await ReCalculateBilling(gameInBasket.BasketId);
                return RedirectToAction("Index", "Games");
            }

            return View(gameInBasket);
        }

        // GET: GamesInBaskets/Edit/5
        /// <summary>
        /// Get a game in basket for editing.
        /// </summary>
        /// <param name="id">The id of a game in basket to be edited.</param>
        /// <returns>The edit view or NotFound, if given id is null or forbids
        /// deletion, if game in basket does not belong to a currently logged in user.</returns>
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var gameInBasket = await _bll.GameInBaskets.FindForUserAsync(id.Value, User.GetUserId());
            if (gameInBasket == null)
            {
                return NotFound();
            }
            
                        
            if (!await _bll.GameInBaskets.BelongsToUserAsync(id.Value, User.GetUserId()))
            {
                return Forbid();
            }
            
            return View(gameInBasket);
        }

        // POST: GamesInBaskets/Edit/5
        /// <summary>
        /// Save changes made to a game in basket.
        /// </summary>
        /// <param name="id">Id of an edited game in basket.</param>
        /// <param name="gameInBasket">The game in basket object.</param>
        /// <returns>Redirects to game in baskets' main page or stays to the same page, if game in basket
        /// is not valid or forbids deletion, if address does not belong to a currently logged in user.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int id, [Bind("Amount,GameId,BasketId,Id")] GameInBasket gameInBasket)
        {
            if (id != gameInBasket.Id)
            {
                return NotFound();
            }
            
            if (!await _bll.GameInBaskets.BelongsToUserAsync(id, User.GetUserId()))
            {
                return Forbid();
            }

            if (ModelState.IsValid)
            {
                _bll.GameInBaskets.Update(gameInBasket);
                await _bll.SaveChangesAsync();
                await ReCalculateBilling(gameInBasket.BasketId);

                return RedirectToAction(nameof(Index));
            }
            
            return View(gameInBasket);
        }

        // GET: GamesInBaskets/Delete/5
        /// <summary>
        /// Get a game in basket to be deleted.
        /// </summary>
        /// <param name="id">Id of a game in basket to be deleted.</param>
        /// <returns>The confirmation view or NotFound in case id or game in basket is null or forbids deletion, if
        /// game in basket does not belong to a currently logged in user.</returns>
        [Authorize(Roles = "Admin, Client")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var gameInBasket = await _bll.GameInBaskets.FindForUserAsync(id.Value, User.GetUserId());
            if (gameInBasket == null)
            {
                return NotFound();
            }
            
            if (!await _bll.GameInBaskets.BelongsToUserAsync(id.Value, User.GetUserId()))
            {
                return Forbid();
            }

            return View(gameInBasket);
        }

        // POST: GamesInBaskets/Delete/5
        /// <summary>
        /// Save changes after game in basket has been deleted.
        /// </summary>
        /// <param name="id">Id of an game in basket to be deleted.</param>
        /// <returns>Redirects to games in baskets' main page or forbids deletion, if game in basket
        /// does not belong to a currently logged in user.</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin, Client")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (!await _bll.GameInBaskets.BelongsToUserAsync(id, User.GetUserId()))
            {
                return Forbid();
            }
            
            var gameInBasket = await _bll.GameInBaskets.FindForUserAsync(id, User.GetUserId());
            _bll.GameInBaskets.Remove(id);
            await _bll.SaveChangesAsync();
            await ReCalculateBilling(gameInBasket.BasketId);
            return RedirectToAction("Index", "Baskets");
        }
        
        /// <summary>
        /// Recalculates billing after changes.
        /// </summary>
        /// <param name="id">basket id</param>
        [Authorize(Roles = "Admin, Client")]
        public async Task ReCalculateBilling(int id)
        {
            var basket = await _bll.Baskets.GetBasket(id);
            var billing = await _bll.Billings.GetBilling(basket.BillingId.Value);

            var gamesInBasket = await _bll.GameInBaskets.AllForBasketAsync(id);
            var games = await _bll.Games.AllAsync();

            Decimal sumWithoutTax = 0;
            foreach (var gameInBasket in gamesInBasket)
            {
                sumWithoutTax += gameInBasket.Amount * games.Find(game => game.Id == gameInBasket.GameId).Price;
            }

            billing.SumWithoutTax = sumWithoutTax;
            billing.SumTotal = billing.SumWithoutTax * (1 + billing.Tax);

            _bll.Billings.Update(billing);
            await _bll.SaveChangesAsync();
        }

    }
}
