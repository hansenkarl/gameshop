using System.Threading.Tasks;
using BLL.App.DTO;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace WebApp.Controllers
{
    /// <summary>
    /// Controller containing all the methods for operations with basket statuses.
    /// </summary>
    [Authorize(Roles = "Admin")]
    public class BasketStatusesController : Controller
    {
        private readonly IAppBLL _bll;

        /// <summary>
        /// Construct and initialize BasketStatusesController.
        /// </summary>
        /// <param name="bll">business logic layer</param>
        public BasketStatusesController(IAppBLL bll)
        {
            _bll = bll;
        }

        // GET: BasketStatuses
        /// <summary>
        /// Get all BasketStatus objects.
        /// </summary>
        /// <returns>Array of all the basketStatuses.</returns>
        public async Task<IActionResult> Index()
        {
            return View(await _bll.BasketStatuses.AllAsync());
        }

        // GET: BasketStatuses/Details/5
        /// <summary>
        /// Display details regarding a specific basket.
        /// </summary>
        /// <param name="id">Id of a specific basket.</param>
        /// <returns>A specific basket with all its details.</returns>
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var basketStatus = await _bll.BasketStatuses.FindAsync(id);
            if (basketStatus == null)
            {
                return NotFound();
            }

            return View(basketStatus);
        }

        // GET: BasketStatuses/Create
        /// <summary>
        /// Get the view for creating a new basket status.
        /// </summary>
        /// <returns>The view, where basket status can be created.</returns>
        public IActionResult Create()
        {
            return View();
        }

        // POST: BasketStatuses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// Create a new address object.
        /// </summary>
        /// <param name="basketStatus">A basket status containing all the fields required.</param>
        /// <returns>Redirects to basket statuses' main page or the same view, if the form was faulty.</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("StatusName,Id")] BasketStatus basketStatus)
        {
            if (ModelState.IsValid)
            {
                _bll.BasketStatuses.Add(basketStatus);
                await _bll.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            return View(basketStatus);
        }

        // GET: BasketStatuses/Edit/5
        /// <summary>
        /// Get a basket status for editing.
        /// </summary>
        /// <param name="id">The id of a basket status to be edited.</param>
        /// <returns>The edit view or NotFound, if given id is null.</returns>
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var basketStatus = await _bll.BasketStatuses.FindAsync(id);
            if (basketStatus == null)
            {
                return NotFound();
            }

            return View(basketStatus);
        }

        // POST: BasketStatuses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// Save changes made to a basket status.
        /// </summary>
        /// <param name="id">Id of an edited basket status.</param>
        /// <param name="basketStatus">The basket status object.</param>
        /// <returns>Redirects to basket statuses' main page or stays to the same page, if basket status is not valid.
        /// </returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("StatusName,Id")] BasketStatus basketStatus)
        {
            if (id != basketStatus.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                _bll.BasketStatuses.Update(basketStatus);
                await _bll.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            return View(basketStatus);
        }

        // GET: BasketStatuses/Delete/5
        /// <summary>
        /// Get a basket status to be deleted.
        /// </summary>
        /// <param name="id">Id of a basket status to be deleted.</param>
        /// <returns>The confirmation view or NotFound in case id or basket status is null .</returns>
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var basketStatus = await _bll.BasketStatuses.FindAsync(id);
            if (basketStatus == null)
            {
                return NotFound();
            }

            return View(basketStatus);
        }

        // POST: BasketStatuses/Delete/5
        /// <summary>
        /// Save changes after address has been deleted.
        /// </summary>
        /// <param name="id">Id of a basket status to be deleted.</param>
        /// <returns>Redirects to basket statuses' main page.</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            _bll.BasketStatuses.Remove(id);
            await _bll.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}