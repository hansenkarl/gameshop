using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.ApiControllers.v1_0
{
    /// <summary>
    /// API controller containing all the methods for operations with billingStatuses.
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class BillingStatusesController : ControllerBase
    {
        private readonly IAppBLL _bll;

        /// <summary>
        /// Construct and initialize BillingStatusController.
        /// </summary>
        /// <param name="bll">business logic layer</param>
        public BillingStatusesController(IAppBLL bll)
        {
            _bll = bll;
        }


        // GET: api/BillingStatuses
        /// <summary>
        /// Get all BillingStatus objects.
        /// </summary>
        /// <returns>Array of all the billingStatuses.</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PublicApi.v1.DTO.BillingStatus>>> GetBillingStatuses()
        {
            return (await _bll.BillingStatuses.AllAsync())
                .Select(b => PublicApi.v1.Mappers.BillingStatusMapper.MapFromBLL(b)).ToList();
        }

        // GET: api/BillingStatuses/5
        /// <summary>
        /// Get a specific BillingStatus object.
        /// </summary>
        /// <param name="id">an id of a billingStatus</param>
        /// <returns>A billingStatus corresponding to the id or if there is no such billingStatus, then NotFound().
        /// </returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<PublicApi.v1.DTO.BillingStatus>> GetBillingStatus(int id)
        {
            var billingStatus =
                PublicApi.v1.Mappers.BillingStatusMapper.MapFromBLL(await _bll.BillingStatuses.FindAsync(id));

            if (billingStatus == null)
            {
                return NotFound();
            }

            return billingStatus;
        }

        // PUT: api/BillingStatuses/5
        /// <summary>
        /// Update a specific billingStatus, which the user has modified.
        /// </summary>
        /// <param name="id">an id of a billingStatus</param>
        /// <param name="billingStatus">a billingStatus object</param>
        /// <returns>NoContent() or BadRequest() if given id does not match with the basket's id</returns>
        [Authorize(Roles = "Admin")]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBillingStatus(int id, PublicApi.v1.DTO.BillingStatus billingStatus)
        {
            if (id != billingStatus.Id)
            {
                return BadRequest();
            }

            _bll.BillingStatuses.Update(PublicApi.v1.Mappers.BillingStatusMapper.MapFromExternal(billingStatus));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        // POST: api/BillingStatuses
        /// <summary>
        /// Post a new billingStatus.
        /// </summary>
        /// <param name="billingStatus">a billingStatus object</param>
        /// <returns>a created billingStatus</returns>
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<ActionResult<PublicApi.v1.DTO.BillingStatus>> PostBillingStatus(
            PublicApi.v1.DTO.BillingStatus billingStatus)
        {
            _bll.BillingStatuses.Add(PublicApi.v1.Mappers.BillingStatusMapper.MapFromExternal(billingStatus));
            await _bll.SaveChangesAsync();

            return CreatedAtAction(nameof(GetBillingStatus), 
                new
                {
                    version = HttpContext.GetRequestedApiVersion().ToString(),
                    id = billingStatus.Id
                }, billingStatus);
        }

        // DELETE: api/BillingStatuses/5
        /// <summary>
        /// Delete a specific billingStatus.
        /// </summary>
        /// <param name="id">an id of a billingStatus</param>
        /// <returns>NoContent(), if removal was successful. NotFound(), if this specific basketStatus is null.
        /// </returns>
        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteBillingStatus(int id)
        {
            var billingStatus = await _bll.BillingStatuses.FindAsync(id);
            if (billingStatus == null)
            {
                return NotFound();
            }

            _bll.BillingStatuses.Remove(id);
            await _bll.SaveChangesAsync();

            return NoContent();
        }
    }
}