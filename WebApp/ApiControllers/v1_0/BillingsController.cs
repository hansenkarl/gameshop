using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using PublicApi.v1.DTO;
using WebApp.ApiControllers.v1_0.Helpers;

namespace WebApp.ApiControllers.v1_0
{
    /// <summary>
    /// API controller containing all the methods for operations with billings.
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class BillingsController : ControllerBase
    {
        private readonly IAppBLL _bll;

        /// <summary>
        /// Construct and initialize BillingsController.
        /// </summary>
        /// <param name="bll">business logic layer</param>
        public BillingsController(IAppBLL bll)
        {
            _bll = bll;
        }


        // GET: api/Billings
        /// <summary>
        /// Get all Billings objects for a logged in user.
        /// </summary>
        /// <returns>Array of all the billings that have been created by the logged in user.</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PublicApi.v1.DTO.Billing>>> GetBillings()
        {
            return (await _bll.Billings.AllForUserAsync(User.GetUserId()))
                .Select(b => PublicApi.v1.Mappers.BillingMapper.MapFromBLL(b)).ToList();
        }

        // GET: api/Billings/5
        /// <summary>
        /// Get a specific billing object for a logged in user.
        /// </summary>
        /// <param name="id">an id of a billing</param>
        /// <returns>A billing corresponding to the id or if there is no such billing, then NotFound()</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<PublicApi.v1.DTO.Billing>> GetBilling(int id)
        {
            var billing =
                PublicApi.v1.Mappers.BillingMapper.MapFromBLL(
                    await _bll.Billings.FindForUserAsync(id, User.GetUserId()));
            
            var ccSelection = new SelectLists()
            {
                CreditCardSelectList = new SelectList(
                    await _bll.CreditCards.AllForUserAsync(id),
                    nameof(CreditCard.Id),
                    nameof(CreditCard.CreditCardNr))
            };


            if (billing == null)
            {
                return NotFound();
            }

            return billing;
        }

        // PUT: api/Billings/5
        /// <summary>
        /// Update a specific billing, which the user has modified.
        /// </summary>
        /// <param name="id">an id of a billing</param>
        /// <param name="billing">a billing object</param>
        /// <returns>NoContent(), NotFound(), if billing does not belong to the user or BadRequest(), if given
        /// id does not match with the basket's id</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBilling(int id, PublicApi.v1.DTO.Billing billing)
        {
            var appuser = User.GetUserId();

            if (id != billing.Id)
            {
                return BadRequest();
            }

            if (billing.CreditCardId == null || billing.CreditCardId == 0)
            {
                return BadRequest();
            }

            if (!await _bll.Billings.BelongsToUserAsync(id, appuser))
            {
                return NotFound();
            }

            var creditCardId = billing.CreditCardId;
            var newBilling = await _bll.Billings.GetBilling(id);
            var basket = await _bll.Baskets.GetBasket((int)newBilling.BasketId);
            
            if (newBilling.BillingStatusId != 1) return BadRequest();
            if (basket.BasketStatusId != 1) return BadRequest();
            
            newBilling.CreditCardId = creditCardId;
            newBilling.BillingStatusId = 2;
            basket.BasketStatusId = 2;
            
            _bll.Billings.Update(newBilling);
            _bll.Baskets.Update(basket);
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        // POST: api/Billings
        /// <summary>
        /// Post a new basket for a logged in user.
        /// </summary>
        /// <param name="billing">a billing object</param>
        /// <returns>a created billing</returns>
        [HttpPost]
        public async Task<ActionResult<PublicApi.v1.DTO.Billing>> PostBilling(PublicApi.v1.DTO.Billing billing)
        {
            if (!await _bll.Baskets.BelongsToUserAsync(billing.Basket.AppUserId, User.GetUserId()))
            {
                return NotFound();
            }
            
            _bll.Billings.Add(PublicApi.v1.Mappers.BillingMapper.MapFromExternal(billing));
            await _bll.SaveChangesAsync();

            return CreatedAtAction(nameof(GetBilling), 
                new
                {
                    version = HttpContext.GetRequestedApiVersion().ToString(),
                    id = billing.Id
                }, billing);
        }

        // DELETE: api/Billings/5
        /// <summary>
        /// Delete a specific billing, which has been created by a logged in user.
        /// </summary>
        /// <param name="id">an id of a billing</param>
        /// <returns>NoContent(), if removal was successful. NotFound(), if this specific billing does not belong to
        /// that specific logged in user.</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteBilling(int id)
        {
            if (!await _bll.Billings.BelongsToUserAsync(id, User.GetUserId()))
            {
                return NotFound();
            }

            _bll.Billings.Remove(id);
            await _bll.SaveChangesAsync();

            return NoContent();
        }
    }
}