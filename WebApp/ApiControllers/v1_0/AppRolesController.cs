using Contracts.BLL.App;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;


namespace WebApp.ApiControllers.v1_0
{
    /// <summary>
    /// API controller to handle getting app role for a user.
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class AppRolesController : ControllerBase
    {
        private IAppBLL _bll;

        /// <summary>
        /// Construct and initialize AppRolesController.
        /// </summary>
        /// <param name="bll">business logic layer</param>
        public AppRolesController(IAppBLL bll)
        {
            _bll = bll;
        }

        /// <summary>
        /// Gets the user's app role.
        /// </summary>
        /// <returns>Is user allowed to view this particular page? Is user in admin's role?</returns>
        [HttpGet]
        public ActionResult<bool> GetUserAppRole()
        {
            return Ok(User.IsInRole("Admin"));
        }
    }
}