using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Domain;
using Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebApp.ApiControllers.v1_0.Helpers;

namespace WebApp.ApiControllers.v1_0
{
    /// <summary>
    /// API controller containing all the methods for operations with reviews.
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ReviewsController : ControllerBase
    {
        private readonly IAppBLL _bll;

        /// <summary>
        /// Construct and initialize ReviewsController.
        /// </summary>
        /// <param name="bll">business logic layer</param>
        public ReviewsController(IAppBLL bll)
        {
            _bll = bll;
        }


        // GET: api/Reviews
        /// <summary>
        /// Get all Reviews objects that have been created.
        /// </summary>
        /// <returns>Array of all the reviews.</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PublicApi.v1.DTO.Review>>> GetReviews()
        {
            return (await _bll.Reviews.AllAsync())
                .Select(p => PublicApi.v1.Mappers.ReviewMapper.MapFromBLL(p)).ToList();
        }

        // GET: api/Reviews/5
        /// <summary>
        /// Get a specific reviews object.
        /// </summary>
        /// <param name="id">an id of a review</param>
        /// <returns>A review corresponding to the id or if there is no such platform, then NotFound().</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<PublicApi.v1.DTO.Review>> GetReview(int id)
        {
            var review = PublicApi.v1.Mappers.ReviewMapper.MapFromBLL(await _bll.Reviews.FindAsync(id));

            new SelectLists()
            {
                GameSelectList = new SelectList(
                    await _bll.Games.AllAsync(),
                    nameof(Game.Id),
                    nameof(Game.GameTitle)),
                WarehouseSelectList = new SelectList(
                    await _bll.Warehouses.AllAsync(),
                    nameof(Warehouse.Id),
                    nameof(Warehouse.WarehouseName))
            };
            
            if (review == null)
            {
                return NotFound();
            }

            return review;
        }

        // PUT: api/Reviews/5
        /// <summary>
        /// Update a specific review, which the user has modified.
        /// </summary>
        /// <param name="id">an id of a review</param>
        /// <param name="review">a review object</param>
        /// <returns>NoContent(), or BadRequest(), if given id does not match with the review's id.</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutReview(int id, PublicApi.v1.DTO.Review review)
        {
            var appuser = User.GetUserId();

            if (id != review.Id)
            {
                return BadRequest();
            }

            review.AppUserId = appuser;

            _bll.Reviews.Update(PublicApi.v1.Mappers.ReviewMapper.MapFromExternal(review));
            await _bll.SaveChangesAsync();
            
            return NoContent();
        }

        // POST: api/Reviews
        /// <summary>
        /// Post a new review.
        /// </summary>
        /// <param name="review">a review object</param>
        /// <returns>a created review</returns>
        [HttpPost]
        public async Task<ActionResult<PublicApi.v1.DTO.Review>> PostReview(PublicApi.v1.DTO.Review review)
        {
            review.AppUserId = User.GetUserId();
            
            review.Added = DateTime.Now;
            
            _bll.Reviews.Add(PublicApi.v1.Mappers.ReviewMapper.MapFromExternal(review));
            await _bll.SaveChangesAsync();

            return CreatedAtAction(nameof(GetReview), 
                new
                {
                    version = HttpContext.GetRequestedApiVersion().ToString(),
                    id = review.Id
                }, review);
        }

        // DELETE: api/Reviews/5
        /// <summary>
        /// Delete a specific review, which has been created by a logged in user.
        /// </summary>
        /// <param name="id">an id of a review</param>
        /// <returns>NoContent(), if removal was successful. NotFound(), if this specific review is null.</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteReview(int id)
        {
            var review = await _bll.Reviews.FindAsync(id);
            if (review == null)
            {
                return NotFound();
            }

            _bll.Reviews.Remove(id);
            await _bll.SaveChangesAsync();

            return NoContent();
        }
    }
}
