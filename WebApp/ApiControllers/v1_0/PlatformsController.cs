using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.ApiControllers.v1_0
{
    /// <summary>
    /// API controller containing all the methods for operations with platforms.
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PlatformsController : ControllerBase
    {
        private readonly IAppBLL _bll;

        /// <summary>
        /// Construct and initialize PlatformsController.
        /// </summary>
        /// <param name="bll">business logic layer</param>
        public PlatformsController(IAppBLL bll)
        {
            _bll = bll;
        }


        // GET: api/Platforms
        /// <summary>
        /// Get all Platforms objects that have been created.
        /// </summary>
        /// <returns>Array of all the platforms.</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PublicApi.v1.DTO.Platform>>> GetPlatforms()
        {
            return (await _bll.Platforms.AllAsync()).
                Select(p => PublicApi.v1.Mappers.PlatformMapper.MapFromBLL(p)).ToList();
        }

        // GET: api/Platforms/5
        /// <summary>
        /// Get a specific platforms object.
        /// </summary>
        /// <param name="id">an id of a platform</param>
        /// <returns>A platform corresponding to the id or if there is no such platform, then NotFound().</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<PublicApi.v1.DTO.Platform>> GetPlatform(int id)
        {
            var platform = PublicApi.v1.Mappers.PlatformMapper.MapFromBLL(await _bll.Platforms.FindAsync(id));

            if (platform == null)
            {
                return NotFound();
            }

            return platform;
        }

        // PUT: api/Platforms/5
        /// <summary>
        /// Update a specific platform, which the user has modified.
        /// </summary>
        /// <param name="id">an id of a platform</param>
        /// <param name="platform">a platform object</param>
        /// <returns>NoContent(), or BadRequest(), if given id does not match with the platform's id.</returns>
        [Authorize(Roles = "Admin")]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPlatform(int id, PublicApi.v1.DTO.Platform platform)
        {
            if (id != platform.Id)
            {
                return BadRequest();
            }

            _bll.Platforms.Update(PublicApi.v1.Mappers.PlatformMapper.MapFromExternal(platform));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        // POST: api/Platforms
        /// <summary>
        /// Post a new platform.
        /// </summary>
        /// <param name="platform">a platform object</param>
        /// <returns>a created platform</returns>
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<ActionResult<PublicApi.v1.DTO.Platform>> PostPlatform(PublicApi.v1.DTO.Platform platform)
        {
            _bll.Platforms.Add(PublicApi.v1.Mappers.PlatformMapper.MapFromExternal(platform));
            await _bll.SaveChangesAsync();

            return CreatedAtAction(nameof(GetPlatform), 
                new
                {
                    version = HttpContext.GetRequestedApiVersion().ToString(),
                    id = platform.Id
                }, platform);
        }

        // DELETE: api/Platforms/5
        /// <summary>
        /// Delete a specific platform, which has been created by a logged in user.
        /// </summary>
        /// <param name="id">an id of a platform</param>
        /// <returns>NoContent(), if removal was successful. NotFound(), if this specific platform is null.</returns>
        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeletePlatform(int id)
        {
            var platform = await _bll.Platforms.FindAsync(id);
            if (platform == null)
            {
                return NotFound();
            }

            _bll.Platforms.Remove(id);
            await _bll.SaveChangesAsync();

            return NoContent();
        }
    }
}