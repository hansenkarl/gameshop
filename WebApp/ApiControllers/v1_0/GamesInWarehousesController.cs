using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using PublicApi.v1.DTO;
using WebApp.ApiControllers.v1_0.Helpers;

namespace WebApp.ApiControllers.v1_0
{
    /// <summary>
    /// API controller containing all the methods for operations with games in warehouses.
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class GamesInWarehousesController : ControllerBase
    {
        private readonly IAppBLL _bll;

        /// <summary>
        /// Construct and initialize GamesInWarehousesController.
        /// </summary>
        /// <param name="bll">business logic layer</param>
        public GamesInWarehousesController(IAppBLL bll)
        {
            _bll = bll;
        }


        // GET: api/GamesInWarehouses
        /// <summary>
        ///  Get all GamesInWarehouses objects.
        /// </summary>
        /// <returns>Array of all the gamesInWarehouses.</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PublicApi.v1.DTO.GameInWarehouse>>> GetGamesInWarehouses()
        {
            return (await _bll.GameInWarehouses.AllAsync())
                .Select(g => PublicApi.v1.Mappers.GameInWarehouseMapper.MapFromBLL(g)).ToList();
        }

        // GET: api/GamesInWarehouses/5
        /// <summary>
        /// Get a specific gamesInWarehouses object.
        /// </summary>
        /// <param name="id">an id of a game in warehouse</param>
        /// <returns>A gameInWarehouse corresponding to the id or if there is no such gameInWarehouse, then NotFound().
        /// </returns>
        [Authorize(Roles = "Admin")]
        [HttpGet("{id}")]
        public async Task<ActionResult<PublicApi.v1.DTO.GameInWarehouse>> GetGameInWarehouse(int id)
        {
            var gameInWarehouse =
                PublicApi.v1.Mappers.GameInWarehouseMapper.MapFromBLL(await _bll.GameInWarehouses.FindAsync(id));

            new SelectLists()
            {
                GameSelectList = new SelectList(
                    await _bll.Games.AllAsync(),
                    nameof(Game.Id),
                    nameof(Game.GameTitle)
                ),
                
                WarehouseSelectList = new SelectList(
                    await _bll.Warehouses.AllAsync(),
                    nameof(Warehouse.Id),
                    nameof(Warehouse.WarehouseName)
                    )
            };
            if (gameInWarehouse == null)
            {
                return NotFound();
            }

            return gameInWarehouse;
        }

        // PUT: api/GamesInWarehouses/5
        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        /// <param name="gameInWarehouse"></param>
        /// <returns>NoContent(), NotFound(), if gameInWarehouse does not belong to the user or BadRequest(), if given
        /// id does not match with the gameInWarehouse's id.</returns>
        [Authorize(Roles = "Admin")]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutGameInWarehouse(int id, PublicApi.v1.DTO.GameInWarehouse gameInWarehouse)
        {
            if (id != gameInWarehouse.Id)
            {
                return BadRequest();
            }

            _bll.GameInWarehouses.Update(
                PublicApi.v1.Mappers.GameInWarehouseMapper.MapFromExternal(gameInWarehouse));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        // POST: api/GamesInWarehouses
        /// <summary>
        /// Post a new gameInWarehouse for a logged in user.
        /// </summary>
        /// <param name="gameInWarehouse">a gameInWarehouse object</param>
        /// <returns>a created gameInWarehouse</returns>
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<ActionResult<PublicApi.v1.DTO.GameInWarehouse>> PostGameInWarehouse(
            PublicApi.v1.DTO.GameInWarehouse gameInWarehouse)
        {
            gameInWarehouse.Added = DateTime.Now;
            
            
            _bll.GameInWarehouses.Add(PublicApi.v1.Mappers.GameInWarehouseMapper.MapFromExternal(gameInWarehouse));
            await _bll.SaveChangesAsync();

            return CreatedAtAction(nameof(GetGameInWarehouse),
                new
                {
                    version = HttpContext.GetRequestedApiVersion().ToString(),
                    id = gameInWarehouse.Id
                }, gameInWarehouse);
        }

        // DELETE: api/GamesInWarehouses/5
        /// <summary>
        /// Delete a specific gameInWarehouse, which has been created by a logged in user.
        /// </summary>
        /// <param name="id">an id of a gameInWarehouse</param>
        /// <returns>NoContent(), if removal was successful. NotFound(), if this specific gameInWarehouse does not
        /// belong to that specific logged in user.</returns>
        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteGameInWarehouse(int id)
        {
            var gameInWarehouse = await _bll.GameInWarehouses.FindAsync(id);
            if (gameInWarehouse == null)
            {
                return NotFound();
            }

            _bll.GameInWarehouses.Remove(id);
            await _bll.SaveChangesAsync();

            return NoContent();
        }
    }
}