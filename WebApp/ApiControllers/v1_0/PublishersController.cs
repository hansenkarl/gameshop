using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.ApiControllers.v1_0
{
    /// <summary>
    /// API controller containing all the methods for operations with publishers.
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PublishersController : ControllerBase
    {
        private readonly IAppBLL _bll;

        /// <summary>
        /// Construct and initialize PublishersController.
        /// </summary>
        /// <param name="bll">business logic layer</param>
        public PublishersController(IAppBLL bll)
        {
            _bll = bll;
        }


        // GET: api/Publishers
        /// <summary>
        /// Get all Publishers objects that have been created.
        /// </summary>
        /// <returns>Array of all the publishers.</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PublicApi.v1.DTO.Publisher>>> GetPublishers()
        {
            return (await _bll.Publishers.AllAsync())
                .Select(p => PublicApi.v1.Mappers.PublisherMapper.MapFromBLL(p)).ToList();
        }

        // GET: api/Publishers/5
        /// <summary>
        /// Get a specific publishers object.
        /// </summary>
        /// <param name="id">an id of a publisher</param>
        /// <returns>A publisher corresponding to the id or if there is no such platform, then NotFound().</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<PublicApi.v1.DTO.Publisher>> GetPublisher(int id)
        {
            var publisher = PublicApi.v1.Mappers.PublisherMapper.MapFromBLL(await _bll.Publishers.FindAsync(id));

            if (publisher == null)
            {
                return NotFound();
            }

            return publisher;
        }

        // PUT: api/Publishers/5
        /// <summary>
        /// Update a specific publisher, which the user has modified.
        /// </summary>
        /// <param name="id">an id of a publisher</param>
        /// <param name="publisher">a publisher object</param>
        /// <returns>NoContent(), or BadRequest(), if given id does not match with the publisher's id.</returns>
        [Authorize(Roles = "Admin")]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPublisher(int id, PublicApi.v1.DTO.Publisher publisher)
        {
            if (id != publisher.Id)
            {
                return BadRequest();
            }

            _bll.Publishers.Update(PublicApi.v1.Mappers.PublisherMapper.MapFromExternal(publisher));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        // POST: api/Publishers
        /// <summary>
        /// Post a new publisher.
        /// </summary>
        /// <param name="publisher">a publisher object</param>
        /// <returns>a created publisher</returns>
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<ActionResult<PublicApi.v1.DTO.Publisher>> PostPublisher(PublicApi.v1.DTO.Publisher publisher)
        {
            _bll.Publishers.Add(PublicApi.v1.Mappers.PublisherMapper.MapFromExternal(publisher));
            await _bll.SaveChangesAsync();

            return CreatedAtAction(nameof(GetPublisher), 
                new
                {
                    version = HttpContext.GetRequestedApiVersion().ToString(),
                    id = publisher.Id
                }, publisher);
        }

        // DELETE: api/Publishers/5
        /// <summary>
        /// Delete a specific publisher, which has been created by a logged in user.
        /// </summary>
        /// <param name="id">an id of a publisher</param>
        /// <returns>NoContent(), if removal was successful. NotFound(), if this specific publisher is null.</returns>
        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeletePublisher(int id)
        {
            var publisher = await _bll.Publishers.FindAsync(id);
            if (publisher == null)
            {
                return NotFound();
            }

            _bll.Publishers.Remove(id);
            await _bll.SaveChangesAsync();

            return NoContent();
        }
    }
}