using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.ApiControllers.v1_0
{
    /// <summary>
    /// API controller containing all the methods for operations with genres.
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class GenresController : ControllerBase
    {
        private readonly IAppBLL _bll;

        /// <summary>
        /// Construct and initialize GenresController.
        /// </summary>
        /// <param name="bll">business logic layer</param>
        public GenresController(IAppBLL bll)
        {
            _bll = bll;
        }


        // GET: api/Genres
        /// <summary>
        /// Get all Genres objects that have been created.
        /// </summary>
        /// <returns>Array of all the genres.</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PublicApi.v1.DTO.Genre>>> GetGenres()
        {
            return (await _bll.Genres.AllAsync())
                .Select(g => PublicApi.v1.Mappers.GenreMapper.MapFromBLL(g)).ToList();
        }

        // GET: api/Genres/5
        /// <summary>
        /// Get a specific genres object.
        /// </summary>
        /// <param name="id">an id of a genre</param>
        /// <returns>A genre corresponding to the id or if there is no such genre, then NotFound().</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<PublicApi.v1.DTO.Genre>> GetGenre(int id)
        {
            var genre = PublicApi.v1.Mappers.GenreMapper.MapFromBLL(await _bll.Genres.FindAsync(id));

            if (genre == null)
            {
                return NotFound();
            }

            return genre;
        }

        // PUT: api/Genres/5
        /// <summary>
        /// Update a specific genre, which the user has modified.
        /// </summary>
        /// <param name="id">an id of a genre</param>
        /// <param name="genre">a genre object</param>
        /// <returns>NoContent() or BadRequest(), if given id does not match with the genre's id.</returns>
        [Authorize(Roles = "Admin")]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutGenre(int id, PublicApi.v1.DTO.Genre genre)
        {
            if (id != genre.Id)
            {
                return BadRequest();
            }

            _bll.Genres.Update(PublicApi.v1.Mappers.GenreMapper.MapFromExternal(genre));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        // POST: api/Genres
        /// <summary>
        /// Post a new genre.
        /// </summary>
        /// <param name="genre">>a genre object</param>
        /// <returns>a created genre</returns>
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<ActionResult<PublicApi.v1.DTO.Genre>> PostGenre(PublicApi.v1.DTO.Genre genre)
        {
            _bll.Genres.Add(PublicApi.v1.Mappers.GenreMapper.MapFromExternal(genre));
            await _bll.SaveChangesAsync();

            return CreatedAtAction(nameof(GetGenre), 
                new
                {
                    version = HttpContext.GetRequestedApiVersion().ToString(),
                    id = genre.Id
                }, genre);
        }

        // DELETE: api/Genres/5
        /// <summary>
        /// Delete a specific genre, which has been created by a logged in user.
        /// </summary>
        /// <param name="id">an id of a genre</param>
        /// <returns>NoContent(), if removal was successful. NotFound(), if this specific genre is null.</returns>
        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteGenre(int id)
        {
            var genre = await _bll.Genres.FindAsync(id);
            if (genre == null)
            {
                return NotFound();
            }

            _bll.Genres.Remove(id);
            await _bll.SaveChangesAsync();

            return NoContent();
        }
    }
}