using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.ApiControllers.v1_0
{
    /// <summary>
    /// API controller containing all the methods for operations with addresses.
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class AddressesController : ControllerBase
    {
        private readonly IAppBLL _bll;

        /// <summary>
        /// Construct and initialize AddressesController.
        /// </summary>
        /// <param name="bll">business logic layer</param>
        public AddressesController(IAppBLL bll)
        {
            _bll = bll;
        }

        // GET: api/Addresses
        /// <summary>
        /// Get all Address objects for a logged in user.
        /// </summary>
        /// <returns>Array of all the addresses that have been created by the logged in user.</returns>
        /// <response code="200">The array of Addresses was successfully retrieved.</response>
        [ProducesResponseType(typeof(IEnumerable<PublicApi.v1.DTO.Address>),
            StatusCodes.Status200OK)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PublicApi.v1.DTO.Address>>> GetAddresses()
        {
            return (await _bll.Addresses.AllForUserAsync(User.GetUserId()))
                .Select(a => PublicApi.v1.Mappers.AddressMapper.MapFromBLL(a)).ToList();
        }

        // GET: api/Addresses/5
        /// <summary>
        /// Get a specific address object for a logged in user.
        /// </summary>
        /// <param name="id">an id of an address</param>
        /// <returns>An address corresponding to the id or if there is no such address, then NotFound().</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<PublicApi.v1.DTO.Address>> GetAddress(int id)
        {
            var address =
                PublicApi.v1.Mappers.AddressMapper.MapFromBLL(
                    await _bll.Addresses.FindForUserAsync(id, User.GetUserId()));

            if (address == null)
            {
                return NotFound();
            }

            return address;
        }

        // PUT: api/Addresses/5
        /// <summary>
        /// Update a specific address, which the user has modified.
        /// </summary>
        /// <param name="id">an id of an address</param>
        /// <param name="address">an address object</param>
        /// <returns>NoContent(), NotFound(), if address does not belong to the user or BadRequest(), if given
        /// id does not match with the address' id</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAddress(int id, PublicApi.v1.DTO.Address address)
        {
            var appuser = User.GetUserId();

            if (id != address.Id)
            {
                return BadRequest();
            }

            if (!await _bll.Addresses.BelongsToUserAsync(id, appuser))
            {
                return NotFound();
            }

            address.AppUserId = appuser;

            _bll.Addresses.Update(PublicApi.v1.Mappers.AddressMapper.MapFromExternal(address));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        // POST: api/Addresses
        /// <summary>
        /// Post a new address for a logged in user.
        /// </summary>
        /// <param name="address">an address object</param>
        /// <returns>a created address.</returns>
        [HttpPost]
        public async Task<ActionResult<PublicApi.v1.DTO.Address>> PostAddress(PublicApi.v1.DTO.Address address)
        {
            address.AppUserId = User.GetUserId();

            _bll.Addresses.Add(PublicApi.v1.Mappers.AddressMapper.MapFromExternal(address));
            await _bll.SaveChangesAsync();

            return CreatedAtAction(nameof(GetAddress),
                new
                {
                    version = HttpContext.GetRequestedApiVersion().ToString(),
                    id = address.Id
                }, address);
        }

        // DELETE: api/Addresses/5
        /// <summary>
        /// Delete a specific address, which has been created by a logged in user.
        /// </summary>
        /// <param name="id">an id of an address</param>
        /// <returns>NoContent(), if removal was successful. NotFound(), if this specific address does not belong to
        /// that specific logged in user.</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteAddress(int id)
        {
            if (!await _bll.Addresses.BelongsToUserAsync(id, User.GetUserId()))
            {
                return NotFound();
            }

            _bll.Addresses.Remove(id);
            await _bll.SaveChangesAsync();

            return NoContent();
        }
    }
}