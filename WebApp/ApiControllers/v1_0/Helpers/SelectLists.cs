using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApp.ApiControllers.v1_0.Helpers
{
    /// <summary>
    /// A small class containing several SelectLists of objects, which are required in order to create or edit certain
    /// objects.
    /// </summary>
    public class SelectLists
    {
        /// <summary>
        /// SelectList that would contain credit cards.
        /// </summary>
        public SelectList CreditCardSelectList { get; set; }
        
        /// <summary>
        /// SelectList that would contain games.
        /// </summary>
        public SelectList GameSelectList { get; set; }
        
        /// <summary>
        /// SelectList that would contain genres.
        /// </summary>
        public SelectList GenreSelectList { get; set; }
        
        
        /// <summary>
        /// SelectList that would contain platforms.
        /// </summary>
        public SelectList PlatformSelectList { get; set; }
        
        
        /// <summary>
        /// SelectList that would contain publishers.
        /// </summary>
        public SelectList PublisherSelectList { get; set; }
        
        
        /// <summary>
        /// SelectList that would contain warehouses.
        /// </summary>
        public SelectList WarehouseSelectList { get; set; }
    }
}