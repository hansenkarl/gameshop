using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.ApiControllers.v1_0
{
    /// <summary>
    /// Retrieves necessary data regarding app users.
    /// </summary>
    ///     [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class AppUsersController : ControllerBase
    {
        private IAppBLL _bll;

        /// <summary>
        /// Construct and initialize AppUsersController.
        /// </summary>
        /// <param name="bll"></param>
        public AppUsersController(IAppBLL bll)
        {
            _bll = bll;
        }

        /// <summary>
        /// Get all appUsers that are on the site.
        /// </summary>
        /// <returns>Array of all the appUsers.</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PublicApi.v1.DTO.Identity.AppUser>>> GetAppUsers()
        {
            return (await _bll.AppUsers.AllAsync()).Select(a => PublicApi.v1.Mappers.AppUserMapper.MapFromBLL(a))
                .ToList();
        }

        /// <summary>
        /// Get an appUser by their unique id.
        /// </summary>
        /// <param name="id">appUsers id</param>
        /// <returns>appUser or NotFound if such appUser does not exist</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<PublicApi.v1.DTO.Identity.AppUser>> GetAppUser(int id)
        {
            var appUser = PublicApi.v1.Mappers.AppUserMapper.MapFromBLL(await _bll.AppUsers.FindAsync(id));

            if (appUser == null)
            {
                return NotFound();
            }

            return appUser;
        }
    }
}