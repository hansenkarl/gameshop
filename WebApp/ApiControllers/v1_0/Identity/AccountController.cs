using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;
using Domain.Identity;
using Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using WebApp.Areas.Identity.Pages.Account;

namespace WebApp.ApiControllers.v1_0.Identity
{
    /// <summary>
    /// A class containing all the operations regarding account creation and logging in.
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]/[action]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly SignInManager<AppUser> _signInManager;
        private readonly UserManager<AppUser> _userManager;
        private readonly ILogger<RegisterModel> _logger;
        private readonly IEmailSender _emailSender;
        private readonly IConfiguration _configuration;

        /// <summary>
        /// Construct and initialize AccountController.
        /// </summary>
        /// <param name="signInManager">Manage signing in for users.</param>
        /// <param name="configuration">Represents a set of key/value application configuration properties.</param>
        /// <param name="userManager">Provides the APIs for managing users in a persistence store.</param>
        /// <param name="logger">A generic interface for logging.</param>
        /// <param name="emailSender">Sends emails to users.</param>
        public AccountController(SignInManager<AppUser> signInManager, IConfiguration configuration, 
            UserManager<AppUser> userManager, ILogger<RegisterModel> logger, IEmailSender emailSender)
        {
            _signInManager = signInManager;
            _configuration = configuration;
            _userManager = userManager;
            _logger = logger;
            _emailSender = emailSender;
        }

        /// <summary>
        /// User login happens here.
        /// </summary>
        /// <param name="model">A model consisting of LoginDTO.</param>
        /// <returns>Ok or StatusCode(403)</returns>
        [HttpPost]
        public async Task<ActionResult<string>> Login([FromBody] LoginDTO model)
        {
            
            var appUser = await _userManager.FindByEmailAsync(model.Email);
            
            if (appUser == null)
            {
                // user is not found, return 403
                _logger.LogInformation("User not found.");
                return StatusCode(403);
            }
            
            // do not log user in, just check that the password is ok
            var result = await _signInManager.CheckPasswordSignInAsync(appUser, model.Password, false);

            if (result.Succeeded)
            {
                // create claims based user 
                var claimsPrincipal = await _signInManager.CreateUserPrincipalAsync(appUser);
          
                // get the Json Web Token
                var jwt = JwtHelper.GenerateJwt(
                    claimsPrincipal.Claims, 
                    _configuration["JWT:Key"], 
                    _configuration["JWT:Issuer"], 
                    int.Parse(_configuration["JWT:ExpireDays"]));
                _logger.LogInformation("Token generated for user");
                return Ok(new {token = jwt});
            }

            return StatusCode(403);
        }
        
        /// <summary>
        /// Registering user happens here.
        /// </summary>
        /// <param name="model">A model consisting of RegisterDTO.</param>
        /// <returns>Ok, StatusCode(406) or StatusCode(400).</returns>
        [HttpPost]
        public async Task<ActionResult<string>> Register([FromBody] RegisterDTO model)
        {
            if (ModelState.IsValid)
            {
                var appUser = new AppUser
                {
                    UserName = model.Email,
                    Email = model.Email,
                    FirstName = model.FirstName,
                    LastName = model.LastName
                };
                var result = await _userManager.CreateAsync(appUser, model.Password);
                if (result.Succeeded)
                {
                    var currentUser = await _userManager.FindByNameAsync(appUser.UserName); 

                    var roleResult = await _userManager.AddToRoleAsync(currentUser, "Client");
                    
                    _logger.LogInformation("New user created.");
                   
                    // create claims based user 
                    var claimsPrincipal = await _signInManager.CreateUserPrincipalAsync(appUser);
          
                    // get the Json Web Token
                    var jwt = JwtHelper.GenerateJwt(
                        claimsPrincipal.Claims, 
                        _configuration["JWT:Key"], 
                        _configuration["JWT:Issuer"], 
                        int.Parse(_configuration["JWT:ExpireDays"]));
                    _logger.LogInformation("Token generated for user");
                    return Ok(new {token = jwt});
                    
                }
                return StatusCode(406); //406 Not Acceptable
            }
            
            return StatusCode(400); //400 Bad Request
        }


        /// <summary>
        /// LoginDTO class.
        /// </summary>
        public class LoginDTO
        {
            /// <summary>
            /// User's email.
            /// </summary>
            public string Email { get; set; }
            
            /// <summary>
            /// User's password.
            /// </summary>
            public string Password { get; set; }
        }

        /// <summary>
        /// RegisterDTO class.
        /// </summary>
        public class RegisterDTO
        {
            /// <summary>
            /// User's email.
            /// </summary>
            public string Email { get; set; }
            
            /// <summary>
            /// User's firstname.
            /// </summary>
            [Required]
            public string FirstName { get; set; }
            
            /// <summary>
            /// User's last name.
            /// </summary>
            [Required]
            public string LastName { get; set; }
            
            /// <summary>
            /// User's password.
            /// </summary>
            [Required]
            [MinLength(6)]
            public string Password { get; set; }
        }
        
        
    }
}   