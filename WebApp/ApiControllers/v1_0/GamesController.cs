using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using WebApp.ApiControllers.v1_0.Helpers;

namespace WebApp.ApiControllers.v1_0
{
    /// <summary>
    /// API controller containing all the methods for operations with games.
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class GamesController : ControllerBase
    {
        private readonly IAppBLL _bll;

        /// <summary>
        /// Construct and initialize GamesController.
        /// </summary>
        /// <param name="bll">business logic layer</param>
        public GamesController(IAppBLL bll)
        {
            _bll = bll;
        }


        // GET: api/Games
        /// <summary>
        /// Get all Games objects.
        /// </summary>
        /// <returns>Array of all the Games.</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PublicApi.v1.DTO.Game>>> GetGames()
        {
            return (await _bll.Games.AllAsync())
                .Select(g => PublicApi.v1.Mappers.GameMapper.MapFromBLL(g)).ToList();
        }

        // GET: api/Games/5
        /// <summary>
        /// Get a specific Game object.
        /// </summary>
        /// <param name="id">an id of a game</param>
        /// <returns>A game corresponding to the id or if there is no such game, then NotFound().</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<PublicApi.v1.DTO.Game>> GetGame(int id)
        {
            var game = PublicApi.v1.Mappers.GameMapper.MapFromBLL(await _bll.Games.FindAsync(id));

            new SelectLists()
            {
                GenreSelectList = new SelectList(
                    await _bll.Genres.AllAsync(),
                    nameof(PublicApi.v1.DTO.Genre.Id),
                    nameof(PublicApi.v1.DTO.Genre.GenreName)),
                
                PlatformSelectList = new SelectList(
                    await _bll.Platforms.AllAsync(),
                    nameof(PublicApi.v1.DTO.Platform.Id),
                    nameof(PublicApi.v1.DTO.Platform.PlatformName)),
                
                PublisherSelectList = new SelectList(
                    await _bll.Publishers.AllAsync(),
                    nameof(PublicApi.v1.DTO.Publisher.Id),
                    nameof(PublicApi.v1.DTO.Publisher.PublisherName))
            };
            
            if (game == null)
            {
                return NotFound();
            }

            return game;
        }

        // PUT: api/Games/5
        /// <summary>
        /// Update a specific game, which the user has modified.
        /// </summary>
        /// <param name="id">an id of a game</param>
        /// <param name="game">a game object</param>
        /// <returns>NoContent() or BadRequest() if given id does not match with the game's id</returns>
        [Authorize(Roles = "Admin")]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutGame(int id, PublicApi.v1.DTO.Game game)
        {
            if (id != game.Id)
            {
                return BadRequest();
            }
            
            new SelectLists()
            {
                GenreSelectList = new SelectList(
                    await _bll.Genres.AllAsync(),
                    nameof(PublicApi.v1.DTO.Genre.Id),
                    nameof(PublicApi.v1.DTO.Genre.GenreName)),
                
                PlatformSelectList = new SelectList(
                    await _bll.Platforms.AllAsync(),
                    nameof(PublicApi.v1.DTO.Platform.Id),
                    nameof(PublicApi.v1.DTO.Platform.PlatformName)),
                
                PublisherSelectList = new SelectList(
                    await _bll.Publishers.AllAsync(),
                    nameof(PublicApi.v1.DTO.Publisher.Id),
                    nameof(PublicApi.v1.DTO.Publisher.PublisherName))
            };

            _bll.Games.Update(PublicApi.v1.Mappers.GameMapper.MapFromExternal(game));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        // POST: api/Games
        /// <summary>
        /// Post a new game.
        /// </summary>
        /// <param name="game">a game object</param>
        /// <returns>a created game</returns>
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<ActionResult<PublicApi.v1.DTO.Game>> PostGame(PublicApi.v1.DTO.Game game)
        {
            _bll.Games.Add(PublicApi.v1.Mappers.GameMapper.MapFromExternal(game));
            await _bll.SaveChangesAsync();

            return CreatedAtAction(nameof(GetGame), 
                new
                {
                    version = HttpContext.GetRequestedApiVersion().ToString(),
                    id = game.Id
                }, game);
        }

        // DELETE: api/Games/5
        /// <summary>
        /// Delete a specific game.
        /// </summary>
        /// <param name="id">an id of a game</param>
        /// <returns>NoContent(), if removal was successful. NotFound(), if this specific game is null.</returns>
        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteGame(int id)
        {
            var game = await _bll.Games.FindAsync(id);
            if (game == null)
            {
                return NotFound();
            }

            _bll.Games.Remove(id);
            await _bll.SaveChangesAsync();

            return NoContent();
        }
    }
}