using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.ApiControllers.v1_0
{
    /// <summary>
    /// API controller containing all the methods for operations with basket statuses.
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class BasketStatusesController : ControllerBase
    {
        private readonly IAppBLL _bll;

        /// <summary>
        /// Construct and initialize BasketStatusesController.
        /// </summary>
        /// <param name="bll">business logic layer</param>
        public BasketStatusesController(IAppBLL bll)
        {
            _bll = bll;
        }

        // GET: api/BasketStatuses
        /// <summary>
        /// Get all BasketStatus objects.
        /// </summary>
        /// <returns>Array of all the basketStatuses.</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PublicApi.v1.DTO.BasketStatus>>> GetBasketStatuses()
        {
            return (await _bll.BasketStatuses.AllAsync())
                .Select(b => PublicApi.v1.Mappers.BasketStatusMapper.MapFromBLL(b)).ToList();
        }

        // GET: api/BasketStatuses/5
        /// <summary>
        /// Get a specific basketStatus object.
        /// </summary>
        /// <param name="id">an id of a basketStatus</param>
        /// <returns>A basketStatus corresponding to the id or if there is no such basketStatus, then NotFound().
        /// </returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<PublicApi.v1.DTO.BasketStatus>> GetBasketStatus(int id)
        {
            var basketStatus =
                PublicApi.v1.Mappers.BasketStatusMapper.MapFromBLL(await _bll.BasketStatuses.FindAsync(id));

            if (basketStatus == null)
            {
                return NotFound();
            }

            return basketStatus;
        }

        // PUT: api/BasketStatuses/5
        /// <summary>
        /// Update a specific basketStatus, which the user has modified.
        /// </summary>
        /// <param name="id">an id of a basketStatus</param>
        /// <param name="basketStatus">a basketStatus object</param>
        /// <returns>NoContent() or BadRequest() if given id does not match with the basket's id</returns>
        [Authorize(Roles = "Admin")]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBasketStatus(int id, PublicApi.v1.DTO.BasketStatus basketStatus)
        {
            if (id != basketStatus.Id)
            {
                return BadRequest();
            }

            _bll.BasketStatuses.Update(PublicApi.v1.Mappers.BasketStatusMapper.MapFromExternal(basketStatus));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        // POST: api/BasketStatuses
        /// <summary>
        /// Post a new basketStatus.
        /// </summary>
        /// <param name="basketStatus">a basketStatus object</param>
        /// <returns>a created basketStatus</returns>
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<ActionResult<PublicApi.v1.DTO.BasketStatus>> PostBasketStatus(
            PublicApi.v1.DTO.BasketStatus basketStatus)
        {
            _bll.BasketStatuses.Add(PublicApi.v1.Mappers.BasketStatusMapper.MapFromExternal(basketStatus));
            await _bll.SaveChangesAsync();

            return CreatedAtAction(nameof(GetBasketStatus),
                new
                {
                    version = HttpContext.GetRequestedApiVersion().ToString(),
                    id = basketStatus.Id
                }, basketStatus);
        }

        // DELETE: api/BasketStatuses/5
        /// <summary>
        /// Delete a specific basketStatus.
        /// </summary>
        /// <param name="id">an id of a basketStatus</param>
        /// <returns>NoContent(), if removal was successful. NotFound(), if this specific basketStatus is null.
        /// </returns>
        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteBasketStatus(int id)
        {
            var basketStatus = await _bll.BasketStatuses.FindAsync(id);
            if (basketStatus == null)
            {
                return NotFound();
            }

            _bll.BasketStatuses.Remove(id);
            await _bll.SaveChangesAsync();

            return NoContent();
        }
    }
}