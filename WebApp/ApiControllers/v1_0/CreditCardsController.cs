using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.ApiControllers.v1_0
{
    /// <summary>
    /// API controller containing all the methods for operations with CreditCards.
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CreditCardsController : ControllerBase
    {
        private readonly IAppBLL _bll;

        /// <summary>
        /// Construct and initialize CreditCardsController.
        /// </summary>
        /// <param name="bll">business logic layer</param>
        public CreditCardsController(IAppBLL bll)
        {
            _bll = bll;
        }


        // GET: api/CreditCards
        /// <summary>
        /// Get all CreditCards objects for a logged in user.
        /// </summary>
        /// <returns>Array of all the CreditCards that have been created by the logged in user.</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PublicApi.v1.DTO.CreditCard>>> GetCreditCards()
        {
            return (await _bll.CreditCards.AllForUserAsync(User.GetUserId()))
                .Select(c => PublicApi.v1.Mappers.CreditCardMapper.MapFromBLL(c)).ToList();
        }

        // GET: api/CreditCards/5
        /// <summary>
        /// Get a specific creditCard object for a logged in user.
        /// </summary>
        /// <param name="id">an id of a creditCard</param>
        /// <returns>A creditCard corresponding to the id or if there is no such creditCard, then NotFound().</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<PublicApi.v1.DTO.CreditCard>> GetCreditCard(int id)
        {
            var creditCard =
                PublicApi.v1.Mappers.CreditCardMapper.MapFromBLL(
                    await _bll.CreditCards.FindForUserAsync(id, User.GetUserId()));

            if (creditCard == null)
            {
                return NotFound();
            }

            return creditCard;
        }

        // PUT: api/CreditCards/5
        /// <summary>
        /// Update a specific creditCard, which the user has modified.
        /// </summary>
        /// <param name="id">an id of a creditCard</param>
        /// <param name="creditCard">a creditCard object</param>
        /// <returns>NoContent(), NotFound(), if creditCard does not belong to the user or BadRequest(), if given
        /// id does not match with the creditCard's id.</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCreditCard(int id, PublicApi.v1.DTO.CreditCard creditCard)
        {
            var appuser = User.GetUserId();

            if (id != creditCard.Id)
            {
                return BadRequest();
            }

            if (!await _bll.CreditCards.BelongsToUserAsync(id, appuser))
            {
                return NotFound();
            }

            creditCard.AppUserId = appuser;

            _bll.CreditCards.Update(PublicApi.v1.Mappers.CreditCardMapper.MapFromExternal(creditCard));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        // POST: api/CreditCards
        /// <summary>
        /// Post a new creditCard for a logged in user.
        /// </summary>
        /// <param name="creditCard">a creditCard object</param>
        /// <returns>a created creditCard</returns>
        [HttpPost]
        public async Task<ActionResult<PublicApi.v1.DTO.CreditCard>> PostCreditCard(
            PublicApi.v1.DTO.CreditCard creditCard)
        {
            creditCard.AppUserId = User.GetUserId();

            _bll.CreditCards.Add(PublicApi.v1.Mappers.CreditCardMapper.MapFromExternal(creditCard));
            await _bll.SaveChangesAsync();

            return CreatedAtAction(nameof(GetCreditCard),
                new
                {
                    version = HttpContext.GetRequestedApiVersion().ToString(),
                    id = creditCard.Id
                }, creditCard);
        }

        // DELETE: api/CreditCards/5
        /// <summary>
        /// Delete a specific creditCard, which has been created by a logged in user.
        /// </summary>
        /// <param name="id">an id of a creditCard</param>
        /// <returns>NoContent(), if removal was successful. NotFound(), if this specific creditCard does not belong to
        /// that specific logged in user.</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteCreditCard(int id)
        {
            if (!await _bll.CreditCards.BelongsToUserAsync(id, User.GetUserId()))
            {
                return NotFound();
            }

            _bll.CreditCards.Remove(id);
            await _bll.SaveChangesAsync();

            return NoContent();
        }
    }
}