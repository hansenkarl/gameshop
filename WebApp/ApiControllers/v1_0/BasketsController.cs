using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.ApiControllers.v1_0
{
    /// <summary>
    /// API controller containing all the methods for operations with baskets.
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class BasketsController : ControllerBase
    {
        private readonly IAppBLL _bll;

        /// <summary>
        /// Construct and initialize BasketsController.
        /// </summary>
        /// <param name="bll">business logic layer</param>
        public BasketsController(IAppBLL bll)
        {
            _bll = bll;
        }


        // GET: api/Baskets
        /// <summary>
        /// Get all Basket objects for a logged in user.
        /// </summary>
        /// <returns>Array of all the baskets that have been created by the logged in user.</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PublicApi.v1.DTO.Basket>>> GetBaskets()
        {
            return (await _bll.Baskets.AllForUserAsync(User.GetUserId()))
                .Select(b => PublicApi.v1.Mappers.BasketMapper.MapFromBLL(b)).ToList();
        }

        // GET: api/Baskets/5
        /// <summary>
        /// Get a specific basket object for a logged in user.
        /// </summary>
        /// <param name="id">an id of a basket</param>
        /// <returns>A basket corresponding to the id or if there is no such basket, then NotFound().</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<PublicApi.v1.DTO.Basket>> GetBasket(int id)
        {
            var basket =
                PublicApi.v1.Mappers.BasketMapper.MapFromBLL(await _bll.Baskets.FindForUserAsync(id, User.GetUserId()));

            if (basket == null)
            {
                return NotFound();
            }

            return basket;
        }

        // PUT: api/Baskets/5
        /// <summary>
        /// Update a specific basket, which the user has modified.
        /// </summary>
        /// <param name="id">an id of a basket</param>
        /// <param name="basket">a basket object</param>
        /// <returns>NoContent(), NotFound(), if basket does not belong to the user or BadRequest(), if given
        /// id does not match with the basket's id.</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutBasket(int id, PublicApi.v1.DTO.Basket basket)
        {
            var appuser = User.GetUserId();
            basket.Created = _bll.Baskets.GetBasket(id).Result.Created;

            if (id != basket.Id)
            {
                return BadRequest();
            }

            if (!await _bll.Baskets.BelongsToUserAsync(id, appuser))
            {
                return NotFound();
            }

            basket.AppUserId = appuser;

            _bll.Baskets.Update(PublicApi.v1.Mappers.BasketMapper.MapFromExternal(basket));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        // POST: api/Baskets
        /// <summary>
        /// Post a new basket for a logged in user.
        /// </summary>
        /// <param name="basket">a basket object</param>
        /// <returns>a created basket</returns>
        [HttpPost]
        public async Task<ActionResult<PublicApi.v1.DTO.Basket>> PostBasket(PublicApi.v1.DTO.Basket basket)
        {
            var mappedBasket = await _bll.Baskets.CreateBasket();
            var mappedBilling = await _bll.Billings.CreateBilling();

            mappedBasket.BillingId = mappedBilling.Id;
            mappedBilling.BasketId = mappedBasket.Id;

            mappedBasket.AppUserId = User.GetUserId();
            mappedBasket.Created = DateTime.Now;

            mappedBilling.Created = mappedBasket.Created;

            _bll.Baskets.Update(mappedBasket);
            _bll.Billings.Update(mappedBilling);

            await _bll.SaveChangesAsync();

            return CreatedAtAction(nameof(GetBasket),
                new
                {
                    version = HttpContext.GetRequestedApiVersion().ToString(),
                    id = basket.Id
                }, basket);
        }

        // DELETE: api/Baskets/5
        /// <summary>
        /// Delete a specific basket, which has been created by a logged in user.
        /// </summary>
        /// <param name="id">an id of a basket</param>
        /// <returns>NoContent(), if removal was successful. NotFound(), if this specific basket does not belong to
        /// that specific logged in user.</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteBasket(int id)
        {
            if (!await _bll.Baskets.BelongsToUserAsync(id, User.GetUserId()))
            {
                return NotFound();
            }

            var basketToDelete = await _bll.Baskets.GetBasket(id);
            var billingToDelete = await _bll.Billings.GetBilling((int) basketToDelete.BillingId);
            var gameInBasketToDelete = await _bll.GameInBaskets.AllForBasketAsync(basketToDelete.Id);

            if (basketToDelete.BasketStatusId != 1) return BadRequest();

            foreach (var game in gameInBasketToDelete)
            {
                _bll.GameInBaskets.Remove(game.Id);
            }

            _bll.Baskets.Remove(basketToDelete.Id);
            _bll.Billings.Remove(billingToDelete.Id);
            await _bll.SaveChangesAsync();

            return NoContent();
        }
    }
}