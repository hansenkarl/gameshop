using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.ApiControllers.v1_0
{
    /// <summary>
    /// API controller containing all the methods for operations with warehouses.
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class WarehousesController : ControllerBase
    {
        private readonly IAppBLL _bll;

        /// <summary>
        /// Construct and initialize WarehousesController.
        /// </summary>
        /// <param name="bll">business logic layer</param>
        public WarehousesController(IAppBLL bll)
        {
            _bll = bll;
        }


        // GET: api/Warehouses
        /// <summary>
        /// Get all Warehouses objects that have been created.
        /// </summary>
        /// <returns>Array of all the warehouses.</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PublicApi.v1.DTO.Warehouse>>> GetWarehouses()
        {
            return (await _bll.Warehouses.AllAsync())
                .Select(w => PublicApi.v1.Mappers.WarehouseMapper.MapFromBLL(w)).ToList();
        }

        // GET: api/Warehouses/5
        /// <summary>
        /// Get a specific warehouses object.
        /// </summary>
        /// <param name="id">an id of a warehouse</param>
        /// <returns>A warehouse corresponding to the id or if there is no such warehouse, then NotFound().</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<PublicApi.v1.DTO.Warehouse>> GetWarehouse(int id)
        {
            var warehouse = PublicApi.v1.Mappers.WarehouseMapper.MapFromBLL(await _bll.Warehouses.FindAsync(id));

            if (warehouse == null)
            {
                return NotFound();
            }

            return warehouse;
        }

        // PUT: api/Warehouses/5
        /// <summary>
        /// Update a specific warehouse, which the user has modified.
        /// </summary>
        /// <param name="id">an id of a warehouse</param>
        /// <param name="warehouse">a warehouse object</param>
        /// <returns>NoContent(), or BadRequest(), if given id does not match with the warehouse's id.</returns>
        [Authorize(Roles = "Admin")]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutWarehouse(int id, PublicApi.v1.DTO.Warehouse warehouse)
        {
            if (id != warehouse.Id)
            {
                return BadRequest();
            }

            _bll.Warehouses.Update(PublicApi.v1.Mappers.WarehouseMapper.MapFromExternal(warehouse));
            await _bll.SaveChangesAsync();
            
            return NoContent();
        }

        // POST: api/Warehouses
        /// <summary>
        /// Post a new warehouse.
        /// </summary>
        /// <param name="warehouse">a warehouse object</param>
        /// <returns>a created warehouse</returns>
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public async Task<ActionResult<PublicApi.v1.DTO.Warehouse>> PostWarehouse(PublicApi.v1.DTO.Warehouse warehouse)
        {
            _bll.Warehouses.Add(PublicApi.v1.Mappers.WarehouseMapper.MapFromExternal(warehouse));
            await _bll.SaveChangesAsync();

            return CreatedAtAction(nameof(GetWarehouse), 
                new
                {
                    version = HttpContext.GetRequestedApiVersion().ToString(),
                    id = warehouse.Id
                }, warehouse);
        }

        // DELETE: api/Warehouses/5
        /// <summary>
        /// Delete a specific warehouse, which has been created by a logged in user.
        /// </summary>
        /// <param name="id">an id of a warehouse</param>
        /// <returns>NoContent(), if removal was successful. NotFound(), if this specific warehouse is null.</returns>
        [Authorize(Roles = "Admin")]
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteWarehouse(int id)
        {
            var warehouse = await _bll.Warehouses.FindAsync(id);
            if (warehouse == null)
            {
                return NotFound();
            }

            _bll.Warehouses.Remove(id);
            await _bll.SaveChangesAsync();

            return NoContent();
        }
    }
}
