using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PublicApi.v1.DTO;

namespace WebApp.ApiControllers.v1_0
{
    /// <summary>
    /// Controller to handle seeing into the baskets.
    /// </summary>
    ///     [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class BasketDetailsController : ControllerBase
    {
        private IAppBLL _bll;

        /// <summary>
        /// Construct and initialize BasketDetailsController.
        /// </summary>
        /// <param name="bll"></param>
        public BasketDetailsController(IAppBLL bll)
        {
            _bll = bll;
        }

        /// <summary>
        /// Get basket with it's desired details.
        /// </summary>
        /// <param name="id">basket's id</param>
        /// <returns>Baskets' details or NotFound, if basket does not exist.</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<BasketDetails>> GetBasket(int id)
        {
            var basket =
                PublicApi.v1.Mappers.BasketMapper.MapFromBLL(await _bll.Baskets.FindForUserAsync(id, User.GetUserId()));

            if (basket == null)
            {
                return NotFound();
            }

            var basketDetails = new BasketDetails
            {
                Id = basket.Id,
                Basket = basket,
                Games = new List<GameInBasket>()
            };


            foreach (var gameInBasket in await _bll.GameInBaskets.AllForBasketAsync(basketDetails.Basket.Id))
            {
                basketDetails.Games.Add(PublicApi.v1.Mappers.GameInBasketMapper.MapFromBLL(gameInBasket));
            }

            return basketDetails;
        }

        /// <summary>
        /// Helper class to retrieve basket's details.
        /// </summary>
        public class BasketDetails
        {
            /// <summary>
            /// Dummy id. Will be equal to Basket.Id.
            /// </summary>
            public int Id;
            /// <summary>
            /// The basket, we want to look at.
            /// </summary>
            public PublicApi.v1.DTO.Basket Basket;
            /// <summary>
            /// Games, which are in that particular basket.
            /// </summary>
            public List<PublicApi.v1.DTO.GameInBasket> Games;
        }
    }
}