using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.ApiControllers.v1_0
{
    /// <summary>
    /// API controller containing all the methods for operations with games in baskets.
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class GamesInBasketsController : ControllerBase
    {
        private readonly IAppBLL _bll;

        /// <summary>
        /// Construct and initialize GamesInBasketsController.
        /// </summary>
        /// <param name="bll">business logic layer</param>
        public GamesInBasketsController(IAppBLL bll)
        {
            _bll = bll;
        }


        // GET: api/GamesInBaskets
        /// <summary>
        /// Get all GamesInBaskets objects that have been created by the logged in user.
        /// </summary>
        /// <returns>Array of all the gamesInBaskets.</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PublicApi.v1.DTO.GameInBasket>>> GetGamesInBaskets()
        {
            return (await _bll.GameInBaskets.AllForUserAsync(User.GetUserId()))
                .Select(g => PublicApi.v1.Mappers.GameInBasketMapper.MapFromBLL(g)).ToList();
        }

        // GET: api/GamesInBaskets/5
        /// <summary>
        /// Get a specific gamesInBaskets object.
        /// </summary>
        /// <param name="id">an id of a game in basket</param>
        /// <returns>A gameInBasket corresponding to the id or if there is no such gameInBasket, then NotFound().
        /// </returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<PublicApi.v1.DTO.GameInBasket>> GetGameInBasket(int id)
        {
            var appuser = User.GetUserId();
            
            var gameInBasket =
                PublicApi.v1.Mappers.GameInBasketMapper.MapFromBLL(
                    await _bll.GameInBaskets.FindForUserAsync(id, User.GetUserId()));

            if (gameInBasket == null)
            {
                return NotFound();
            }

            return gameInBasket;
        }

        // PUT: api/GamesInBaskets/5
        /// <summary>
        /// Update a specific gameInBasket, which the user has modified.
        /// </summary>
        /// <param name="id">an id of a gameInBasket</param>
        /// <param name="gameInBasket">a gameInBasket object</param>
        /// <returns>NoContent(), NotFound(), if gameInBasket does not belong to the user or BadRequest(), if given
        /// id does not match with the gameInBasket's id.</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutGameInBasket(int id, PublicApi.v1.DTO.GameInBasket gameInBasket)
        {
            if (id != gameInBasket.Id)
            {
                return BadRequest();
            }

            if (!await _bll.GameInBaskets.BelongsToUserAsync(id, User.GetUserId()))
            {
                return NotFound();
            }

            _bll.GameInBaskets.Update(PublicApi.v1.Mappers.GameInBasketMapper.MapFromExternal(gameInBasket));
            await _bll.SaveChangesAsync();
            await ReCalculateBilling(gameInBasket.BasketId);

            return NoContent();
        }

        // POST: api/GamesInBaskets
        /// <summary>
        /// Post a new gameInBasket for a logged in user.
        /// </summary>
        /// <param name="gameInBasket">a gameInBasket object</param>
        /// <returns>a created gameInBasket</returns>
        [HttpPost]
        public async Task<ActionResult<PublicApi.v1.DTO.GameInBasket>> PostGameInBasket(
            PublicApi.v1.DTO.GameInBasket gameInBasket)
        {
            if ((await _bll.Baskets.GetBasket(gameInBasket.BasketId)).AppUserId != User.GetUserId())
            {
                return NotFound();
            }

            _bll.GameInBaskets.Add(PublicApi.v1.Mappers.GameInBasketMapper.MapFromExternal(gameInBasket));
            await _bll.SaveChangesAsync();
            await ReCalculateBilling(gameInBasket.BasketId);

            return CreatedAtAction(nameof(GetGameInBasket),
                new
                {
                    version = HttpContext.GetRequestedApiVersion().ToString(),
                    id = gameInBasket.Id
                }, gameInBasket);
        }

        // DELETE: api/GamesInBaskets/5
        /// <summary>
        /// Delete a specific gameInBasket, which has been created by a logged in user.
        /// </summary>
        /// <param name="id">an id of a gameInBasket</param>
        /// <returns>NoContent(), if removal was successful. NotFound(), if this specific gameInBasket does not belong
        /// to that specific logged in user.</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteGameInBasket(int id)
        {
            if (!await _bll.GameInBaskets.BelongsToUserAsync(id, User.GetUserId()))
            {
                return NotFound();
            }

            var gameInBasket = await _bll.GameInBaskets.FindForUserAsync(id, User.GetUserId());
            
            _bll.GameInBaskets.Remove(id);
            await _bll.SaveChangesAsync();
            await ReCalculateBilling(gameInBasket.BasketId);

            return NoContent();
        }

        /// <summary>
        /// Recalculates billing after changes.
        /// </summary>
        /// <param name="id">basket id</param>
        public async Task ReCalculateBilling(int id)
        {
            var basket = await _bll.Baskets.GetBasket(id);
            var billing = await _bll.Billings.GetBilling(basket.BillingId.Value);

            var gamesInBasket = await _bll.GameInBaskets.AllForBasketAsync(id);
            var games = await _bll.Games.AllAsync();

            Decimal sumWithoutTax = 0;
            foreach (var gameInBasket in gamesInBasket)
            {
                sumWithoutTax += gameInBasket.Amount * games.Find(game => game.Id == gameInBasket.GameId).Price;
            }

            billing.SumWithoutTax = sumWithoutTax;
            billing.SumTotal = billing.SumWithoutTax * (1 + billing.Tax);

            _bll.Billings.Update(billing);
            await _bll.SaveChangesAsync();
        }
    }
}