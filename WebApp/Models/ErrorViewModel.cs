using System;

namespace WebApp.Models
{
    /// <summary>
    /// Error view model.
    /// </summary>
    public class ErrorViewModel
    {
        /// <summary>
        /// Id of an error request.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// Show a request id.
        /// </summary>
        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}