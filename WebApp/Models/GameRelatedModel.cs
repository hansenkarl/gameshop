using BLL.App.DTO;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApp.Models
{
    /// <summary>
    /// Model that contains required data for displaying in views related to Game object. 
    /// </summary>
    public class GameRelatedModel
    {
        /// <summary>
        /// Game object.
        /// </summary>
        public Game Game { get; set; }
        
        /// <summary>
        /// SelectList consisting of publishers.
        /// </summary>
        public SelectList PublisherSelectList { get; set; }

        /// <summary>
        /// SelectList consisting of games.
        /// </summary>
        public SelectList GameSelectList { get; set; }
        
        /// <summary>
        /// SelectList consisting of genres.
        /// </summary>
        public SelectList GenreSelectList { get; set; }

        /// <summary>
        /// GameInWarehouse object.
        /// </summary>
        public GameInWarehouse GameInWarehouse { get; set; }

        /// <summary>
        /// SelectList consisting of warehouses.
        /// </summary>
        public SelectList WarehouseSelectList { get; set; }

        /// <summary>
        /// SelectList consisting of platforms.
        /// </summary>
        public SelectList PlatformSelectList { get; set; }
    }
}