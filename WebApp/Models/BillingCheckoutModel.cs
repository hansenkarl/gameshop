using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApp.Models
{
    /// <summary>
    /// A model to enable checkout of a billing.
    /// </summary>
    public class BillingCheckoutModel
    {
        /// <summary>
        /// Billing object.
        /// </summary>
        public BLL.App.DTO.Billing Billing { get; set; }
        
        /// <summary>
        /// Basket object.
        /// </summary>
        public BLL.App.DTO.Basket Basket { get; set; }
        
        /// <summary>
        /// SelectList consisting of credit cards.
        /// </summary>
        public SelectList CreditCardSelectList { get; set; }
    }
}