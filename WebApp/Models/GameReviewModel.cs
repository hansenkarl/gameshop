namespace WebApp.Models
{
    /// <summary>
    /// A model containing required data to enable review creation next to game (link).
    /// </summary>
    public class GameReviewModel
    {
        /// <summary>
        /// Game object.
        /// </summary>
        public BLL.App.DTO.Game Game { get; set; }
        
        /// <summary>
        /// Review object.
        /// </summary>
        public BLL.App.DTO.Review Review { get; set; }
    }
}