using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApp.Models
{
    /// <summary>
    /// A model that makes it possible to put game in basket under game details view.
    /// </summary>
    public class PutGameInBasketModel
    {
        /// <summary>
        /// Game object.
        /// </summary>
        public BLL.App.DTO.Game Game { get; set; }
        
        /// <summary>
        /// GameInBasket object.
        /// </summary>
        public BLL.App.DTO.GameInBasket GameInBasket { get; set; }
        
        
        /// <summary>
        /// SelectList containing baskets.
        /// </summary>
        public SelectList BasketSelectList { get; set; }
    }
}