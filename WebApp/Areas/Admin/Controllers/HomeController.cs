using Microsoft.AspNetCore.Mvc;

namespace WebApp.Areas.Admin.Controllers
{
    /// <summary>
    /// Home controller in an admin area.
    /// </summary>
    [Area("Admin")]
    public class HomeController : Controller
    {
        // GET
        /// <summary>
        /// Gets the admin index view.
        /// </summary>
        /// <returns>An index View()</returns>
        public IActionResult Index()
        {
            return View();
        }
    }
}