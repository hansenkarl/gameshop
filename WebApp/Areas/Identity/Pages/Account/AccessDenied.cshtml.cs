using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace WebApp.Areas.Identity.Pages.Account
{
    /// <summary>
    /// Class that handles the access denied related.
    /// </summary>
    public class AccessDeniedModel : PageModel
    {
        /// <summary>
        /// Empty method.
        /// </summary>
        public void OnGet()
        {

        }
    }
}

