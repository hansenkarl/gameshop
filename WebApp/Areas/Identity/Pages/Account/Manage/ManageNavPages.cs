using System;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace WebApp.Areas.Identity.Pages.Account.Manage
{
    /// <summary>
    /// A class to manage navigation pages.
    /// </summary>
    public static class ManageNavPages
    {
        /// <summary>
        /// A string called Index.
        /// </summary>
        public static string Index => "Index";

        /// <summary>
        /// A string called ChangePassword.
        /// </summary>
        public static string ChangePassword => "ChangePassword";

        /// <summary>
        /// A string called ExternalLogins.
        /// </summary>
        public static string ExternalLogins => "ExternalLogins";

        /// <summary>
        /// A string called PersonalData.
        /// </summary>
        public static string PersonalData => "PersonalData";

        /// <summary>
        /// A string called TwoFactorAuthentication.
        /// </summary>
        public static string TwoFactorAuthentication => "TwoFactorAuthentication";

        /// <summary>
        /// Execute navigation to Index.
        /// </summary>
        /// <param name="viewContext">context for view execution</param>
        /// <returns></returns>
        public static string IndexNavClass(ViewContext viewContext) => PageNavClass(viewContext, Index);

        /// <summary>
        /// Execute navigation to ChangePassword.
        /// </summary>
        /// <param name="viewContext">context for view execution</param>
        /// <returns></returns>
        public static string ChangePasswordNavClass(ViewContext viewContext) => PageNavClass(viewContext, ChangePassword);

        /// <summary>
        /// Execute navigation to ExternalLogins.
        /// </summary>
        /// <param name="viewContext">context for view execution</param>
        /// <returns></returns>
        public static string ExternalLoginsNavClass(ViewContext viewContext) => PageNavClass(viewContext, ExternalLogins);

        /// <summary>
        /// Execute navigation to PersonalData.
        /// </summary>
        /// <param name="viewContext">context for view execution</param>
        /// <returns></returns>
        public static string PersonalDataNavClass(ViewContext viewContext) => PageNavClass(viewContext, PersonalData);

        /// <summary>
        /// Execute navigation to TwoFactorAuthentication.
        /// </summary>
        /// <param name="viewContext">context for view execution</param>
        /// <returns></returns>
        public static string TwoFactorAuthenticationNavClass(ViewContext viewContext) => PageNavClass(viewContext, TwoFactorAuthentication);

        private static string PageNavClass(ViewContext viewContext, string page)
        {
            var activePage = viewContext.ViewData["ActivePage"] as string
                ?? System.IO.Path.GetFileNameWithoutExtension(viewContext.ActionDescriptor.DisplayName);
            return string.Equals(activePage, page, StringComparison.OrdinalIgnoreCase) ? "active" : null;
        }
    }
}