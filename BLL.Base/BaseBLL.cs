﻿using System;
using System.Threading.Tasks;
using Contracts.BLL.Base;
using Contracts.BLL.Base.Helpers;
using Contracts.BLL.Base.Services;
using Contracts.DAL.Base;

namespace BLL.Base
{
    public class BaseBLL<TUnitOfWork> : IBaseBLL
        where TUnitOfWork : IBaseUnitOfWork
    {
        public virtual Guid InstanceId { get; } = Guid.NewGuid();

        protected readonly TUnitOfWork Uow;
        protected readonly IBaseServiceProvider ServiceProvider;

        public BaseBLL(TUnitOfWork uow, IBaseServiceProvider serviceProvider)
        {
            Uow = uow;
            ServiceProvider = serviceProvider;
        }
        
        public virtual async Task<int> SaveChangesAsync()
        {
            return await Uow.SaveChangesAsync();
        }

        public int SaveChanges()
        {
            return Uow.SaveChanges();
        }
    }
}