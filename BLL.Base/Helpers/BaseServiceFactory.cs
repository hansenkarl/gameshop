using System;
using System.Collections.Generic;
using BLL.Base.Mappers;
using BLL.Base.Services;
using Contracts.BLL.Base.Helpers;
using Contracts.DAL.Base;

namespace BLL.Base.Helpers
{
    public class BaseServiceFactory<TUnitOfWork> : IBaseServiceFactory<TUnitOfWork>
        where TUnitOfWork : IBaseUnitOfWork
    {
        protected readonly Dictionary<Type, Func<TUnitOfWork, object>> ServiceCreationMethods;

        public BaseServiceFactory() : this(new Dictionary<Type, Func<TUnitOfWork, object>>())
        {
        }

        public BaseServiceFactory(Dictionary<Type, Func<TUnitOfWork, object>> serviceCreationMethods)
        {
            ServiceCreationMethods = serviceCreationMethods;
        }

        public virtual void AddToCreationMethods<TService>(Func<TUnitOfWork, TService> creationMethod)
            where TService : class
        {
            ServiceCreationMethods.Add(typeof(TService), creationMethod);
        }


        public virtual Func<TUnitOfWork, object> GetServiceFactory<TService>()
        {
            if (ServiceCreationMethods.ContainsKey(typeof(TService)))
            {
                return ServiceCreationMethods[typeof(TService)];
            }

            throw new NullReferenceException("No service creation method found for " + typeof(TService).FullName);
        }

        /*public virtual Func<TUnitOfWork, object> GetEntityServiceFactory<TBLLEntity, TDALEntity, TDomainEntity>()
            where TBLLEntity : class, new()
            where TDALEntity : class, new()
            where TDomainEntity : class, IDomainEntity, new()
        {
            return (uow) =>
                new BaseEntityService<TBLLEntity, TDALEntity, TDomainEntity, TUnitOfWork>(
                    new BaseBLLMapper<TBLLEntity, TDALEntity>(), uow);
        }*/
    }
}