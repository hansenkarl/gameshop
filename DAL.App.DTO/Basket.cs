using System;
using DAL.App.DTO.Identity;


namespace DAL.App.DTO
{
    public class Basket
    {
        public int Id { get; set; }
        
        public DateTime Created { get; set; }

        public int AppUserId { get; set; }
        public AppUser AppUser { get; set; }

        public int? BillingId { get; set; }
        public Billing Billing { get; set; }

        public int BasketStatusId { get; set; }
        public BasketStatus BasketStatus { get; set; }
    }
}