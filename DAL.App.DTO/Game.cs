using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL.App.DTO
{
    public class Game
    {
        public int Id { get; set; }
        
        [Required]
        [MaxLength(128)]
        [MinLength(2)]
        public string GameTitle { get; set; }
        
        [Required]
        [Column(TypeName = "decimal(5,2)")]
        public decimal Price { get; set; }
        
        [Required]
        [MaxLength(3)]
        [MinLength(1)]
        public string Pegi { get; set; }

        public int GenreId { get; set; }
        public Genre Genre { get; set; }

        public int PlatformId { get; set; }
        public Platform Platform { get; set; }
        
        public int PublisherId { get; set; }
        public Publisher Publisher { get; set; }

        public ICollection<GameInWarehouse> GameInWarehouses{ get; set; }

        public ICollection<GameInBasket> GameInBaskets { get; set; }

        public ICollection<Review> Reviews { get; set; }
    }
}