using System;
using System.ComponentModel.DataAnnotations;
using Domain.Identity;
using AppUser = DAL.App.DTO.Identity.AppUser;

namespace DAL.App.DTO
{
    public class Review
    {
        public int Id { get; set; }
        
        [Range(0, 10, ErrorMessage = "Please enter a rating between 0 - 10!")]
        public int Rating { get; set; }
        
        [Required]
        [MaxLength(4096)]
        [MinLength(2)]
        public string Comment { get; set; }
        
        [Required]
        public DateTime Added { get; set; }

        public int AppUserId { get; set; }
        public AppUser AppUser { get; set; }
        
        public int GameId { get; set; }
        public Game Game { get; set; }

        public int PlatformId { get; set; }
        public Platform Platform { get; set; }
    }
}