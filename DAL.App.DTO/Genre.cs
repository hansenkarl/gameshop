using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DAL.App.DTO
{
    public class Genre
    {
        public int Id { get; set; }
        
        [Required]
        [MaxLength(64)]
        [MinLength(2)]
        public string GenreName { get; set; }

        public ICollection<Game> Games{ get; set; }
    }
}