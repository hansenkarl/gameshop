using Contracts.DAL.Base.Mappers;

namespace Contracts.BLL.Base.Helpers
{
    public interface IBaseBLLMapper : IBaseDALMapper
    {
    }
}