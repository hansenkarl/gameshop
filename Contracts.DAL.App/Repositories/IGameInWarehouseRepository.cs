using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.Base.Repositories;
using DAL.App.DTO;


namespace Contracts.DAL.App.Repositories
{
    public interface IGameInWarehouseRepository : IGameInWarehouseRepository<GameInWarehouse>
    {
    }
    
    public interface IGameInWarehouseRepository<TDALEntity> : IBaseRepository<TDALEntity>
        where TDALEntity : class, new()
    {
        new Task<List<TDALEntity>> AllAsync();
        new Task<TDALEntity> FindAsync(params object[] id);
    }
}