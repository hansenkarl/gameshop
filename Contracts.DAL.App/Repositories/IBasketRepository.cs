using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.Base.Repositories;
using DAL.App.DTO;


namespace Contracts.DAL.App.Repositories
{
    public interface IBasketRepository : IBasketRepository<Basket>
    {
    }
    
    public interface IBasketRepository<TDALEntity> : IBaseRepository<TDALEntity>
        where TDALEntity : class, new()
    {
        Task<List<TDALEntity>> AllForUserAsync(int userId);
        Task<TDALEntity> FindForUserAsync(int id, int userId);
        Task<bool> BelongsToUserAsync(int id, int userId);
        Task<TDALEntity> GetBasket(int id);
        Task<TDALEntity> CreateBasket();

        Task<List<TDALEntity>> AllNonCheckedOutUserAsync(List<TDALEntity> baskets);
    }
}