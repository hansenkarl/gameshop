﻿using System;
using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base;

namespace Contracts.DAL.App
{
    public interface IAppUnitOfWork : IBaseUnitOfWork
    {
        IAddressRepository Addresses { get; }
        IAppUserRepository AppUsers { get; }
        IAppRoleRepository AppRoles { get; }
        IBasketRepository Baskets { get; }
        IBasketStatusRepository BasketStatuses { get; }
        IBillingRepository Billings { get; }
        IBillingStatusRepository BillingStatuses { get; }
        ICreditCardRepository CreditCards { get; }
        IGameRepository Games { get; }
        IGameInBasketRepository GameInBaskets { get; }
        IGameInWarehouseRepository GameInWarehouses { get; }
        IGenreRepository Genres { get; }
        IPlatformRepository Platforms { get; }
        IPublisherRepository Publishers { get; }
        IReviewRepository Reviews { get; }
        IWarehouseRepository Warehouses { get; }
    }
}