using System;
using externalDTO = PublicApi.v1.DTO;
using internalDTO = BLL.App.DTO;

namespace PublicApi.v1.Mappers
{
    public class GameInWarehouseMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.GameInWarehouse))
            {
                return MapFromBLL((internalDTO.GameInWarehouse) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.GameInWarehouse))
            {
                return MapFromExternal((externalDTO.GameInWarehouse) inObject) as TOutObject;
            }

            throw new InvalidCastException("No conversion!");
        }

        public static externalDTO.GameInWarehouse MapFromBLL(internalDTO.GameInWarehouse gameInWarehouse)
        {
            if (gameInWarehouse == null) return null;
            
            var res = new externalDTO.GameInWarehouse
            {
                Id = gameInWarehouse.Id,
                Amount = gameInWarehouse.Amount,
                Added = gameInWarehouse.Added,
                GameId = gameInWarehouse.GameId,
                Game = GameMapper.MapFromBLL(gameInWarehouse.Game),
                WarehouseId = gameInWarehouse.WarehouseId,
                Warehouse = WarehouseMapper.MapFromBLL(gameInWarehouse.Warehouse)
            };

            return res;

        }

        public static internalDTO.GameInWarehouse MapFromExternal(externalDTO.GameInWarehouse gameInWarehouse)
        {
            if (gameInWarehouse == null) return null;
            
            var res = new internalDTO.GameInWarehouse
            {
                Id = gameInWarehouse.Id,
                Amount = gameInWarehouse.Amount,
                Added = gameInWarehouse.Added,
                GameId = gameInWarehouse.GameId,
                Game = GameMapper.MapFromExternal(gameInWarehouse.Game),
                WarehouseId = gameInWarehouse.WarehouseId,
                Warehouse = WarehouseMapper.MapFromExternal(gameInWarehouse.Warehouse)
            };

            return res;
        }
    }
}