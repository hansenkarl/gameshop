using System;
using externalDTO = PublicApi.v1.DTO;
using internalDTO = BLL.App.DTO;

namespace PublicApi.v1.Mappers
{
    public class CreditCardMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.CreditCard))
            {
                return MapFromBLL((internalDTO.CreditCard) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.CreditCard))
            {
                return MapFromExternal((externalDTO.CreditCard) inObject) as TOutObject;
            }

            throw new InvalidCastException("No conversion!");
        }

        public static externalDTO.CreditCard MapFromBLL(internalDTO.CreditCard creditCard)
        {
            if (creditCard == null) return null;
            
            var res = new externalDTO.CreditCard
            {
                Id = creditCard.Id,
                CreditCardNr = creditCard.CreditCardNr,
                ValidThru = creditCard.ValidThru,
                AppUserId = creditCard.AppUserId,
                AppUser = AppUserMapper.MapFromBLL(creditCard.AppUser),
            };

            return res;
        }

        public static internalDTO.CreditCard MapFromExternal(externalDTO.CreditCard creditCard)
        {
            if (creditCard == null) return null;
            
            var res = new internalDTO.CreditCard
            {
                Id = creditCard.Id,
                CreditCardNr = creditCard.CreditCardNr,
                ValidThru = creditCard.ValidThru,
                AppUserId = creditCard.AppUserId,
                AppUser = AppUserMapper.MapFromExternal(creditCard.AppUser),
            };

            return res;
        }
    }
}