using System;
using externalDTO = PublicApi.v1.DTO;
using internalDTO = BLL.App.DTO;

namespace PublicApi.v1.Mappers
{
    public class BillingStatusMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.BillingStatus))
            {
                return MapFromBLL((internalDTO.BillingStatus) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.BillingStatus))
            {
                return MapFromExternal((externalDTO.BillingStatus) inObject) as TOutObject;
            }

            throw new InvalidCastException("No conversion!");
        }

        public static externalDTO.BillingStatus MapFromBLL(internalDTO.BillingStatus billingStatus)
        {
            if (billingStatus == null) return null;
            
            var res = new externalDTO.BillingStatus
            {
                Id = billingStatus.Id,
                StatusName = billingStatus.StatusName,
            };
            
            return res;

        }

        public static internalDTO.BillingStatus MapFromExternal(externalDTO.BillingStatus billingStatus)
        {
            if (billingStatus == null) return null;
            
            var res = new internalDTO.BillingStatus
            {
                Id = billingStatus.Id,
                StatusName = billingStatus.StatusName,
            };
            
            return res;
        }
    }
}