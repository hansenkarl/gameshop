using System;
using externalDTO = PublicApi.v1.DTO;
using internalDTO = BLL.App.DTO;

namespace PublicApi.v1.Mappers

{
    public class AppRoleMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.Identity.AppRole))
            {
                return MapFromBLL((internalDTO.Identity.AppRole) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.Identity.AppRole))
            {
                return MapFromExternal((externalDTO.Identity.AppRole) inObject) as TOutObject;
            }

            throw new InvalidCastException("No conversion!");
        }

        public static externalDTO.Identity.AppRole MapFromBLL(internalDTO.Identity.AppRole appRole)
        {
            if (appRole == null) return null;
            
            var res = new externalDTO.Identity.AppRole
            {
                Id = appRole.Id,
            };

            return res;
        }

        public static internalDTO.Identity.AppRole MapFromExternal(externalDTO.Identity.AppRole appRole)
        {
            if (appRole == null) return null;
            
            var res = new internalDTO.Identity.AppRole
            {
                Id = appRole.Id,
            };

            return res;
        }
    }
}