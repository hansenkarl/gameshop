using System;
using externalDTO = PublicApi.v1.DTO;
using internalDTO = BLL.App.DTO;

namespace PublicApi.v1.Mappers
{
    public class AddressMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.Address))
            {
                return MapFromBLL((internalDTO.Address) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.Address))
            {
                return MapFromExternal((externalDTO.Address) inObject) as TOutObject;
            }

            throw new InvalidCastException("No conversion!");
        }

        public static externalDTO.Address MapFromBLL(internalDTO.Address address)
        {
            var res = new externalDTO.Address
            {
                Id = address.Id,
                Country = address.Country,
                County = address.County,
                City = address.City,
                Street = address.Street,
                HouseNr = address.HouseNr,
                ZipCode = address.ZipCode,
                AppUserId = address.AppUserId,
                AppUser = AppUserMapper.MapFromBLL(address.AppUser)
            };
            return res;
        }

        public static internalDTO.Address MapFromExternal(externalDTO.Address address)
        {
            var res = new internalDTO.Address
            {
                Id = address.Id,
                Country = address.Country,
                County = address.County,
                City = address.City,
                Street = address.Street,
                HouseNr = address.HouseNr,
                ZipCode = address.ZipCode,
                AppUserId = address.AppUserId,
                AppUser = AppUserMapper.MapFromExternal(address.AppUser)
            };
            return res;
        }
    }
}