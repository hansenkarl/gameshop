using System;
using externalDTO = PublicApi.v1.DTO;
using internalDTO = BLL.App.DTO;

namespace PublicApi.v1.Mappers
{
    public class GameMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.Game))
            {
                return MapFromBLL((internalDTO.Game) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.Game))
            {
                return MapFromExternal((externalDTO.Game) inObject) as TOutObject;
            }

            throw new InvalidCastException("No conversion!");
        }

        public static externalDTO.Game MapFromBLL(internalDTO.Game game)
        {
            if (game == null) return null;
            
            var res = new externalDTO.Game
            {
                Id = game.Id,
                GameTitle = game.GameTitle,
                Price = game.Price,
                Pegi = game.Pegi,
                GenreId = game.GenreId,
                Genre = GenreMapper.MapFromBLL(game.Genre),
                PlatformId = game.PlatformId,
                Platform = PlatformMapper.MapFromBLL(game.Platform),
                PublisherId = game.PublisherId,
                Publisher = PublisherMapper.MapFromBLL(game.Publisher),
            };
            
            return res;

        }

        public static internalDTO.Game MapFromExternal(externalDTO.Game game)
        {
            if (game == null) return null;
            
            var res = new internalDTO.Game
            {
                Id = game.Id,
                GameTitle = game.GameTitle,
                Price = game.Price,
                Pegi = game.Pegi,
                GenreId = game.GenreId,
                Genre = GenreMapper.MapFromExternal(game.Genre),
                PlatformId = game.PlatformId,
                Platform = PlatformMapper.MapFromExternal(game.Platform),
                PublisherId = game.PublisherId,
                Publisher = PublisherMapper.MapFromExternal(game.Publisher),
            };
            
            return res;
        }
    }
}