using System;
using externalDTO = PublicApi.v1.DTO;
using internalDTO = BLL.App.DTO;

namespace PublicApi.v1.Mappers
{
    public class GameInBasketMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.GameInBasket))
            {
                return MapFromBLL((internalDTO.GameInBasket) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.GameInBasket))
            {
                return MapFromExternal((externalDTO.GameInBasket) inObject) as TOutObject;
            }

            throw new InvalidCastException("No conversion!");
        }

        public static externalDTO.GameInBasket MapFromBLL(internalDTO.GameInBasket gameInBasket)
        {
            if (gameInBasket == null) return null;
                
            var res = new externalDTO.GameInBasket
            {
                Id = gameInBasket.Id,
                Amount = gameInBasket.Amount,
                GameId = gameInBasket.GameId,
                Game = GameMapper.MapFromBLL(gameInBasket.Game),
                BasketId = gameInBasket.BasketId,
                Basket = BasketMapper.MapFromBLL(gameInBasket.Basket)
            };

            return res;

        }

        public static internalDTO.GameInBasket MapFromExternal(externalDTO.GameInBasket gameInBasket)
        {
            if (gameInBasket == null) return null;
            
            var res = new internalDTO.GameInBasket
            {
                Id = gameInBasket.Id,
                Amount = gameInBasket.Amount,
                GameId = gameInBasket.GameId,
                Game = GameMapper.MapFromExternal(gameInBasket.Game),
                BasketId = gameInBasket.BasketId,
                Basket = BasketMapper.MapFromExternal(gameInBasket.Basket)
            };

            return res;
        }
    }
}