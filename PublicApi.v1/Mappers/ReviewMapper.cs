using System;
using externalDTO = PublicApi.v1.DTO;
using internalDTO = BLL.App.DTO;

namespace PublicApi.v1.Mappers
{
    public class ReviewMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.Review))
            {
                return MapFromBLL((internalDTO.Review) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.Review))
            {
                return MapFromExternal((externalDTO.Review) inObject) as TOutObject;
            }

            throw new InvalidCastException("No conversion!");
        }

        public static externalDTO.Review MapFromBLL(internalDTO.Review review)
        {
            if (review == null) return null;
            
            var res = new externalDTO.Review
            {
                Id = review.Id,
                Rating = review.Rating,
                Comment = review.Comment,
                Added = review.Added,
                AppUserId = review.AppUserId,
                AppUser = AppUserMapper.MapFromBLL(review.AppUser),
                GameId = review.GameId,
                Game = GameMapper.MapFromBLL(review.Game),
                PlatformId = review.PlatformId,
                Platform = PlatformMapper.MapFromBLL(review.Platform)
            };

            return res;

        }

        public static internalDTO.Review MapFromExternal(externalDTO.Review review)
        {
            if (review == null) return null;
            
            var res = new internalDTO.Review
            {
                Id = review.Id,
                Rating = review.Rating,
                Comment = review.Comment,
                Added = review.Added,
                AppUserId = review.AppUserId,
                AppUser = AppUserMapper.MapFromExternal(review.AppUser),
                GameId = review.GameId,
                Game = GameMapper.MapFromExternal(review.Game),
                PlatformId = review.PlatformId,
                Platform = PlatformMapper.MapFromExternal(review.Platform)
            };

            return res;
        }
    }
}