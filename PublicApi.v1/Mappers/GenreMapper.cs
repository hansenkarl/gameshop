using System;
using externalDTO = PublicApi.v1.DTO;
using internalDTO = BLL.App.DTO;

namespace PublicApi.v1.Mappers
{
    public class GenreMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.Genre))
            {
                return MapFromBLL((internalDTO.Genre) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.Genre))
            {
                return MapFromExternal((externalDTO.Genre) inObject) as TOutObject;
            }

            throw new InvalidCastException("No conversion!");
        }

        public static externalDTO.Genre MapFromBLL(internalDTO.Genre genre)
        {
            if (genre == null) return null;
            
            var res = new externalDTO.Genre
            {
                Id = genre.Id,
                GenreName = genre.GenreName,
            };

            return res;
        }

        public static internalDTO.Genre MapFromExternal(externalDTO.Genre genre)
        {
            if (genre == null) return null;
            
            var res = new internalDTO.Genre
            {
                Id = genre.Id,
                GenreName = genre.GenreName,
            };

            return res;
        }
    }
}