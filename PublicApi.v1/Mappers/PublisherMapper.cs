using System;
using externalDTO = PublicApi.v1.DTO;
using internalDTO = BLL.App.DTO;

namespace PublicApi.v1.Mappers
{
    public class PublisherMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.Publisher))
            {
                return MapFromBLL((internalDTO.Publisher) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.Publisher))
            {
                return MapFromExternal((externalDTO.Publisher) inObject) as TOutObject;
            }

            throw new InvalidCastException("No conversion!");
        }

        public static externalDTO.Publisher MapFromBLL(internalDTO.Publisher publisher)
        {
            if (publisher == null) return null;
            
            var res = new externalDTO.Publisher
            {
                Id = publisher.Id,
                PublisherName = publisher.PublisherName,
            };

            return res;
        }

        public static internalDTO.Publisher MapFromExternal(externalDTO.Publisher publisher)
        {
            if (publisher == null) return null;
            
            var res = new internalDTO.Publisher
            {
                Id = publisher.Id,
                PublisherName = publisher.PublisherName,
            };

            return res;
        }
    }
}