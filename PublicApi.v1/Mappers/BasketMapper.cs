using System;
using System.Runtime.InteropServices;
using externalDTO = PublicApi.v1.DTO;
using internalDTO = BLL.App.DTO;


namespace PublicApi.v1.Mappers
{
    public class BasketMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.Basket))
            {
                return MapFromBLL((internalDTO.Basket) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.Basket))
            {
                return MapFromExternal((externalDTO.Basket) inObject) as TOutObject;
            }

            throw new InvalidCastException("No conversion!");
        }

        public static externalDTO.Basket MapFromBLL(internalDTO.Basket basket, [Optional] externalDTO.Billing billing)
        {
            if (basket == null) return null;
            
            var res = new externalDTO.Basket
            {
                Id = basket.Id,
                Created = basket.Created,
                AppUserId = basket.AppUserId,
                AppUser = AppUserMapper.MapFromBLL(basket.AppUser),
                BillingId = basket.BillingId,
                Billing = null,
                BasketStatusId = basket.BasketStatusId,
                BasketStatus = BasketStatusMapper.MapFromBLL(basket.BasketStatus)
            };
            
            res.Billing = billing ?? BillingMapper.MapFromBLL(basket.Billing, res);

            return res;

        }

        public static internalDTO.Basket MapFromExternal(externalDTO.Basket basket, [Optional] internalDTO.Billing billing)
        {
            if (basket == null) return null;
            
            var res = new internalDTO.Basket
            {
                Id = basket.Id,
                Created = basket.Created,
                AppUserId = basket.AppUserId,
                AppUser = AppUserMapper.MapFromExternal(basket.AppUser),
                BillingId = basket.BillingId,
                Billing = null,
                BasketStatusId = basket.BasketStatusId,
                BasketStatus = BasketStatusMapper.MapFromExternal(basket.BasketStatus)
            };
            
            res.Billing = billing ?? BillingMapper.MapFromExternal(basket.Billing, res);

            return res;
        }
    }
}