using System;
using externalDTO = PublicApi.v1.DTO;
using internalDTO = BLL.App.DTO;


namespace PublicApi.v1.Mappers
{
    public class WarehouseMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.Warehouse))
            {
                return MapFromBLL((internalDTO.Warehouse) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.Warehouse))
            {
                return MapFromExternal((externalDTO.Warehouse) inObject) as TOutObject;
            }

            throw new InvalidCastException("No conversion!");
        }

        public static externalDTO.Warehouse MapFromBLL(internalDTO.Warehouse warehouse)
        {
            if (warehouse == null) return null;
            
            var res = new externalDTO.Warehouse
            {
                Id = warehouse.Id,
                WarehouseName = warehouse.WarehouseName
            };

            return res;

        }

        public static internalDTO.Warehouse MapFromExternal(externalDTO.Warehouse warehouse)
        {
            if (warehouse == null) return null;
            
            var res = new internalDTO.Warehouse
            {
                Id = warehouse.Id,
                WarehouseName = warehouse.WarehouseName
            };

            return res;
        }
    }
}