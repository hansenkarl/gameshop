using System;
using externalDTO = PublicApi.v1.DTO;
using internalDTO = BLL.App.DTO;

namespace PublicApi.v1.Mappers
{
    public class BasketStatusMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.BasketStatus))
            {
                return MapFromBLL((internalDTO.BasketStatus) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.BasketStatus))
            {
                return MapFromExternal((externalDTO.BasketStatus) inObject) as TOutObject;
            }

            throw new InvalidCastException("No conversion!");
        }

        public static externalDTO.BasketStatus MapFromBLL(internalDTO.BasketStatus basketStatus)
        {
            if (basketStatus == null) return null;
            
            var res = new externalDTO.BasketStatus
            {
                Id = basketStatus.Id,
                StatusName = basketStatus.StatusName,
            };
            
            return res;
        }

        public static internalDTO.BasketStatus MapFromExternal(externalDTO.BasketStatus basketStatus)
        {
            if (basketStatus == null) return null;
            
            var res = new internalDTO.BasketStatus
            {
                Id = basketStatus.Id,
                StatusName = basketStatus.StatusName,
            };
            
            return res;
        }
    }
}