using System;
using System.Runtime.InteropServices;
using externalDTO = PublicApi.v1.DTO;
using internalDTO = BLL.App.DTO;

namespace PublicApi.v1.Mappers
{
    public class BillingMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.Billing))
            {
                return MapFromBLL((internalDTO.Billing) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.Billing))
            {
                return MapFromExternal((externalDTO.Billing) inObject) as TOutObject;
            }

            throw new InvalidCastException("No conversion!");
        }

        public static externalDTO.Billing MapFromBLL(internalDTO.Billing billing, [Optional] externalDTO.Basket basket)
        {
            if (billing == null) return null;
            
            var res = new externalDTO.Billing
            {
                Id = billing.Id,
                SumWithoutTax = billing.SumWithoutTax,
                Tax = billing.Tax,
                SumTotal = billing.SumTotal,
                Created = billing.Created,
                BasketId = billing.BasketId,
                Basket = null,
                CreditCardId = billing.CreditCardId,
                CreditCard = CreditCardMapper.MapFromBLL(billing.CreditCard),
                BillingStatusId = billing.BillingStatusId,
                BillingStatus = BillingStatusMapper.MapFromBLL(billing.BillingStatus)
            };

            res.Basket = basket ?? BasketMapper.MapFromBLL(billing.Basket, res);
            
            return res;
        }

        public static internalDTO.Billing MapFromExternal(externalDTO.Billing billing, [Optional] internalDTO.Basket basket)
        {
            if (billing == null) return null;
            
            var res = new internalDTO.Billing
            {
                Id = billing.Id,
                SumWithoutTax = billing.SumWithoutTax,
                Tax = billing.Tax,
                SumTotal = billing.SumTotal,
                Created = billing.Created,
                BasketId = billing.BasketId,
                Basket = null,
                CreditCardId = billing.CreditCardId,
                CreditCard = CreditCardMapper.MapFromExternal(billing.CreditCard),
                BillingStatusId = billing.BillingStatusId,
                BillingStatus = BillingStatusMapper.MapFromExternal(billing.BillingStatus)
            };

            res.Basket = basket ?? BasketMapper.MapFromExternal(billing.Basket, res);
            
            return res;
        }
    }
}