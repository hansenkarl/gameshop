using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PublicApi.v1.DTO
{
    public class Warehouse
    {
        public int Id { get; set; }
        
        [Required]
        [MaxLength(64)]
        [MinLength(2)]
        public string WarehouseName { get; set; }

        public ICollection<GameInWarehouse> GamesInWarehouse{ get; set; }
    }
}