using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PublicApi.v1.DTO
{
    public class Platform
    {
        public int Id { get; set; }
        
        [Required]
        [MaxLength(32)]
        [MinLength(2)]
        public string PlatformName { get; set; }

        public ICollection<Game> Games { get; set; }

        public ICollection<Review> Reviews { get; set; }
    }
}