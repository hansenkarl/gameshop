using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PublicApi.v1.DTO
{
    public class BasketStatus
    {
        public int Id { get; set; }
        
        [Required]
        [MaxLength(32)]
        [MinLength(2)]
        public string StatusName { get; set; }

        public ICollection<Basket> Baskets { get; set; }
    }
}