using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using PublicApi.v1.DTO.Identity;

namespace PublicApi.v1.DTO
{
    public class CreditCard
    {
        public int Id { get; set; }
        
        [Required]
        [MinLength(16, ErrorMessage = "Credit card must be 16 digits long.")]
        [MaxLength(16, ErrorMessage = "Credit card must be 16 digits long.")]
        public string CreditCardNr { get; set; }
        
        [Required]
        public DateTime ValidThru { get; set; }

        public int AppUserId { get; set; }
        public AppUser AppUser { get; set; }

        public ICollection<Billing> Billings { get; set; }
    }
}