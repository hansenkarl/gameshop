using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PublicApi.v1.DTO
{
    public class Billing
    {
        public int Id { get; set; }
        
        [Required]
        [Column(TypeName = "decimal(6,2)")]
        public decimal SumWithoutTax { get; set; }
        
        [Required]
        [Column(TypeName = "decimal(4,2)")]
        public decimal Tax { get; set; }
        
        [Required]
        [Column(TypeName = "decimal(6,2)")]
        public decimal SumTotal { get; set; }
        
        [Required]
        public DateTime Created { get; set; }

        public int? BasketId { get; set; }
        public Basket Basket { get; set; }
        
        public int? CreditCardId { get; set; }
        public CreditCard CreditCard { get; set; }

        public int BillingStatusId { get; set; }
        public BillingStatus BillingStatus { get; set; }
    }
}