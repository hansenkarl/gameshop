using System;
using System.ComponentModel.DataAnnotations;
using PublicApi.v1.DTO.Identity;

namespace PublicApi.v1.DTO
{
    public class Basket
    {
        public int Id { get; set; }
        
        [Required] 
        public DateTime Created { get; set; }

        public int AppUserId { get; set; }
        public AppUser AppUser { get; set; }

        public int? BillingId { get; set; }
        public Billing Billing { get; set; }

        public int BasketStatusId { get; set; }
        public BasketStatus BasketStatus { get; set; }
    }
}