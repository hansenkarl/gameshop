using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PublicApi.v1.DTO
{
    public class Publisher
    {
        public int Id { get; set; }
        
        [Required]
        [MaxLength(128)]
        [MinLength(2)]
        public string PublisherName { get; set; }

        public ICollection<Game> Games { get; set; }
    }
}