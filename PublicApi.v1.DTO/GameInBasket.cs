using System.ComponentModel.DataAnnotations;

namespace PublicApi.v1.DTO
{
    public class GameInBasket
    {
        public int Id { get; set; }
        
        [Required]
        [Range(1, 999)]
        public int Amount { get; set; }

        public int GameId { get; set; }
        public Game Game { get; set; }

        public int BasketId { get; set; }
        public Basket Basket { get; set; }
    }
}