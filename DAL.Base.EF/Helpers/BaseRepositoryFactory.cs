using System;
using System.Collections.Generic;
using Contracts.DAL.Base;
using Contracts.DAL.Base.Helpers;
using DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DAL.Base.EF.Helpers
{
    public class BaseRepositoryFactory<TDbContext> : IBaseRepositoryFactory<TDbContext>
        where TDbContext : DbContext

    {
        private readonly Dictionary<Type, Func<TDbContext, object>> _repositoryCreationMethods;

        public BaseRepositoryFactory() : this(new Dictionary<Type, Func<TDbContext, object>>())
        {
        }

        public BaseRepositoryFactory(Dictionary<Type, Func<TDbContext, object>> repositoryCreationMethods)
        {
            _repositoryCreationMethods = repositoryCreationMethods;
        }

        public void AddToCreationMethods<TRepository>(Func<TDbContext, TRepository> creationMethod)
            where TRepository : class
        {
            _repositoryCreationMethods.Add(typeof(TRepository), creationMethod);
        }

        public Func<TDbContext, object> GetRepositoryFactory<TRepository>()
        {
            if (_repositoryCreationMethods.ContainsKey(typeof(TRepository)))
            {
                return _repositoryCreationMethods[typeof(TRepository)];
            }

            throw new NullReferenceException("No repo creation method found for " + typeof(TRepository).FullName);
        }

        public Func<TDbContext, object> GetEntityRepositoryFactory<TDALEntity, TDomainEntity>()
            where TDALEntity : class, new()
            where TDomainEntity : class, IDomainEntity, new()

        {
            return (dataContext) =>
                new BaseRepository<TDALEntity, TDomainEntity, TDbContext>(dataContext,
                    new BaseDALMapper<TDALEntity, TDomainEntity>());
        }
    }
}