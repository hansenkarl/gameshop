using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Publisher : DomainEntity
    {
        [Required]
        [MaxLength(128)]
        [MinLength(2)]
        public string PublisherName { get; set; }

        public ICollection<Game> Games { get; set; }
    }
}