using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Warehouse : DomainEntity
    {
        [Required]
        [MaxLength(64)]
        [MinLength(2)]
        public string WarehouseName { get; set; }

        public ICollection<GameInWarehouse> GamesInWarehouse{ get; set; }
    }
}