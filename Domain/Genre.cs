using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Genre : DomainEntity
    {
        [Required]
        [MaxLength(64)]
        [MinLength(2)]
        public string GenreName { get; set; }

        public ICollection<Game> Games{ get; set; }
    }
}