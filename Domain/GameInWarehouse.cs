using System;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class GameInWarehouse : DomainEntity
    {
        [Required]
        [Range(0, int.MaxValue)]
        public int Amount { get; set; }

        [Required]
        public DateTime Added { get; set; }

        public int WarehouseId { get; set; }
        public Warehouse Warehouse { get; set; }

        public int GameId{ get; set; }
        public Game Game { get; set; }
    }
}