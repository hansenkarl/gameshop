using System;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class GameInBasket : DomainEntity
    {
        [Required]
        [Range(1, 999)]
        public int Amount { get; set; }

        public int GameId { get; set; }
        public Game Game { get; set; }

        public int BasketId { get; set; }
        public Basket Basket { get; set; }
    }
}