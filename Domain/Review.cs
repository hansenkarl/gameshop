using System;
using System.ComponentModel.DataAnnotations;
using Domain.Identity;

namespace Domain
{
    public class Review : DomainEntity
    {
        [Range(0, 10, ErrorMessage = "Please enter a rating between 0 - 10!")]
        public int Rating { get; set; }
        
        [Required]
        [MaxLength(4096)]
        [MinLength(2)]
        public string Comment { get; set; }
        
        [Required]
        public DateTime Added { get; set; }

        public int AppUserId { get; set; }
        public AppUser AppUser { get; set; }
        
        public int GameId { get; set; }
        public Game Game { get; set; }

        public int PlatformId { get; set; }
        public Platform Platform { get; set; }
    }
}