using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class BasketStatus : DomainEntity
    {        
        [Required]
        [MaxLength(32)]
        [MinLength(2)]
        public string StatusName { get; set; }

        public ICollection<Basket> Baskets { get; set; }
    }
}