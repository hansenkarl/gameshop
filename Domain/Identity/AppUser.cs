using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Contracts.DAL.Base;
using Microsoft.AspNetCore.Identity;

namespace Domain.Identity
{
    public class AppUser : IdentityUser<int>, IDomainEntity
    {
        [Required]
        [MaxLength(254)]
        [MinLength(2)]
        public string FirstName { get; set; }
        
        [Required]
        [MaxLength(254)]
        [MinLength(2)]
        public string LastName { get; set; }
        
        public ICollection<Address> Addresses { get; set; }
        public ICollection<Basket> Baskets { get; set; }
        public ICollection<CreditCard> CreditCards { get; set; }
        public ICollection<Review> Reviews { get; set; }
    }
}