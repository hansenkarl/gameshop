using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Platform : DomainEntity
    {
        [Required]
        [MaxLength(32)]
        [MinLength(2)]
        public string PlatformName { get; set; }

        public ICollection<Game> Games { get; set; }

        public ICollection<Review> Reviews { get; set; }
    }
}