using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Domain.Identity;

namespace Domain
{
    public class CreditCard : DomainEntity
    {
        [Required]
        [MinLength(16, ErrorMessage = "Credit card must be 16 digits long.")]
        [MaxLength(16, ErrorMessage = "Credit card must be 16 digits long.")]
        public string CreditCardNr { get; set; }
        
        [Required]
        public DateTime ValidThru { get; set; }

        public int AppUserId { get; set; }
        public AppUser AppUser { get; set; }

        public ICollection<Billing> Billings { get; set; }
    }
}