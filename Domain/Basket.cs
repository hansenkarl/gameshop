using System;
using Domain.Identity;

namespace Domain
{
    public class Basket : DomainEntity
    {
        public DateTime Created { get; set; }

        public int AppUserId { get; set; }
        public AppUser AppUser { get; set; }

        public int? BillingId { get; set; }
        public Billing Billing { get; set; }

        public int BasketStatusId { get; set; }
        public BasketStatus BasketStatus { get; set; }
    }
}