using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BLL.App.DTO
{
    public class Publisher
    {
        public int Id { get; set; }
        
        [Required]
        [MaxLength(128)]
        [MinLength(2)]
        [DisplayName("Publisher's name")]
        public string PublisherName { get; set; }

        public ICollection<Game> Games { get; set; }
    }
}