using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using BLL.App.DTO.Identity;

namespace BLL.App.DTO
{
    public class CreditCard
    {
        public int Id { get; set; }
        
        [Required]
        [MinLength(16, ErrorMessage = "Credit card must be 16 digits long.")]
        [MaxLength(16, ErrorMessage = "Credit card must be 16 digits long.")]
        [DisplayName("Credit card number")]
        public string CreditCardNr { get; set; }
        
        [Required]
        [DisplayName("Valid thru")]
        public DateTime ValidThru { get; set; }

        public int AppUserId { get; set; }
        public AppUser AppUser { get; set; }

        public ICollection<Billing> Billings { get; set; }
    }
}