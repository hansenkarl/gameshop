using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BLL.App.DTO
{
    public class Platform
    {
        public int Id { get; set; }
        
        [Required]
        [MaxLength(32)]
        [MinLength(2)]
        [DisplayName("Platform's name")]
        public string PlatformName { get; set; }

        public ICollection<Game> Games { get; set; }

        public ICollection<Review> Reviews { get; set; }
    }
}