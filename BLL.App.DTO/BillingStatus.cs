using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BLL.App.DTO
{
    public class BillingStatus
    {
        public int Id { get; set; }
        
        [Required]
        [MaxLength(32)]
        [MinLength(2)]
        public string StatusName { get; set; }

        public ICollection<Billing> Billings { get; set; }
    }
}