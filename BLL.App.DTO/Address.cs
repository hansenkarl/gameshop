﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using BLL.App.DTO.Identity;

namespace BLL.App.DTO
{
    public class Address
    {
        public int Id { get; set; }
        
        [Required]
        [MaxLength(128)]
        [MinLength(2)]
        public string Country { get; set; }
        
        [Required]
        [MaxLength(64)]
        [MinLength(2)]
        public string County { get; set; }
        
        [Required]
        [MaxLength(64)]
        [MinLength(2)]
        public string City { get; set; }
        
        [Required]
        [MaxLength(32)]
        [MinLength(2)]
        public string Street { get; set; }
        
        [Required]
        [MaxLength(16)]
        [MinLength(1)]
        [DisplayName("House number")]
        public string HouseNr { get; set; }
        
        [Required]
        [MaxLength(8)]
        [MinLength(1)]
        [DisplayName("Zip code")]
        public string ZipCode { get; set; }

        public int AppUserId { get; set; }
        public AppUser AppUser { get; set; }
    }
}