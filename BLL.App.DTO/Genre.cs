using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BLL.App.DTO
{
    public class Genre
    {
        public int Id { get; set; }
        
        [Required]
        [MaxLength(64)]
        [MinLength(2)]
        [DisplayName("Genre's name")]
        public string GenreName { get; set; }

        public ICollection<Game> Games{ get; set; }
    }
}