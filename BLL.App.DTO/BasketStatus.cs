using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BLL.App.DTO
{
    public class BasketStatus
    {
        public int Id { get; set; }
        
        [Required]
        [MaxLength(32)]
        [MinLength(2)]
        [DisplayName("Status name")]
        public string StatusName { get; set; }

        public ICollection<Basket> Baskets { get; set; }
    }
}