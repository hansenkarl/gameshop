using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BLL.App.DTO
{
    public class Warehouse
    {
        public int Id { get; set; }
        
        [Required]
        [MaxLength(64)]
        [MinLength(2)]
        [DisplayName("Warehouse's name")]
        public string WarehouseName { get; set; }

        public ICollection<GameInWarehouse> GamesInWarehouse{ get; set; }
    }
}