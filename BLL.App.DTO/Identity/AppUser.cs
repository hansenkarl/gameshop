using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BLL.App.DTO.Identity
{
    public class AppUser
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(254)]
        [MinLength(2)]
        public string FirstName { get; set; }
        
        [Required]
        [MaxLength(254)]
        [MinLength(2)]
        public string LastName { get; set; }
        
        public ICollection<Address> Addresses { get; set; }
        public ICollection<Basket> Baskets { get; set; }
        public ICollection<CreditCard> CreditCards { get; set; }
        public ICollection<Review> Reviews { get; set; }
    }
}