using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BLL.App.DTO
{
    public class Billing
    {
        public int Id { get; set; }
        
        [Required]
        [Column(TypeName = "decimal(6,2)")]
        [DisplayName("Sum without tax")]
        public decimal SumWithoutTax { get; set; }
        
        [Required]
        [Column(TypeName = "decimal(4,2)")]
        public decimal Tax { get; set; }
        
        [Required]
        [Column(TypeName = "decimal(6,2)")]
        [DisplayName("Sum total")]
        public decimal SumTotal { get; set; }
        
        [Required]
        public DateTime Created { get; set; }

        public int? BasketId { get; set; }
        public Basket Basket { get; set; }
        
        public int? CreditCardId { get; set; }
        [DisplayName("Credit card")]
        public CreditCard CreditCard { get; set; }

        public int BillingStatusId { get; set; }
        [DisplayName("Billing status")]
        public BillingStatus BillingStatus { get; set; }
    }
}