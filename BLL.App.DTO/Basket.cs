using System;
using System.ComponentModel;
using BLL.App.DTO.Identity;


namespace BLL.App.DTO
{
    public class Basket
    {
        public int Id { get; set; }
        
        public DateTime Created { get; set; }

        public int AppUserId { get; set; }
        public AppUser AppUser { get; set; }

        public int? BillingId { get; set; }
        public Billing Billing { get; set; }

        public int BasketStatusId { get; set; }
        [DisplayName("Basket status")]
        public BasketStatus BasketStatus { get; set; }
    }
}