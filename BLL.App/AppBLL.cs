﻿using BLL.Base;
using Contracts.BLL.App;
using Contracts.BLL.App.Services;
using Contracts.BLL.Base.Helpers;
using Contracts.DAL.App;

namespace BLL.App
{
    public class AppBLL : BaseBLL<IAppUnitOfWork>, IAppBLL
    {
        protected readonly IAppUnitOfWork AppUnitOfWork;
        public AppBLL(IAppUnitOfWork uow, IBaseServiceProvider serviceProvider, IAppUnitOfWork appUnitOfWork) 
            : base(uow, serviceProvider)
        {
            AppUnitOfWork = appUnitOfWork;
        }

        public IAddressService Addresses => ServiceProvider.GetService<IAddressService>();
        public IAppUserService AppUsers => ServiceProvider.GetService<IAppUserService>();
        public IAppRoleService AppRoles => ServiceProvider.GetService<IAppRoleService>();
        public IBasketService Baskets => ServiceProvider.GetService<IBasketService>();
        public IBasketStatusService BasketStatuses => ServiceProvider.GetService<IBasketStatusService>();
        public IBillingService Billings => ServiceProvider.GetService<IBillingService>();
        public IBillingStatusService BillingStatuses => ServiceProvider.GetService<IBillingStatusService>();
        public ICreditCardService CreditCards => ServiceProvider.GetService<ICreditCardService>();
        public IGameService Games => ServiceProvider.GetService<IGameService>();
        public IGameInBasketService GameInBaskets => ServiceProvider.GetService<IGameInBasketService>();
        public IGameInWarehouseService GameInWarehouses => ServiceProvider.GetService<IGameInWarehouseService>();
        public IGenreService Genres => ServiceProvider.GetService<IGenreService>();
        public IPlatformService Platforms => ServiceProvider.GetService<IPlatformService>();
        public IPublisherService Publishers => ServiceProvider.GetService<IPublisherService>();
        public IReviewService Reviews => ServiceProvider.GetService<IReviewService>();
        public IWarehouseService Warehouses => ServiceProvider.GetService<IWarehouseService>();
    }
}