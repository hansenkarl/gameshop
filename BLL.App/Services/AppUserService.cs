using BLL.App.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;

namespace BLL.App.Services
{
    public class AppUserService :
        BaseEntityService<BLL.App.DTO.Identity.AppUser, DAL.App.DTO.Identity.AppUser, IAppUnitOfWork>,
        IAppUserService
    {
        public AppUserService(IAppUnitOfWork uow) : base(new AppUserMapper(), uow)
        {
            ServiceRepository = Uow.AppUsers;
        }
    }
}