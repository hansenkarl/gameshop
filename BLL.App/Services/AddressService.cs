using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;


namespace BLL.App.Services
{
    public class AddressService : BaseEntityService<BLL.App.DTO.Address, DAL.App.DTO.Address, IAppUnitOfWork>,
        IAddressService
    {
        public AddressService(IAppUnitOfWork uow) : base(new AddressMapper(), uow)
        {
            ServiceRepository = Uow.Addresses;
        }

        public async Task<List<BLL.App.DTO.Address>> AllForUserAsync(int userId)
        {
            return (await Uow.Addresses.AllForUserAsync(userId))
                .Select(a => AddressMapper.MapFromInternal(a))
                .ToList();
        }

        public async Task<BLL.App.DTO.Address> FindForUserAsync(int id, int userId)
        {
            return AddressMapper.MapFromInternal(await Uow.Addresses.FindForUserAsync(id, userId));
        }

        public async Task<bool> BelongsToUserAsync(int id, int userId)
        {
            return await Uow.Addresses.BelongsToUserAsync(id, userId);
        }
    }
}