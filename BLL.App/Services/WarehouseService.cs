using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;

namespace BLL.App.Services
{
    public class WarehouseService :
        BaseEntityService<BLL.App.DTO.Warehouse, DAL.App.DTO.Warehouse, IAppUnitOfWork>,
        IWarehouseService
    {
        public WarehouseService(IAppUnitOfWork uow) : base(new WarehouseMapper(), uow)
        {
            ServiceRepository = Uow.Warehouses;
        }

        public new async Task<List<BLL.App.DTO.Warehouse>> AllAsync()
        {
            return (await Uow.Warehouses.AllAsync()).Select(warehouse => WarehouseMapper.MapFromInternal(warehouse)).ToList();
        }

        public new async Task<BLL.App.DTO.Warehouse> FindAsync(params object[] id)
        {
            return WarehouseMapper.MapFromInternal(await Uow.Warehouses.FindAsync(id));
        }
    }
}