using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;

namespace BLL.App.Services
{
    public class BasketStatusService :
        BaseEntityService<BLL.App.DTO.BasketStatus, DAL.App.DTO.BasketStatus, IAppUnitOfWork>,
        IBasketStatusService
    {
        public BasketStatusService(IAppUnitOfWork uow) : base(new BasketStatusMapper(), uow)
        {
            ServiceRepository = Uow.BasketStatuses;
        }

        public new async Task<List<BLL.App.DTO.BasketStatus>> AllAsync()
        {
            return (await Uow.BasketStatuses.AllAsync())
                .Select(basketStatus => BasketStatusMapper.MapFromInternal(basketStatus)).ToList();
        }

        public new async Task<BLL.App.DTO.BasketStatus> FindAsync(params object[] id)
        {
            return BasketStatusMapper.MapFromInternal(await Uow.BasketStatuses.FindAsync(id));
        }
    }
}