using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.DTO;
using BLL.App.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;

namespace BLL.App.Services
{
    public class BillingService : BaseEntityService<BLL.App.DTO.Billing, DAL.App.DTO.Billing, IAppUnitOfWork>,
        IBillingService
    {
        public BillingService(IAppUnitOfWork uow) : base(new BillingMapper(), uow)
        {
            ServiceRepository = Uow.Billings;
        }

        public async Task<List<Billing>> AllForUserAsync(int userId)
        {
            return (await Uow.Billings.AllForUserAsync(userId))
                .Select(b => BillingMapper.MapFromInternal(b))
                .ToList();
        }

        public async Task<Billing> FindForUserAsync(int id, int userId)
        {
            return BillingMapper.MapFromInternal(await Uow.Billings.FindForUserAsync(id, userId));
        }

        public async Task<bool> BelongsToUserAsync(int id, int userId)
        {
            return await Uow.Billings.BelongsToUserAsync(id, userId);
        }

        public async Task<Billing> GetBilling(int id)
        {
            return BillingMapper.MapFromInternal(await Uow.Billings.GetBilling(id));
        }
        
        public async Task<Billing> CreateBilling()
        {
            return BillingMapper.MapFromInternal(await Uow.Billings.CreateBilling());
        }
    }
}