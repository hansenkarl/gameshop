using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;

namespace BLL.App.Services
{
    public class GameService : BaseEntityService<BLL.App.DTO.Game, DAL.App.DTO.Game, IAppUnitOfWork>,
        IGameService
    {
        public GameService(IAppUnitOfWork uow) : base(new GameMapper(), uow)
        {
            ServiceRepository = Uow.Games;
        }

        public new async Task<List<BLL.App.DTO.Game>> AllAsync()
        {
            return (await Uow.Games.AllAsync()).Select(game => GameMapper.MapFromInternal(game)).ToList();
        }

        public new async Task<BLL.App.DTO.Game> FindAsync(params object[] id)
        {
            return GameMapper.MapFromInternal(await Uow.Games.FindAsync(id));
        }
    }
}