using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.DTO;
using BLL.App.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;

namespace BLL.App.Services
{
    public class GameInWarehouseService :
        BaseEntityService<BLL.App.DTO.GameInWarehouse, DAL.App.DTO.GameInWarehouse, IAppUnitOfWork>,
        IGameInWarehouseService
    {
        public GameInWarehouseService(IAppUnitOfWork uow) : base(new GameInWarehouseMapper(), uow)
        {
            ServiceRepository = Uow.GameInWarehouses;
        }
        
        public new async Task<List<GameInWarehouse>> AllAsync()
        {
            return (await Uow.GameInWarehouses.AllAsync())
                .Select(gameInWarehouse => GameInWarehouseMapper.MapFromInternal(gameInWarehouse))
                .ToList();
        }

        public new async Task<GameInWarehouse> FindAsync(params object[] id)
        {
            return GameInWarehouseMapper.MapFromInternal(await Uow.GameInWarehouses.FindAsync(id));
        }
    }
}