using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.DTO;
using BLL.App.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;

namespace BLL.App.Services
{
    public class BillingStatusService :
        BaseEntityService<BLL.App.DTO.BillingStatus, DAL.App.DTO.BillingStatus, IAppUnitOfWork>, IBillingStatusService
    {
        public BillingStatusService(IAppUnitOfWork uow) : base(new BillingStatusMapper(), uow)
        {
            ServiceRepository = Uow.BillingStatuses;
        }

        public new async Task<List<BillingStatus>> AllAsync()
        {
            return (await Uow.BillingStatuses.AllAsync()).Select(billingStatus =>
                BillingStatusMapper.MapFromInternal(billingStatus)).ToList();
        }

        public new async Task<BillingStatus> FindAsync(params object[] id)
        {
            return BillingStatusMapper.MapFromInternal(await Uow.BillingStatuses.FindAsync(id));
        }
    }
}