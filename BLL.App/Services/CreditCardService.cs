using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.Mappers;
using BLL.Base.Helpers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using DAL.App.DTO;
using CreditCard = BLL.App.DTO.CreditCard;

namespace BLL.App.Services
{
    public class CreditCardService :
        BaseEntityService<BLL.App.DTO.CreditCard, DAL.App.DTO.CreditCard, IAppUnitOfWork>,
        ICreditCardService
    {
        public CreditCardService(IAppUnitOfWork uow) : base(new CreditCardMapper(), uow)
        {
            ServiceRepository = Uow.CreditCards;
        }

        public async Task<List<CreditCard>> AllForUserAsync(int userId)
        {
            return (await Uow.CreditCards.AllForUserAsync(userId))
                .Select(c => CreditCardMapper.MapFromInternal(c))
                .ToList();
        }

        public async Task<CreditCard> FindForUserAsync(int id, int userId)
        {
            return CreditCardMapper.MapFromInternal(await Uow.CreditCards.FindForUserAsync(id, userId));
        }

        public async Task<bool> BelongsToUserAsync(int id, int userId)
        {
            return await Uow.CreditCards.BelongsToUserAsync(id, userId);
        }
    }
}