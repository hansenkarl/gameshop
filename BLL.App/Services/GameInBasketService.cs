using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using GameInBasket = BLL.App.DTO.GameInBasket;

namespace BLL.App.Services
{
    public class GameInBasketService :
        BaseEntityService<BLL.App.DTO.GameInBasket, DAL.App.DTO.GameInBasket, IAppUnitOfWork>,
        IGameInBasketService
    {
        public GameInBasketService(IAppUnitOfWork uow) : base(new GameInBasketMapper(), uow)
        {
            ServiceRepository = Uow.GameInBaskets;
        }

        public async Task<List<GameInBasket>> AllForUserAsync(int userId)
        {
            return (await Uow.GameInBaskets.AllForUserAsync(userId))
                .Select(gameInBasket => GameInBasketMapper.MapFromInternal(gameInBasket)).ToList();
        }

        public async Task<GameInBasket> FindForUserAsync(int id, int userId)
        {
            return GameInBasketMapper.MapFromInternal(await Uow.GameInBaskets.FindForUserAsync(id, userId));
        }

        public async Task<bool> BelongsToUserAsync(int id, int userId)
        {
            return await Uow.GameInBaskets.BelongsToUserAsync(id, userId);
        }

        public async Task<List<GameInBasket>> AllForBasketAsync(int basketId)
        {
            return (await Uow.GameInBaskets.AllForBasketAsync(basketId))
                .Select(gameInBasket => GameInBasketMapper.MapFromInternal(gameInBasket)).ToList();
        }
    }
}