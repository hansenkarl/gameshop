using BLL.App.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;

namespace BLL.App.Services
{
    public class AppRoleService :
        BaseEntityService<BLL.App.DTO.Identity.AppRole, DAL.App.DTO.Identity.AppRole, IAppUnitOfWork>,
        IAppRoleService
    {
        public AppRoleService(IAppUnitOfWork uow) : base(new AppRoleMapper(), uow)
        {
            ServiceRepository = Uow.AppRoles;
        }
    }
}