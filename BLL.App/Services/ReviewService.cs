using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.Mappers;
using BLL.Base.Helpers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using DAL.App.DTO;

namespace BLL.App.Services
{
    public class ReviewService :
        BaseEntityService<BLL.App.DTO.Review, DAL.App.DTO.Review, IAppUnitOfWork>, IReviewService
    {
        public ReviewService(IAppUnitOfWork uow) : base(new ReviewMapper(), uow)
        {
            ServiceRepository = Uow.Reviews;
        }

        public new async Task<List<BLL.App.DTO.Review>> AllAsync()
        {
            return (await Uow.Reviews.AllAsync()).Select(review => ReviewMapper.MapFromInternal(review)).ToList();
        }

        public new async Task<BLL.App.DTO.Review> FindAsync(params object[] id)
        {
            return ReviewMapper.MapFromInternal(await Uow.Reviews.FindAsync(id));
        }
    }
}