using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.Mappers;
using BLL.Base.Helpers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using DAL.App.DTO;
using Publisher = BLL.App.DTO.Publisher;

namespace BLL.App.Services
{
    public class PublisherService :
        BaseEntityService<BLL.App.DTO.Publisher, DAL.App.DTO.Publisher, IAppUnitOfWork>,
        IPublisherService
    {
        public PublisherService(IAppUnitOfWork uow) : base(new PublisherMapper(), uow)
        {
            ServiceRepository = Uow.Publishers;
        }
        
        public new async Task<List<BLL.App.DTO.Publisher>> AllAsync()
        {
            return (await Uow.Publishers.AllAsync())
                .Select(publisher => PublisherMapper.MapFromInternal(publisher)).ToList();
        }

        public new async Task<BLL.App.DTO.Publisher> FindAsync(params object[] id)
        {
            return PublisherMapper.MapFromInternal(await Uow.Publishers.FindAsync(id));
        }

        public async Task<Publisher> GetPublisher(int id)
        {
            return PublisherMapper.MapFromInternal(await Uow.Publishers.GetPublisher(id));
        }
    }
}