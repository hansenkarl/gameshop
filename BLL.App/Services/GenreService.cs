using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.Mappers;
using BLL.Base.Helpers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using DAL.App.DTO;
using Genre = BLL.App.DTO.Genre;

namespace BLL.App.Services
{
    public class GenreService : BaseEntityService<BLL.App.DTO.Genre, DAL.App.DTO.Genre, IAppUnitOfWork>,
        IGenreService
    {
        public GenreService(IAppUnitOfWork uow) : base(new GenreMapper(), uow)
        {
            ServiceRepository = Uow.Genres;
        }

        public new async Task<List<BLL.App.DTO.Genre>> AllAsync()
        {
            return (await Uow.Genres.AllAsync()).Select(genre => GenreMapper.MapFromInternal(genre)).ToList();
        }

        public new async Task<BLL.App.DTO.Genre> FindAsync(params object[] id)
        {
            return GenreMapper.MapFromInternal(await Uow.Genres.FindAsync(id));
        }

        public async Task<Genre> GetGenre(int id)
        {
            return GenreMapper.MapFromInternal(await Uow.Genres.GetGenre(id));
        }
    }
}