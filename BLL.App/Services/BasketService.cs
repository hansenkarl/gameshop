using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.DTO;
using BLL.App.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;


namespace BLL.App.Services
{
    public class BasketService :
        BaseEntityService<BLL.App.DTO.Basket, DAL.App.DTO.Basket, IAppUnitOfWork>, IBasketService
    {
        public BasketService(IAppUnitOfWork uow) : base(new BasketMapper(), uow)
        {
            ServiceRepository = Uow.Baskets;
        }

        public async Task<List<BLL.App.DTO.Basket>> AllForUserAsync(int userId)
        {
            return (await Uow.Baskets.AllForUserAsync(userId))
                .Select(b => BasketMapper.MapFromInternal(b))
                .ToList();
        }

        public async Task<BLL.App.DTO.Basket> FindForUserAsync(int id, int userId)
        {
            return BasketMapper.MapFromInternal(await Uow.Baskets.FindForUserAsync(id, userId));
        }

        public async Task<bool> BelongsToUserAsync(int id, int userId)
        {
            return await Uow.Baskets.BelongsToUserAsync(id, userId);
        }
        
        public async Task<Basket> GetBasket(int id)
        {
            return BasketMapper.MapFromInternal(await Uow.Baskets.GetBasket(id));
        }

        public async Task<Basket> CreateBasket()
        {
            return BasketMapper.MapFromInternal(await Uow.Baskets.CreateBasket());
        }

        public async Task<List<Basket>> AllNonCheckedOutUserAsync(List<Basket> baskets)
        {
            List<DAL.App.DTO.Basket> lollpead = new List<DAL.App.DTO.Basket>();
            
            foreach (var basket in baskets)
            {
                lollpead.Add(BasketMapper.MapFromExternal(basket));
            }
            
            return (await Uow.Baskets.AllNonCheckedOutUserAsync(lollpead))
                .Select(b => BasketMapper.MapFromInternal(b))
                .ToList();
        }
    }
}