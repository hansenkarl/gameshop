using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BLL.App.DTO;
using BLL.App.Mappers;
using BLL.Base.Services;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;

namespace BLL.App.Services
{
    public class PlatformService :
        BaseEntityService<BLL.App.DTO.Platform, DAL.App.DTO.Platform, IAppUnitOfWork>, IPlatformService
    {
        public PlatformService(IAppUnitOfWork uow) : base(new PlatformMapper(), uow)
        {
            ServiceRepository = Uow.Platforms;
        }

        public new async Task<List<BLL.App.DTO.Platform>> AllAsync()
        {
            return (await Uow.Platforms.AllAsync()).Select(platform => PlatformMapper.MapFromInternal(platform))
                .ToList();
        }

        public new async Task<BLL.App.DTO.Platform> FindAsync(params object[] id)
        {
            return PlatformMapper.MapFromInternal(await Uow.Platforms.FindAsync(id));
        }

        public async Task<Platform> GetPlatform(int id)
        {
            return PlatformMapper.MapFromInternal(await Uow.Platforms.GetPlatform(id));
        }
    }
}