using System.Net.Http.Headers;
using BLL.App.Services;
using BLL.Base.Helpers;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;

namespace BLL.App.Helpers
{
    public class AppServiceFactory : BaseServiceFactory<IAppUnitOfWork>
    {
        public AppServiceFactory()
        {
            RegisterServices();
        }

        private void RegisterServices()
        {
            AddToCreationMethods<IAddressService>(uow => new AddressService(uow));
            AddToCreationMethods<IAppUserService>(uow => new AppUserService(uow));
            AddToCreationMethods<IAppRoleService>(uow => new AppRoleService(uow));
            AddToCreationMethods<IBasketService>(uow => new BasketService(uow));
            AddToCreationMethods<IBasketStatusService>(uow => new BasketStatusService(uow));
            AddToCreationMethods<IBillingService>(uow => new BillingService(uow));
            AddToCreationMethods<IBillingStatusService>(uow => new BillingStatusService(uow));
            AddToCreationMethods<ICreditCardService>(uow => new CreditCardService(uow));
            AddToCreationMethods<IGameService>(uow => new GameService(uow));
            AddToCreationMethods<IGameInBasketService>(uow => new GameInBasketService(uow));
            AddToCreationMethods<IGameInWarehouseService>(uow => new GameInWarehouseService(uow));
            AddToCreationMethods<IGenreService>(uow => new GenreService(uow));
            AddToCreationMethods<IPlatformService>(uow => new PlatformService(uow));
            AddToCreationMethods<IPublisherService>(uow => new PublisherService(uow));
            AddToCreationMethods<IReviewService>(uow => new ReviewService(uow));
            AddToCreationMethods<IWarehouseService>(uow => new WarehouseService(uow));
        }
    }
}