using System;
using Contracts.BLL.Base.Helpers;
using internalDTO = DAL.App.DTO;
using externalDTO = BLL.App.DTO;

namespace BLL.App.Mappers
{
    public class ReviewMapper : IBaseBLLMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.Review))
            {
                return MapFromInternal((internalDTO.Review) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.Review))
            {
                return MapFromExternal((externalDTO.Review) inObject) as TOutObject;
            }

            throw new InvalidCastException("No conversion!");
        }

        public static externalDTO.Review MapFromInternal(internalDTO.Review review)
        {
            if (review == null) return null;
            
            var res = new externalDTO.Review
            {
                Id = review.Id,
                Rating = review.Rating,
                Comment = review.Comment,
                Added = review.Added,
                AppUserId = review.AppUserId,
                AppUser = AppUserMapper.MapFromInternal(review.AppUser),
                GameId = review.GameId,
                Game = GameMapper.MapFromInternal(review.Game),
                PlatformId = review.PlatformId,
                Platform = PlatformMapper.MapFromInternal(review.Platform)
            };

            return res;

        }

        public static internalDTO.Review MapFromExternal(externalDTO.Review review)
        {
            if (review == null) return null;
            
            var res = new internalDTO.Review
            {
                Id = review.Id,
                Rating = review.Rating,
                Comment = review.Comment,
                Added = review.Added,
                AppUserId = review.AppUserId,
                AppUser = AppUserMapper.MapFromExternal(review.AppUser),
                GameId = review.GameId,
                Game = GameMapper.MapFromExternal(review.Game),
                PlatformId = review.PlatformId,
                Platform = PlatformMapper.MapFromExternal(review.Platform)
            };

            return res;
        }
    }
}