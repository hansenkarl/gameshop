using System;
using System.Runtime.InteropServices;
using Contracts.BLL.Base.Helpers;
using internalDTO = DAL.App.DTO;
using externalDTO = BLL.App.DTO;

namespace BLL.App.Mappers
{
    public class BasketMapper : IBaseBLLMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.Basket))
            {
                return MapFromInternal((internalDTO.Basket) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.Basket))
            {
                return MapFromExternal((externalDTO.Basket) inObject) as TOutObject;
            }

            throw new InvalidCastException("No conversion!");
        }

        public static externalDTO.Basket MapFromInternal(internalDTO.Basket basket, [Optional] externalDTO.Billing billing)
        {
            if (basket == null) return null;
            
            var res = new externalDTO.Basket
            {
                Id = basket.Id,
                Created = basket.Created,
                AppUserId = basket.AppUserId,
                AppUser = AppUserMapper.MapFromInternal(basket.AppUser),
                BillingId = basket.BillingId,
                Billing = null,
                BasketStatusId = basket.BasketStatusId,
                BasketStatus = BasketStatusMapper.MapFromInternal(basket.BasketStatus)
            };
            
            res.Billing = billing ?? BillingMapper.MapFromInternal(basket.Billing, res);

            return res;

        }

        public static internalDTO.Basket MapFromExternal(externalDTO.Basket basket, [Optional] internalDTO.Billing billing)
        {
            if (basket == null) return null;
            
            var res = new internalDTO.Basket
            {
                Id = basket.Id,
                Created = basket.Created,
                AppUserId = basket.AppUserId,
                AppUser = AppUserMapper.MapFromExternal(basket.AppUser),
                BillingId = basket.BillingId,
                Billing = null,
                BasketStatusId = basket.BasketStatusId,
                BasketStatus = BasketStatusMapper.MapFromExternal(basket.BasketStatus)
            };
            
            res.Billing = billing ?? BillingMapper.MapFromExternal(basket.Billing, res);

            return res;
        }
    }
}