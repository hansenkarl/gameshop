using System;
using Contracts.BLL.Base.Helpers;
using internalDTO = DAL.App.DTO;
using externalDTO = BLL.App.DTO;

namespace BLL.App.Mappers
{
    public class AppRoleMapper : IBaseBLLMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.Identity.AppRole))
            {
                return MapFromInternal((internalDTO.Identity.AppRole) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.Identity.AppRole))
            {
                return MapFromExternal((externalDTO.Identity.AppRole) inObject) as TOutObject;
            }

            throw new InvalidCastException("No conversion!");
        }
        
        public static externalDTO.Identity.AppRole MapFromInternal(internalDTO.Identity.AppRole appRole)
        {
            if (appRole == null) return null;
            
            var res = new externalDTO.Identity.AppRole
            {
                Id = appRole.Id,
            };

            return res;
        }

        public static internalDTO.Identity.AppRole MapFromExternal(externalDTO.Identity.AppRole appRole)
        {
            if (appRole == null) return null;
            
            var res = new internalDTO.Identity.AppRole
            {
                Id = appRole.Id,

            };

            return res;
        }
    }
}