using System;
using Contracts.BLL.Base.Helpers;
using internalDTO = DAL.App.DTO;
using externalDTO = BLL.App.DTO;

namespace BLL.App.Mappers
{
    public class WarehouseMapper : IBaseBLLMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.Warehouse))
            {
                return MapFromInternal((internalDTO.Warehouse) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.Warehouse))
            {
                return MapFromExternal((externalDTO.Warehouse) inObject) as TOutObject;
            }

            throw new InvalidCastException("No conversion!");
        }

        public static externalDTO.Warehouse MapFromInternal(internalDTO.Warehouse warehouse)
        {
            if (warehouse == null) return null;
            
            var res = new externalDTO.Warehouse
            {
                Id = warehouse.Id,
                WarehouseName = warehouse.WarehouseName
            };

            return res;

        }

        public static internalDTO.Warehouse MapFromExternal(externalDTO.Warehouse warehouse)
        {
            if (warehouse == null) return null;
            
            var res = new internalDTO.Warehouse
            {
                Id = warehouse.Id,
                WarehouseName = warehouse.WarehouseName
            };

            return res;
        }
    }
}