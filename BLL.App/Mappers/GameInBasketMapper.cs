using System;
using Contracts.BLL.Base.Helpers;
using internalDTO = DAL.App.DTO;
using externalDTO = BLL.App.DTO;

namespace BLL.App.Mappers
{
    public class GameInBasketMapper : IBaseBLLMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.GameInBasket))
            {
                return MapFromInternal((internalDTO.GameInBasket) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.GameInBasket))
            {
                return MapFromExternal((externalDTO.GameInBasket) inObject) as TOutObject;
            }

            throw new InvalidCastException("No conversion!");
        }

        public static externalDTO.GameInBasket MapFromInternal(internalDTO.GameInBasket gameInBasket)
        {
            if (gameInBasket == null) return null;
                
            var res = new externalDTO.GameInBasket
            {
                Id = gameInBasket.Id,
                Amount = gameInBasket.Amount,
                GameId = gameInBasket.GameId,
                Game = GameMapper.MapFromInternal(gameInBasket.Game),
                BasketId = gameInBasket.BasketId,
                Basket = BasketMapper.MapFromInternal(gameInBasket.Basket)
            };

            return res;

        }

        public static internalDTO.GameInBasket MapFromExternal(externalDTO.GameInBasket gameInBasket)
        {
            if (gameInBasket == null) return null;
            
            var res = new internalDTO.GameInBasket
            {
                Id = gameInBasket.Id,
                Amount = gameInBasket.Amount,
                GameId = gameInBasket.GameId,
                Game = GameMapper.MapFromExternal(gameInBasket.Game),
                BasketId = gameInBasket.BasketId,
                Basket = BasketMapper.MapFromExternal(gameInBasket.Basket)
            };

            return res;
        }
    }
}