using System;
using Contracts.BLL.Base.Helpers;
using internalDTO = DAL.App.DTO;
using externalDTO = BLL.App.DTO;

namespace BLL.App.Mappers
{
    public class GenreMapper : IBaseBLLMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.Genre))
            {
                return MapFromInternal((internalDTO.Genre) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.Genre))
            {
                return MapFromExternal((externalDTO.Genre) inObject) as TOutObject;
            }

            throw new InvalidCastException("No conversion!");
        }

        public static externalDTO.Genre MapFromInternal(internalDTO.Genre genre)
        {
            if (genre == null) return null;
            
            var res = new externalDTO.Genre
            {
                Id = genre.Id,
                GenreName = genre.GenreName,
            };

            return res;
        }

        public static internalDTO.Genre MapFromExternal(externalDTO.Genre genre)
        {
            if (genre == null) return null;
            
            var res = new internalDTO.Genre
            {
                Id = genre.Id,
                GenreName = genre.GenreName,
            };

            return res;
        }
    }
}