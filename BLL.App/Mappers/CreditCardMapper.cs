using System;
using Contracts.BLL.Base.Helpers;
using internalDTO = DAL.App.DTO;
using externalDTO = BLL.App.DTO;

namespace BLL.App.Mappers
{
    public class CreditCardMapper : IBaseBLLMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.CreditCard))
            {
                return MapFromInternal((internalDTO.CreditCard) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.CreditCard))
            {
                return MapFromExternal((externalDTO.CreditCard) inObject) as TOutObject;
            }

            throw new InvalidCastException("No conversion!");
        }

        public static externalDTO.CreditCard MapFromInternal(internalDTO.CreditCard creditCard)
        {
            if (creditCard == null) return null;
            
            var res = new externalDTO.CreditCard
            {
                Id = creditCard.Id,
                CreditCardNr = creditCard.CreditCardNr,
                ValidThru = creditCard.ValidThru,
                AppUserId = creditCard.AppUserId,
                AppUser = AppUserMapper.MapFromInternal(creditCard.AppUser),
            };

            return res;
        }

        public static internalDTO.CreditCard MapFromExternal(externalDTO.CreditCard creditCard)
        {
            if (creditCard == null) return null;
            
            var res = new internalDTO.CreditCard
            {
                Id = creditCard.Id,
                CreditCardNr = creditCard.CreditCardNr,
                ValidThru = creditCard.ValidThru,
                AppUserId = creditCard.AppUserId,
                AppUser = AppUserMapper.MapFromExternal(creditCard.AppUser),
            };

            return res;
        }
    }
}