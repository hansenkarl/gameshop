using System;
using Contracts.BLL.Base.Helpers;
using internalDTO = DAL.App.DTO;
using externalDTO = BLL.App.DTO;

namespace BLL.App.Mappers
{
    public class AddressMapper : IBaseBLLMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.Address))
            {
                return MapFromInternal((internalDTO.Address) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.Address))
            {
                return MapFromExternal((externalDTO.Address) inObject) as TOutObject;
            }

            throw new InvalidCastException("No conversion!");
        }

        public static externalDTO.Address MapFromInternal(internalDTO.Address address)
        {
            var res = new externalDTO.Address
            {
                Id = address.Id,
                Country = address.Country,
                County = address.County,
                City = address.City,
                Street = address.Street,
                HouseNr = address.HouseNr,
                ZipCode = address.ZipCode,
                AppUserId = address.AppUserId,
                AppUser = AppUserMapper.MapFromInternal(address.AppUser)
            };
            return res;
        }

        public static internalDTO.Address MapFromExternal(externalDTO.Address address)
        {
            var res = new internalDTO.Address
            {
                Id = address.Id,
                Country = address.Country,
                County = address.County,
                City = address.City,
                Street = address.Street,
                HouseNr = address.HouseNr,
                ZipCode = address.ZipCode,
                AppUserId = address.AppUserId,
                AppUser = AppUserMapper.MapFromExternal(address.AppUser)
            };
            return res;
        }
    }
}