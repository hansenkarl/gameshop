using System;
using Contracts.BLL.Base.Helpers;
using internalDTO = DAL.App.DTO;
using externalDTO = BLL.App.DTO;

namespace BLL.App.Mappers
{
    public class GameMapper : IBaseBLLMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.Game))
            {
                return MapFromInternal((internalDTO.Game) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.Game))
            {
                return MapFromExternal((externalDTO.Game) inObject) as TOutObject;
            }

            throw new InvalidCastException("No conversion!");
        }

        public static externalDTO.Game MapFromInternal(internalDTO.Game game)
        {
            if (game == null) return null;
            
            var res = new externalDTO.Game
            {
                Id = game.Id,
                GameTitle = game.GameTitle,
                Price = game.Price,
                Pegi = game.Pegi,
                GenreId = game.GenreId,
                Genre = GenreMapper.MapFromInternal(game.Genre),
                PlatformId = game.PlatformId,
                Platform = PlatformMapper.MapFromInternal(game.Platform),
                PublisherId = game.PublisherId,
                Publisher = PublisherMapper.MapFromInternal(game.Publisher),
            };
            
            return res;

        }

        public static internalDTO.Game MapFromExternal(externalDTO.Game game)
        {
            if (game == null) return null;
            
            var res = new internalDTO.Game
            {
                Id = game.Id,
                GameTitle = game.GameTitle,
                Price = game.Price,
                Pegi = game.Pegi,
                GenreId = game.GenreId,
                Genre = GenreMapper.MapFromExternal(game.Genre),
                PlatformId = game.PlatformId,
                Platform = PlatformMapper.MapFromExternal(game.Platform),
                PublisherId = game.PublisherId,
                Publisher = PublisherMapper.MapFromExternal(game.Publisher),
            };
            
            return res;
        }
    }
}