using System;
using System.Runtime.InteropServices;
using Contracts.BLL.Base.Helpers;
using internalDTO = DAL.App.DTO;
using externalDTO = BLL.App.DTO;

namespace BLL.App.Mappers
{
    public class BillingMapper : IBaseBLLMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(BLL.App.DTO.Billing))
            {
                return MapFromInternal((DAL.App.DTO.Billing) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(DAL.App.DTO.Billing))
            {
                return MapFromExternal((BLL.App.DTO.Billing) inObject) as TOutObject;
            }

            throw new InvalidCastException("No conversion!");
        }

        public static externalDTO.Billing MapFromInternal(internalDTO.Billing billing, [Optional] externalDTO.Basket basket)
        {
            if (billing == null) return null;
            
            var res = new BLL.App.DTO.Billing
            {
                Id = billing.Id,
                SumWithoutTax = billing.SumWithoutTax,
                Tax = billing.Tax,
                SumTotal = billing.SumTotal,
                Created = billing.Created,
                BasketId = billing.BasketId,
                Basket = null,
                CreditCardId = billing.CreditCardId,
                CreditCard = CreditCardMapper.MapFromInternal(billing.CreditCard),
                BillingStatusId = billing.BillingStatusId,
                BillingStatus = BillingStatusMapper.MapFromInternal(billing.BillingStatus)
            };

            res.Basket = basket ?? BasketMapper.MapFromInternal(billing.Basket, res);
            
            return res;
        }

        public static internalDTO.Billing MapFromExternal(externalDTO.Billing billing, [Optional] internalDTO.Basket basket)
        {
            if (billing == null) return null;
            
            var res = new DAL.App.DTO.Billing
            {
                Id = billing.Id,
                SumWithoutTax = billing.SumWithoutTax,
                Tax = billing.Tax,
                SumTotal = billing.SumTotal,
                Created = billing.Created,
                BasketId = billing.BasketId,
                Basket = null,
                CreditCardId = billing.CreditCardId,
                CreditCard = CreditCardMapper.MapFromExternal(billing.CreditCard),
                BillingStatusId = billing.BillingStatusId,
                BillingStatus = BillingStatusMapper.MapFromExternal(billing.BillingStatus)
            };

            res.Basket = basket ?? BasketMapper.MapFromExternal(billing.Basket, res);
            
            return res;
        }
    }
}