using System;
using Contracts.BLL.Base.Helpers;
using internalDTO = DAL.App.DTO;
using externalDTO = BLL.App.DTO;

namespace BLL.App.Mappers
{
    public class PublisherMapper : IBaseBLLMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.Publisher))
            {
                return MapFromInternal((internalDTO.Publisher) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.Publisher))
            {
                return MapFromExternal((externalDTO.Publisher) inObject) as TOutObject;
            }

            throw new InvalidCastException("No conversion!");
        }

        public static externalDTO.Publisher MapFromInternal(internalDTO.Publisher publisher)
        {
            if (publisher == null) return null;
            
            var res = new externalDTO.Publisher
            {
                Id = publisher.Id,
                PublisherName = publisher.PublisherName,
            };

            return res;
        }

        public static internalDTO.Publisher MapFromExternal(externalDTO.Publisher publisher)
        {
            if (publisher == null) return null;
            
            var res = new internalDTO.Publisher
            {
                Id = publisher.Id,
                PublisherName = publisher.PublisherName,
            };

            return res;
        }
    }
}