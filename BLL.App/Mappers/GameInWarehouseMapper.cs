using System;
using Contracts.BLL.Base.Helpers;
using internalDTO = DAL.App.DTO;
using externalDTO = BLL.App.DTO;

namespace BLL.App.Mappers
{
    public class GameInWarehouseMapper : IBaseBLLMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.GameInWarehouse))
            {
                return MapFromInternal((internalDTO.GameInWarehouse) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.GameInWarehouse))
            {
                return MapFromExternal((externalDTO.GameInWarehouse) inObject) as TOutObject;
            }

            throw new InvalidCastException("No conversion!");
        }

        public static externalDTO.GameInWarehouse MapFromInternal(internalDTO.GameInWarehouse gameInWarehouse)
        {
            if (gameInWarehouse == null) return null;
            
            var res = new externalDTO.GameInWarehouse
            {
                Id = gameInWarehouse.Id,
                Amount = gameInWarehouse.Amount,
                Added = gameInWarehouse.Added,
                GameId = gameInWarehouse.GameId,
                Game = GameMapper.MapFromInternal(gameInWarehouse.Game),
                WarehouseId = gameInWarehouse.WarehouseId,
                Warehouse = WarehouseMapper.MapFromInternal(gameInWarehouse.Warehouse)
            };

            return res;

        }

        public static internalDTO.GameInWarehouse MapFromExternal(externalDTO.GameInWarehouse gameInWarehouse)
        {
            if (gameInWarehouse == null) return null;
            
            var res = new internalDTO.GameInWarehouse
            {
                Id = gameInWarehouse.Id,
                Amount = gameInWarehouse.Amount,
                Added = gameInWarehouse.Added,
                GameId = gameInWarehouse.GameId,
                Game = GameMapper.MapFromExternal(gameInWarehouse.Game),
                WarehouseId = gameInWarehouse.WarehouseId,
                Warehouse = WarehouseMapper.MapFromExternal(gameInWarehouse.Warehouse)
            };

            return res;
        }
    }
}