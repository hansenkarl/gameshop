using System;
using Contracts.BLL.Base.Helpers;
using internalDTO = DAL.App.DTO;
using externalDTO = BLL.App.DTO;

namespace BLL.App.Mappers
{
    public class BasketStatusMapper : IBaseBLLMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.BasketStatus))
            {
                return MapFromInternal((internalDTO.BasketStatus) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.BasketStatus))
            {
                return MapFromExternal((externalDTO.BasketStatus) inObject) as TOutObject;
            }

            throw new InvalidCastException("No conversion!");
        }

        public static externalDTO.BasketStatus MapFromInternal(internalDTO.BasketStatus basketStatus)
        {
            if (basketStatus == null) return null;
            
            var res = new externalDTO.BasketStatus
            {
                Id = basketStatus.Id,
                StatusName = basketStatus.StatusName,
            };
            
            return res;
        }

        public static internalDTO.BasketStatus MapFromExternal(externalDTO.BasketStatus basketStatus)
        {
            if (basketStatus == null) return null;
            
            var res = new internalDTO.BasketStatus
            {
                Id = basketStatus.Id,
                StatusName = basketStatus.StatusName,
            };
            
            return res;
        }
    }
}