using System;
using Contracts.BLL.Base.Helpers;
using internalDTO = DAL.App.DTO;
using externalDTO = BLL.App.DTO;

namespace BLL.App.Mappers
{
    public class BillingStatusMapper : IBaseBLLMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.BillingStatus))
            {
                return MapFromInternal((internalDTO.BillingStatus) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.BillingStatus))
            {
                return MapFromExternal((externalDTO.BillingStatus) inObject) as TOutObject;
            }

            throw new InvalidCastException("No conversion!");
        }

        public static externalDTO.BillingStatus MapFromInternal(internalDTO.BillingStatus billingStatus)
        {
            if (billingStatus == null) return null;
            
            var res = new externalDTO.BillingStatus
            {
                Id = billingStatus.Id,
                StatusName = billingStatus.StatusName,
            };
            
            return res;

        }

        public static internalDTO.BillingStatus MapFromExternal(externalDTO.BillingStatus billingStatus)
        {
            if (billingStatus == null) return null;
            
            var res = new internalDTO.BillingStatus
            {
                Id = billingStatus.Id,
                StatusName = billingStatus.StatusName,
            };
            
            return res;
        }
    }
}