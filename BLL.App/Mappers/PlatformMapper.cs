using System;
using Contracts.BLL.Base.Helpers;
using internalDTO = DAL.App.DTO;
using externalDTO = BLL.App.DTO;

namespace BLL.App.Mappers
{
    public class PlatformMapper : IBaseBLLMapper
    {
        public TOutObject Map<TOutObject>(object inObject) where TOutObject : class
        {
            if (typeof(TOutObject) == typeof(externalDTO.Platform))
            {
                return MapFromInternal((internalDTO.Platform) inObject) as TOutObject;
            }

            if (typeof(TOutObject) == typeof(internalDTO.Platform))
            {
                return MapFromExternal((externalDTO.Platform) inObject) as TOutObject;
            }

            throw new InvalidCastException("No conversion!");
        }

        public static externalDTO.Platform MapFromInternal(internalDTO.Platform platform)
        {
            if (platform == null) return null;
            
            var res = new externalDTO.Platform
            {
                Id = platform.Id,
                PlatformName = platform.PlatformName,
            };

            return res;
        }

        public static internalDTO.Platform MapFromExternal(externalDTO.Platform platform)
        {
            if (platform == null) return null;
            
            var res = new internalDTO.Platform
            {
                Id = platform.Id,
                PlatformName = platform.PlatformName,
            };

            return res;
        }
    }
}