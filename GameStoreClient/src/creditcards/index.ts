import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction} from "aurelia-router";
import {ICreditCard} from "../interfaces/ICreditCard";
import {CreditCardsService} from "../services/credit-cards-service";

export var log = LogManager.getLogger('CreditCards.Index');

@autoinject
export class Index {

  private creditCards: ICreditCard[] = [];

  constructor(
    private creditCardsService: CreditCardsService
  ) {
    log.debug('constructor');
  }

  generateDateTimeString() {
    this.creditCards.forEach(creditCard => {
      console.log(creditCard.validThru);
      let date = new Date(creditCard.validThru);
      creditCard.timeString = date.getFullYear().toString() + "/" + this.fixMonth(date.getMonth()).toString();
    })
  }

  fixMonth(month: number) {
    month += 1;
    if (month <= 9) return "0" + month;
    return month;
  }


  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
    this.creditCardsService.fetchAll().then(
      jsonData => {
        log.debug('jsonData', jsonData);
        this.creditCards = jsonData;
        this.generateDateTimeString();
      }
    )
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
