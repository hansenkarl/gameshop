import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {CreditCardsService} from "../services/credit-cards-service";
import {ICreditCard} from "../interfaces/ICreditCard";

export var log = LogManager.getLogger('CreditCards.Edit');

@autoinject
export class Edit {

  private creditCard = <ICreditCard>{};
  private formMonth: number;
  private formYear: number;
  
  constructor(
    private router: Router,
    private creditCardsService: CreditCardsService
  ) {
    log.debug('constructor');
  }

  // View methods
  submit(): void {
    log.debug('creditCard', this.creditCard);
    if (this.creditCard == undefined || this.creditCard.creditCardNr == undefined ||
      this.formMonth == undefined || this.formYear == undefined) {
      alert('Please fill in all the fields!');
      return;
    }
    if (this.creditCard.creditCardNr.length != 16) {
      alert('Credit card number must be a 16 characters long!');
      return;
    }

    this.creditCard.validThru = new Date(this.formYear, this.formMonth);
    this.creditCardsService.put(this.creditCard!).then(
      response => {
        if (response.status == 204) {
          this.router.navigateToRoute("creditcardsIndex");
        } else {
          log.error('Error in response!', response);
        }
      }
    );
  }

  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
    this.creditCardsService.fetch(params.id).then(creditCard => {
      log.debug('creditCard', creditCard);
      this.creditCard = creditCard;
      this.creditCard.validThru = new Date(this.creditCard.validThru);
      this.formYear = this.creditCard.validThru.getFullYear();
      this.formMonth = this.creditCard.validThru.getMonth() + 1;
    })
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
