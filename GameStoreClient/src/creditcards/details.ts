import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {CreditCardsService} from "../services/credit-cards-service";
import {ICreditCard} from "../interfaces/ICreditCard";

export var log = LogManager.getLogger('CreditCards.Details');

@autoinject
export class Details {

  private creditCard: ICreditCard;

  constructor(
    private router: Router,
    private creditCardsService: CreditCardsService
  ) {
    log.debug('constructor');
  }

  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
    this.creditCardsService.fetch(params.id).then(creditCard => {
      log.debug('creditCard', creditCard);
      this.creditCard = creditCard;
    })
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
