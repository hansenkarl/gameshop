import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {CreditCardsService} from "../services/credit-cards-service";
import {ICreditCard} from "../interfaces/ICreditCard";

export var log = LogManager.getLogger('CreditCards.Delete');

@autoinject
export class Delete {

  private creditCard: ICreditCard;

  constructor(
    private router: Router,
    private creditCardsService: CreditCardsService
  ) {
    log.debug('constructor');
  }

  generateDateTimeString(){
      console.log(this.creditCard.validThru);
      let date = new Date(this.creditCard.validThru);
      this.creditCard.timeString = date.getFullYear().toString() + "/" + this.fixMonth(date.getMonth()).toString();
    
  }
  fixMonth(month: number){
    month += 1;
    if (month <= 9) return "0" + month;
    return month;
  }
  
  submit(): void {
    this.creditCardsService.delete(this.creditCard.id).then(response => {
      if (response.status == 204) {
        this.router.navigateToRoute("creditcardsIndex");
      } else {
        log.debug('response', response);
      }
    })
  }

  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
    this.creditCardsService.fetch(params.id).then(
      creditCard => {
        log.debug('creditCard', creditCard);
        this.creditCard = creditCard;
        this.generateDateTimeString();
      }
    );
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
