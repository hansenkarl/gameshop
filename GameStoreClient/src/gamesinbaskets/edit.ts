import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {IGameInBasket} from "../interfaces/IGameInBasket";
import {GamesInBasketsService} from "../services/games-in-baskets-service";
import {GamesService} from "../services/games-service";
import {BasketsService} from "../services/baskets-service";
import {IGame} from "../interfaces/IGame";
import {IBasket} from "../interfaces/IBasket";

export var log = LogManager.getLogger('GamesInBaskets.Edit');

@autoinject
export class Edit {

  private gamesInBaskets: IGameInBasket;
  private game: IGame;
  private baskets: IBasket[] = [];

  constructor(
    private router: Router,
    private gamesInBasketsServices: GamesInBasketsService,
    private gamesService: GamesService,
    private basketsService: BasketsService
  ) {
    log.debug('constructor');
  }

  // View methods
  submit(): void {
    let gameInBasket: any = {
      id: this.gamesInBaskets.id,
      amount: this.gamesInBaskets.amount,
      gameId: this.gamesInBaskets.gameId,
      basketId: this.gamesInBaskets.basketId
    };
    log.debug('gamesInBaskets', this.gamesInBaskets);
    this.gamesInBasketsServices.put(gameInBasket!).then(
      response => {
        if (response.status == 204) {
          this.router.navigateToRoute("gamesinbasketsIndex");
        } else {
          log.error('Error in response!', response);
        }
      }
    );
  }

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
    this.gamesInBasketsServices.fetch(params.id).then(gamesInBaskets => {
      log.debug('gamesInBaskets', gamesInBaskets);
      this.gamesInBaskets = gamesInBaskets;
    }).then(() => {
      log.debug('activate');
      this.gamesService.fetch(this.gamesInBaskets.gameId).then(game => {
        log.debug('game', game);
        this.gamesInBaskets.game = game.gameTitle;
        this.gamesInBaskets.price = game.price;
      });
      this.basketsService.fetchAll().then(
        jsonData => {
          log.debug('jsonData', jsonData);
          this.baskets = jsonData;
        }
      );
    });
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
