import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {IGameInBasket} from "../interfaces/IGameInBasket";
import {GamesInBasketsService} from "../services/games-in-baskets-service";
import {GamesService} from "../services/games-service";
import {BasketsService} from "../services/baskets-service";

export var log = LogManager.getLogger('GamesInBaskets.Details');

@autoinject
export class Details {

  private gamesInBaskets: IGameInBasket;

  constructor(
    private router: Router,
    private gamesInBasketsServices: GamesInBasketsService,
    private gamesService: GamesService,
    private basketsService: BasketsService
  ) {
    log.debug('constructor');
  }

  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
    this.gamesInBasketsServices.fetch(params.id).then(gamesInBaskets => {
      log.debug('gamesInBaskets', gamesInBaskets);
      this.gamesInBaskets = gamesInBaskets;
    }).then(() => {
      log.debug('activate');
      this.gamesService.fetch(this.gamesInBaskets.gameId).then(game => {
        log.debug('game', game);
        this.gamesInBaskets.game = game.gameTitle;
      });
      this.basketsService.fetch(this.gamesInBaskets.basketId).then(basket => {
          log.debug('baskets', basket);
          this.gamesInBaskets.basket = basket.created;
        }
      );
    });
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
