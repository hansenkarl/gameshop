import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {IGameInBasket} from "../interfaces/IGameInBasket";
import {GamesInBasketsService} from "../services/games-in-baskets-service";
import {GamesService} from "../services/games-service";
import {BasketsService} from "../services/baskets-service";
import {IBasket} from "../interfaces/IBasket";

export var log = LogManager.getLogger('GamesInBaskets.Delete');

@autoinject
export class Delete {

  private gamesInBaskets: IGameInBasket;
  private basket: IBasket;

  constructor(
    private router: Router,
    private gamesInBasketsServices: GamesInBasketsService,
    private gamesService: GamesService,
    private basketsService: BasketsService
  ) {
    log.debug('constructor');
  }

  generateDateTimeString() {
    let date = new Date(this.gamesInBaskets.basket);
    this.gamesInBaskets.timeString = date.getFullYear().toString() + "/" + this.fixMonth(date.getMonth()).toString() + 
      "/" + this.AddLeadingZeroes(date.getDate()).toString() + " " + this.AddLeadingZeroes(date.getHours()).toString()
      + ":" + this.AddLeadingZeroes(date.getMinutes()).toString() + ":" +
      this.AddTrailingZeroesToSeconds(date.getSeconds()).toString();

  }

  fixMonth(month: number) {
    month += 1;
    if (month <= 9) return "0" + month;
    return month;
  }

  AddLeadingZeroes(date: number) {
    if (date <= 9) return "0" + date;
    return date;
  }

  AddTrailingZeroesToSeconds(seconds: number) {
    if (seconds <= 9) return "0" + seconds;
    return seconds;
  }


  submit(): void {
    this.gamesInBasketsServices.delete(this.gamesInBaskets.id).then(response => {
      if (response.status == 204) {
        this.router.navigateToRoute("basketsIndex");
      } else {
        log.debug('response', response);
      }
    })
  }

  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
    this.gamesInBasketsServices.fetch(params.id).then(
      gamesInBaskets => {
        log.debug('gamesInBaskets', gamesInBaskets);
        this.gamesInBaskets = gamesInBaskets;
      }
    ).then(() => {
      log.debug('activate');
      this.gamesService.fetch(this.gamesInBaskets.gameId).then(game => {
        log.debug('game', game);
        this.gamesInBaskets.game = game.gameTitle;
      }).then(() => {
        this.basketsService.fetch(this.gamesInBaskets.basketId).then(basket => {
            log.debug('baskets', basket);
            console.log("-------------------- basket: " + basket);
            this.gamesInBaskets.basket = basket.created;
            this.generateDateTimeString();
          }
        );
      })
    });
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
