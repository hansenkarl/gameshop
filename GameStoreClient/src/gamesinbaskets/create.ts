import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {IGameInBasket} from "../interfaces/IGameInBasket";
import {GamesInBasketsService} from "../services/games-in-baskets-service";
import {IGame} from "../interfaces/IGame";
import {IBasket} from "../interfaces/IBasket";
import {GamesService} from "../services/games-service";
import {BasketsService} from "../services/baskets-service";

export var log = LogManager.getLogger('GamesInBaskets.Create');

@autoinject
export class Create {

  private gamesInBaskets: IGameInBasket;
  private game: IGame;
  private basket: IBasket;
  private games: IGame[] = [];
  private baskets: IBasket[] = [];

  constructor(
    private router: Router,
    private gamesInBasketsServices: GamesInBasketsService,
    private gamesService: GamesService,
    private basketsService: BasketsService
  ) {
    log.debug('constructor');
  }

  // View methods
  submit(): void {
    log.debug('gamesInBaskets', this.gamesInBaskets);
    this.gamesInBaskets.gameId = this.game.id;
    this.gamesInBaskets.basketId = this.basket.id;
    this.gamesInBasketsServices.post(this.gamesInBaskets).then(
      response => {
        if (response.status == 201) {
          this.router.navigateToRoute("gamesinbasketsIndex");
        } else {
          log.error('Error in response!', response);
        }
      }
    );
  }

  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
    this.gamesService.fetchAll().then(
      jsonData => {
        log.debug('jsonData', jsonData);
        this.games = jsonData;
      }
    );
    this.basketsService.fetchAll().then(
      jsonData => {
        log.debug('jsonData', jsonData);
        this.baskets = jsonData;
      }
    );
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
