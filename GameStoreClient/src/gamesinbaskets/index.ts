import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction} from "aurelia-router";
import {IGameInBasket} from "../interfaces/IGameInBasket";
import {GamesInBasketsService} from "../services/games-in-baskets-service";
import {IGame} from "../interfaces/IGame";
import {IBasket} from "../interfaces/IBasket";
import {BasketsService} from "../services/baskets-service";
import {GamesService} from "../services/games-service";

export var log = LogManager.getLogger('GamesInBaskets.Index');

@autoinject
export class Index {

  private gamesInBaskets: IGameInBasket[] = [];
  private games: IGame[] = [];
  private baskets: IBasket[] = [];
  
  constructor(
    private gamesInBasketsService: GamesInBasketsService,
    private gamesService: GamesService,
    private basketsService: BasketsService
  ) {
      log.debug('constructor');
  }

  generateDateTimeString() {
    this.gamesInBaskets.forEach(gameInBasket => {
      console.log(gameInBasket.basket);
      let date = new Date(gameInBasket.basket);
      gameInBasket.timeString = date.getFullYear().toString() + "/" + this.fixMonth(date.getMonth()).toString() +
        "/" + this.AddLeadingZeroes(date.getDate()).toString() + " " + this.AddLeadingZeroes(date.getHours()).toString()
        + ":" + this.AddLeadingZeroes(date.getMinutes()).toString() + ":"
        + this.AddTrailingZeroesToSeconds(date.getSeconds()).toString();
    })
  }

  fixMonth(month: number) {
    month += 1;
    if (month <= 9) return "0" + month;
    return month;
  }

  AddLeadingZeroes(date: number) {
    if (date <= 9) return "0" + date;
    return date;
  }

  AddTrailingZeroesToSeconds(seconds: number) {
    if (seconds <= 9) return "0" + seconds;
    return seconds;
  }


  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
    this.gamesInBasketsService.fetchAll().then(
      jsonData => {
        log.debug('jsonData', jsonData);
        this.gamesInBaskets = jsonData;
      }
    ).then(() => {
      return this.gamesService.fetchAll()
    }).then(
      jsonData => {
        log.debug('jsonData', jsonData);
        this.games = jsonData;
      }
    ).then(() => {
      return this.basketsService.fetchAll()
    }).then(
      jsonData => {
        log.debug('jsonData', jsonData);
        this.baskets = jsonData;
      }
    ).then(() => {
      this.gamesInBaskets.forEach(gameInBasket => {
        gameInBasket.game = this.games.find(game => game.id == gameInBasket.gameId).gameTitle;
        gameInBasket.basket = this.baskets.find(basket => basket.id == gameInBasket.basketId).created;
      })
    }).then(() => {
      this.generateDateTimeString();
    })
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
