import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {IPlatform} from "../interfaces/IPlatform";
import {PlatformsService} from "../services/platforms-service";

export var log = LogManager.getLogger('Platforms.Edit');

@autoinject
export class Edit {

  private platform: IPlatform;

  constructor(
    private router: Router,
    private platformsService: PlatformsService
  ) {
    log.debug('constructor');
  }

  // View methods
  submit(): void {
    log.debug('platform', this.platform);

    if (this.platform == undefined || this.platform.platformName == undefined) {
      alert("Please fill in the platform name field!");
      return;
    }

    if (this.platform.platformName.length < 2) {
      alert("Platform's name must be at least 2 characters.");
      return;
    }

    if (this.platform.platformName.length > 32) {
      alert("Platform's name must not be longer than 32 characters.");
      return;
    }
    
    this.platformsService.put(this.platform!).then(
      response => {
        if (response.status == 204) {
          this.router.navigateToRoute("platformsIndex");
        } else {
          log.error('Error in response!', response);
        }
      }
    );
  }
  
  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
    this.platformsService.fetch(params.id).then(platform => {
      log.debug('platform', platform);
      this.platform = platform;
    })
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
