import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {IPlatform} from "../interfaces/IPlatform";
import {PlatformsService} from "../services/platforms-service";

export var log = LogManager.getLogger('Platforms.Delete');

@autoinject
export class Delete {

  private platform: IPlatform;

  constructor(
    private router: Router,
    private platformsService: PlatformsService
  ) {
    log.debug('constructor');
  }

  submit(): void {
    this.platformsService.delete(this.platform.id).then(response => {
      if (response.status == 204) {
        this.router.navigateToRoute("platformsIndex");
      } else {
        log.debug('response', response);
      }
    })
  }

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
    this.platformsService.fetch(params.id).then(
      platform => {
        log.debug('platform', platform);
        this.platform = platform;
      }
    );
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
