import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction} from "aurelia-router";
import {IGameInWarehouse} from "../interfaces/IGameInWarehouse";
import {GamesInWarehousesService} from "../services/games-in-warehouses-service";
import {IGame} from "../interfaces/IGame";
import {IWarehouse} from "../interfaces/IWarehouse";
import {GamesService} from "../services/games-service";
import {WarehousesService} from "../services/warehouses-service";
import {IAppRole} from "../interfaces/IAppRole";
import {AppRolesService} from "../services/app-roles-service";

export var log = LogManager.getLogger('GamesInWarehouses.Index');

@autoinject
export class Index {

  private gamesInWarehouses: IGameInWarehouse[] = [];
  private games: IGame[] = [];
  private warehouses: IWarehouse[] = [];
  private isAdmin: IAppRole[] = [];

  constructor(
    private gamesInWarehousesService: GamesInWarehousesService,
    private gamesService: GamesService,
    private warehousesService: WarehousesService,
    private appRolesService: AppRolesService
  ) {
    log.debug('constructor');
  }

  generateDateTimeString() {
    this.gamesInWarehouses.forEach(gameInWarehouse => {
      console.log(gameInWarehouse.added);
      let date = new Date(gameInWarehouse.added);
      gameInWarehouse.timeString = date.getFullYear().toString() + "/" + this.fixMonth(date.getMonth()).toString() +
        "/" + this.AddLeadingZeroes(date.getDate()).toString() + " " + this.AddLeadingZeroes(date.getHours()).toString()
        + ":" + this.AddLeadingZeroes(date.getMinutes()).toString() + ":"
        + this.AddTrailingZeroesToSeconds(date.getSeconds()).toString();
    })
  }

  fixMonth(month: number) {
    month += 1;
    if (month <= 9) return "0" + month;
    return month;
  }

  AddLeadingZeroes(date: number) {
    if (date <= 9) return "0" + date;
    return date;
  }

  AddTrailingZeroesToSeconds(seconds: number) {
    if (seconds <= 9) return "0" + seconds;
    return seconds;
  }

  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
    this.appRolesService.fetchAll().then(jsonData => {
      log.debug('jsonData', jsonData);
      this.isAdmin = jsonData;
    });
    this.gamesInWarehousesService.fetchAll().then(
      jsonData => {
        log.debug('jsonData', jsonData);
        this.gamesInWarehouses = jsonData;
      }
    ).then(() => {
      return this.gamesService.fetchAll()
    }).then(
      jsonData => {
        log.debug('jsonData', jsonData);
        this.games = jsonData;
      }
    ).then(() => {
      return this.warehousesService.fetchAll()
    }).then(
      jsonData => {
        log.debug('jsonData', jsonData);
        this.warehouses = jsonData;
      }
    ).then(() => {
      this.gamesInWarehouses.forEach(gameInWarehouse => {
        gameInWarehouse.game = this.games.find(game => game.id == gameInWarehouse.gameId).gameTitle;
        gameInWarehouse.warehouse = this.warehouses.find(
          warehouse => warehouse.id == gameInWarehouse.warehouseId).warehouseName;
      })
    }).then(() => {
      this.generateDateTimeString();
    })
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
