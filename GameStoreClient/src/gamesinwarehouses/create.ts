import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {IGameInWarehouse} from "../interfaces/IGameInWarehouse";
import {GamesInWarehousesService} from "../services/games-in-warehouses-service";
import {GamesService} from "../services/games-service";
import {WarehousesService} from "../services/warehouses-service";
import {IWarehouse} from "../interfaces/IWarehouse";
import {IGame} from "../interfaces/IGame";

export var log = LogManager.getLogger('GamesInWarehouses.Create');

@autoinject
export class Create {

  private gameInWarehouse: IGameInWarehouse;
  private game: IGame;
  private warehouse: IWarehouse;
  private games: IGame[] = [];
  private warehouses: IWarehouse[] = [];

  constructor(
    private router: Router,
    private gamesInWarehousesService: GamesInWarehousesService,
    private gamesService: GamesService,
    private warehousesService: WarehousesService,
  ) {
    log.debug('constructor');
  }

  // View methods
  submit(): void {
    log.debug('gameInWarehouse', this.gameInWarehouse);
    if (this.gameInWarehouse == undefined || this.gameInWarehouse.gameId == undefined || 
    this.gameInWarehouse.amount == undefined || this.gameInWarehouse.warehouseId == undefined) {
      alert("Please fill in all the fields!");
      return;
    }
    
    this.gameInWarehouse.gameId = this.game.id;
    this.gameInWarehouse.warehouseId = this.warehouse.id;
    this.gamesInWarehousesService.post(this.gameInWarehouse).then(
      response => {
        if (response.status == 201) {
          this.router.navigateToRoute("gamesinwarehousesIndex");
        } else {
          log.error('Error in response!', response);
        }
      }
    );
  }
  
  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
    this.gamesService.fetchAll().then(
      jsonData => {
        log.debug('jsonData', jsonData);
        this.games = jsonData;
      }
    );
    this.warehousesService.fetchAll().then(
      jsonData => {
        log.debug('jsonData', jsonData);
        this.warehouses = jsonData;
      }
    )
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
