import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {IGameInWarehouse} from "../interfaces/IGameInWarehouse";
import {GamesInWarehousesService} from "../services/games-in-warehouses-service";
import {IGame} from "../interfaces/IGame";
import {IWarehouse} from "../interfaces/IWarehouse";
import {GamesService} from "../services/games-service";
import {WarehousesService} from "../services/warehouses-service";

export var log = LogManager.getLogger('GamesInWarehouses.Edit');

@autoinject
export class Edit {

  private gamesInWarehouses: IGameInWarehouse;
  private game: IGame;
  private warehouse: IWarehouse;

  constructor(
    private router: Router,
    private gamesInWarehousesService: GamesInWarehousesService,
    private gamesService: GamesService,
    private warehousesService: WarehousesService
  ) {
    log.debug('constructor');
  }

  generateDateTimeString() {
    console.log(this.gamesInWarehouses.added);
    let date = new Date(this.gamesInWarehouses.added);
    this.gamesInWarehouses.timeString = date.getFullYear().toString() + "/" + this.fixMonth(date.getMonth()).toString() +
      "/" + this.AddLeadingZeroes(date.getDate()).toString() + " " + this.AddLeadingZeroes(date.getHours()).toString()
      + ":" + this.AddLeadingZeroes(date.getMinutes()).toString() + ":"
      + this.AddTrailingZeroesToSeconds(date.getSeconds()).toString();

  }

  fixMonth(month: number) {
    month += 1;
    if (month <= 9) return "0" + month;
    return month;
  }

  AddLeadingZeroes(date: number) {
    if (date <= 9) return "0" + date;
    return date;
  }

  AddTrailingZeroesToSeconds(seconds: number) {
    if (seconds <= 9) return "0" + seconds;
    return seconds;
  }

  // View methods
  submit(): void {
    if (this.gamesInWarehouses.amount == null) {
      alert("Please insert a number!");
      return;
    }
    
    if (this.gamesInWarehouses.amount < 0) {
      alert("Please insert a positive number!");
      return;
    }

    if (this.gamesInWarehouses.amount > 2147483647) {
      alert("Please insert a number smaller than int maximum value!");
      return;
    }
    
    let gameInWarehouse: any = {
      id: this.gamesInWarehouses.id,
      amount: this.gamesInWarehouses.amount,
      added: this.gamesInWarehouses.added,
      warehouseId: this.gamesInWarehouses.warehouseId,
      gameId: this.gamesInWarehouses.gameId
    };
    log.debug('gamesInWarehouses', this.gamesInWarehouses);
    this.gamesInWarehousesService.put(gameInWarehouse!).then(
      response => {
        if (response.status == 204) {
          this.router.navigateToRoute("gamesinwarehousesIndex");
        } else {
          log.error('Error in response!', response);
        }
      }
    );
  }

  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
    this.gamesInWarehousesService.fetch(params.id).then(gameInWarehouse => {
      log.debug('gamesInWarehouses', gameInWarehouse);
      this.gamesInWarehouses = gameInWarehouse;
      this.generateDateTimeString();
    }).then(() => {
      log.debug('activate');
      this.gamesService.fetch(this.gamesInWarehouses.gameId).then(game => {
        log.debug('game', game);
        this.gamesInWarehouses.game = game.gameTitle;
      });
      this.warehousesService.fetch(this.gamesInWarehouses.warehouseId).then(
        warehouse => {
          log.debug('warehouse', warehouse);
          this.gamesInWarehouses.warehouse = warehouse.warehouseName;
        }
      )
    });
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
