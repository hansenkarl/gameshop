import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {IGameInWarehouse} from "../interfaces/IGameInWarehouse";
import {GamesInWarehousesService} from "../services/games-in-warehouses-service";
import {GamesService} from "../services/games-service";
import {WarehousesService} from "../services/warehouses-service";

export var log = LogManager.getLogger('GamesInWarehouses.Details');

@autoinject
export class Details {

  private gameInWarehouse: IGameInWarehouse;

  constructor(
    private router: Router,
    private gamesInWarehousesService: GamesInWarehousesService,
    private gamesService: GamesService,
    private warehousesService: WarehousesService
  ) {
    log.debug('constructor');
  }

  generateDateTimeString() {
    console.log(this.gameInWarehouse.added);
    let date = new Date(this.gameInWarehouse.added);
    this.gameInWarehouse.timeString = date.getFullYear().toString() + "/" + this.fixMonth(date.getMonth()).toString() +
      "/" + this.AddLeadingZeroes(date.getDate()).toString() + " " + this.AddLeadingZeroes(date.getHours()).toString()
      + ":" + this.AddLeadingZeroes(date.getMinutes()).toString() + ":"
      + this.AddTrailingZeroesToSeconds(date.getSeconds()).toString();

  }

  fixMonth(month: number) {
    month += 1;
    if (month <= 9) return "0" + month;
    return month;
  }

  AddLeadingZeroes(date: number) {
    if (date <= 9) return "0" + date;
    return date;
  }

  AddTrailingZeroesToSeconds(seconds: number) {
    if (seconds <= 9) return "0" + seconds;
    return seconds;
  }

  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
    this.gamesInWarehousesService.fetch(params.id).then(gameInWarehouse => {
      log.debug('gameInWarehouse', gameInWarehouse);
      this.gameInWarehouse = gameInWarehouse;
      this.generateDateTimeString();
    }).then(() => {
      log.debug('activate');
      this.gamesService.fetch(this.gameInWarehouse.gameId).then(game => {
        log.debug('game', game);
        this.gameInWarehouse.game = game.gameTitle;
      });
      this.warehousesService.fetch(this.gameInWarehouse.warehouseId).then(warehouse => {
        log.debug('warehouse', warehouse);
        this.gameInWarehouse.warehouse = warehouse.warehouseName;
      })
    })
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
