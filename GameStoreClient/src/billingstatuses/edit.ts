import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {IBillingStatus} from "../interfaces/IBillingStatus";
import {BillingStatusesService} from "../services/billing-statuses-service";

export var log = LogManager.getLogger('BillingStatuses.Edit');

@autoinject
export class Edit {

  private billingStatus: IBillingStatus;

  constructor(
    private router: Router,
    private billingStatusesService: BillingStatusesService
  ) {
    log.debug('constructor');
  }

  // View methods
  submit(): void {
    log.debug('billingStatus', this.billingStatus);
    if (this.billingStatus == undefined || this.billingStatus.statusName == undefined) {
      alert('Please fill in the status name field!');
      return;
    }
    if (this.billingStatus.statusName.length < 2 || this.billingStatus.statusName.length > 32) {
      alert('Status name must be at least 2 characters long and should not be longer than 32 characters.')
    }
    this.billingStatusesService.put(this.billingStatus!).then(
      response => {
        if (response.status == 204) {
          this.router.navigateToRoute("billingstatusesIndex");
        } else {
          log.error('Error in response!', response);
        }
      }
    );
  }
  
  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
    this.billingStatusesService.fetch(params.id).then(billingStatus => {
      log.debug('billingStatus', billingStatus);
      this.billingStatus = billingStatus;
    })
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
