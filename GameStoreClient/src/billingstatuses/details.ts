import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {IBillingStatus} from "../interfaces/IBillingStatus";
import {BillingStatusesService} from "../services/billing-statuses-service";

export var log = LogManager.getLogger('BillingStatuses.Details');

@autoinject
export class Details {

  private billingStatus: IBillingStatus;

  constructor(
    private router: Router,
    private billingStatusesService: BillingStatusesService
  ) {
    log.debug('constructor');
  }

  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
    this.billingStatusesService.fetch(params.id).then(billingStatus => {
      log.debug('billingStatus', billingStatus);
      this.billingStatus = billingStatus;
    })
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
