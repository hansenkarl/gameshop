import {IBaseEntity} from "./IBaseEntity";

export interface IWarehouse extends IBaseEntity{
  warehouseName: string;
}
