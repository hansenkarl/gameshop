import {IBaseEntity} from "./IBaseEntity";

export interface IReview extends IBaseEntity{
  rating: number;
  comment: string;
  added: Date;
  timeString: string;
  appUserId: number;
  appuser: string;
  gameId: number;
  platformId: number;
  game?: string;
  platform?: string;
}
