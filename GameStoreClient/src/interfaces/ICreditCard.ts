import {IBaseEntity} from "./IBaseEntity";

export interface ICreditCard extends IBaseEntity{
  creditCardNr: string;
  validThru: Date;
  timeString: string;
  appUserId: number;
}
