import {IBaseEntity} from "./IBaseEntity";

export interface IGameInBasket extends IBaseEntity{
  amount: number;
  gameId: number;
  basketId: number;
  game?: string;
  basket?: Date;
  timeString: string;
  price?: string;
}
