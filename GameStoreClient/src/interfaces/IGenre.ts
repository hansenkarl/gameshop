import {IBaseEntity} from "./IBaseEntity";

export interface IGenre extends IBaseEntity{
  genreName: string;
}
