import {IBaseEntity} from "./IBaseEntity";

export interface IGameInWarehouse extends IBaseEntity{
  amount: number;
  added: Date;
  timeString: string;
  warehouseId: number;
  gameId: number;
  game?: string;
  warehouse?: string;
}
