import {IBaseEntity} from "./IBaseEntity";

export interface IBasket extends IBaseEntity{
  created: Date;
  timeString: string;
  appUserId: number;
  billingId: number;
  basketStatusId: number;
  basketStatus: string;
}
