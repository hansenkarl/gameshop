import {IBaseEntity} from "./IBaseEntity";

export interface IAppRole  extends IBaseEntity{
  name: string;
}
