import {IBaseEntity} from "./IBaseEntity";

export interface IAddress extends IBaseEntity{
  country: string;
  county: string;
  city: string;
  street: string;
  houseNr: string;
  zipCode: string;
  appUserId: number;
}
