import {IBaseEntity} from "./IBaseEntity";

export interface IAppUser extends IBaseEntity{
  email: string;
  firstName: string;
  lastName: string;
}
