import {IBaseEntity} from "./IBaseEntity";

export interface IPublisher extends IBaseEntity{
  publisherName: string;
}
