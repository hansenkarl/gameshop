import {IBaseEntity} from "./IBaseEntity";

export interface IGame extends IBaseEntity{
  gameTitle: string;
  price: string;
  pegi: string;
  genreId: number;
  platformId: number;
  publisherId: number;
  genre?: string;
  platform?: string;
  publisher?: string;
  inBasket?: boolean;
  inWarehouse?: boolean;
}
