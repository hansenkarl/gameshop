import {IBaseEntity} from "./IBaseEntity";

export interface IBillingStatus extends IBaseEntity{
  statusName: string;
}
