import {IBaseEntity} from "./IBaseEntity";

export interface IPlatform extends IBaseEntity{
  platformName: string;
}
