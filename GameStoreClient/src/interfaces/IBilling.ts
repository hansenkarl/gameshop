import {IBaseEntity} from "./IBaseEntity";

export interface IBilling extends IBaseEntity{
  sumWithoutTax: number;	
  tax: number;	
  sumTotal: number;	
  created: Date;
  timeString: string;
  basketId: number;
  creditCardId: number;
  creditCard: string;
  billingStatusId: number;
  billingStatus: string;
}
