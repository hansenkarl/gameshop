import {IBaseEntity} from "./IBaseEntity";
import {IBasket} from "./IBasket";
import {IGameInBasket} from "./IGameInBasket";

export interface IBasketDetails extends IBaseEntity{
  basket: IBasket;
  games: IGameInBasket[];
}
