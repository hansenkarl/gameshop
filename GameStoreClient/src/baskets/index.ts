import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction} from "aurelia-router";
import {IBasket} from "../interfaces/IBasket";
import {BasketsService} from "../services/baskets-service";
import {BasketStatusesService} from "../services/basket-statuses-service";
import {IBasketStatus} from "../interfaces/IBasketStatus";

export var log = LogManager.getLogger('Baskets.Index');

@autoinject
export class Index {

  private baskets: IBasket[] = [];
  private basketStatuses: IBasketStatus[] = [];

  constructor(
    private basketsService: BasketsService,
    private basketStatusesService: BasketStatusesService
  ) {
    log.debug('constructor');
  }
  
  generateDateTimeString() {
    this.baskets.forEach(basket => {
      let date = new Date(basket.created);
      basket.timeString = date.getFullYear().toString() + "/" + this.fixMonth(date.getMonth()).toString() + "/" +
        this.AddLeadingZeroes(date.getDate()).toString() + " " + this.AddLeadingZeroes(date.getHours()).toString()
        + ":" + this.AddLeadingZeroes(date.getMinutes()).toString() + ":" +
        this.AddTrailingZeroesToSeconds(date.getSeconds()).toString();
    })
  }

  fixMonth(month: number) {
    month += 1;
    if (month <= 9) return "0" + month;
    return month;
  }

  AddLeadingZeroes(date: number) {
    if (date <= 9) return "0" + date;
    return date;
  }

  AddTrailingZeroesToSeconds(seconds: number) {
    if (seconds <= 9) return "0" + seconds;
    return seconds;
  }

  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  }

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
    this.basketsService.fetchAll().then(
      jsonData => {
        log.debug('jsonData', jsonData);
        this.baskets = jsonData;
      }
    ).then(() => {
      return this.basketStatusesService.fetchAll();
    }).then(jsonData => {
      log.debug('jsonData', jsonData);
      this.basketStatuses = jsonData;
    }).then(() => {
      this.baskets.forEach(basket => {
        basket.basketStatus = this.basketStatuses.find(status => status.id == basket.basketStatusId)
          .statusName.toString();
      })
    }).then(() => {
      this.baskets.forEach(() => {
        this.generateDateTimeString();
      })
    })
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
