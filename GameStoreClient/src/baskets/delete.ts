import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {IBasket} from "../interfaces/IBasket";
import {BasketsService} from "../services/baskets-service";

export var log = LogManager.getLogger('Baskets.Delete');

@autoinject
export class Delete {

  private basket: IBasket;

  constructor(
    private router: Router,
    private basketsService: BasketsService
  ) {
    log.debug('constructor');
  }

  generateDateTimeString() {

    let date = new Date(this.basket.created);
    this.basket.timeString = date.getFullYear().toString() + "/" + this.fixMonth(date.getMonth()).toString() + "/" + 
      this.AddLeadingZeroes(date.getDate()).toString() + " " + this.AddLeadingZeroes(date.getHours()).toString()
      + ":" + this.AddLeadingZeroes(date.getMinutes()).toString() + ":" +
      this.AddTrailingZeroesToSeconds(date.getSeconds()).toString();

  }

  fixMonth(month: number) {
    month += 1;
    if (month <= 9) return "0" + month;
    return month;
  }

  AddLeadingZeroes(date: number) {
    if (date <= 9) return "0" + date;
    return date;
  }

  AddTrailingZeroesToSeconds(seconds: number) {
    if (seconds <= 9) return "0" + seconds;
    return seconds;
  }

  submit(): void {
    this.basketsService.delete(this.basket.id).then(response => {
      if (response.status == 204) {
        this.router.navigateToRoute("basketsIndex");
      } else {
        log.debug('response', response);
      }
    })
  }

  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
    this.basketsService.fetch(params.id).then(
      basket => {
        log.debug('baskets', basket);
        this.basket = basket;
        this.generateDateTimeString();
      }
    );
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
