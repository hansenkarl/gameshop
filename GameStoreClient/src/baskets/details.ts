import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {IBasketDetails} from "../interfaces/IBasketDetails";
import {BasketDetailsService} from "../services/basket-details-service";

export var log = LogManager.getLogger('Baskets.Details');

@autoinject
export class Details {
  private basketDetails: IBasketDetails;

  constructor(
    private router: Router,
    private basketDetailsService: BasketDetailsService
  ) {
    log.debug('constructor');
  }

  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
    this.basketDetailsService.fetch(params.id).then(basketDetails => {
      log.debug('basketDetails', basketDetails);
      this.basketDetails = basketDetails;
    })
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
