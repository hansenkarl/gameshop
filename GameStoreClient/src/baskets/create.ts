import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {IBasket} from "../interfaces/IBasket";
import {BasketsService} from "../services/baskets-service";

export var log = LogManager.getLogger('Baskets.Create');

@autoinject
export class Create {

  private basket = <IBasket>{};

  constructor(
    private router: Router,
    private basketsService: BasketsService
  ) {
    log.debug('constructor');
  }

  // View methods
  submit(): void {
    log.debug('basket', this.basket);
    this.basketsService.post(this.basket).then(
      response => {
        if (response.status == 201) {
          this.router.navigateToRoute("basketsIndex");
        } else {
          log.error('Error in response!', response);
        }
      }
    );
  }

  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
