import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {IGenre} from "../interfaces/IGenre";
import {GenresService} from "../services/genres-service";

export var log = LogManager.getLogger('Genres.Details');

@autoinject
export class Details {

  private genre: IGenre;

  constructor(
    private router: Router,
    private genresService: GenresService
  ) {
    log.debug('constructor');
  }

  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
    this.genresService.fetch(params.id).then(genre => {
      log.debug('genre', genre);
      this.genre = genre;
    })
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
