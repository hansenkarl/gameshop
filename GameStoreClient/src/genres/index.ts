import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction} from "aurelia-router";
import {IGenre} from "../interfaces/IGenre";
import {GenresService} from "../services/genres-service";
import {IAppRole} from "../interfaces/IAppRole";
import {AppRolesService} from "../services/app-roles-service";

export var log = LogManager.getLogger('Genres.Index');

@autoinject
export class Index {

  private genres: IGenre[] = [];
  private isAdmin: IAppRole[] = [];
  
  constructor(
    private genresService: GenresService,
    private appRolesService: AppRolesService
  ) {
    log.debug('constructor');
  }

  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
    this.appRolesService.fetchAll().then(jsonData => {
      log.debug('jsonData', jsonData);
      this.isAdmin = jsonData;
    });
    this.genresService.fetchAll().then(
      jsonData => {
        log.debug('jsonData', jsonData);
        this.genres = jsonData;
      }
    );
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
