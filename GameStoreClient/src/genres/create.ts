import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {IGenre} from "../interfaces/IGenre";
import {GenresService} from "../services/genres-service";

export var log = LogManager.getLogger('Genres.Create');

@autoinject
export class Create {

  private genre: IGenre;

  constructor(
    private router: Router,
    private genresService: GenresService
  ) {
    log.debug('constructor');
  }

  submit(): void {
    log.debug('genre', this.genre);
    if (this.genre == undefined) {
      alert("Please fill in the genre's name field.");
      return;
    }

    if (this.genre.genreName.length < 2) {
      alert("Genre's name must be at least 2 characters.")
      return;
    }

    if (this.genre.genreName.length > 64) {
      alert("Genre's name must be less than 65 characters.")
      return;
    }
    
    this.genresService.post(this.genre).then(
      response => {
        if (response.status == 201) {
          this.router.navigateToRoute("genresIndex");
        } else {
          log.error('Error in response!', response);
        }
      }
    );
  }

  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
