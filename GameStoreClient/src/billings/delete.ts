import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {IBilling} from "../interfaces/IBilling";
import {BillingsService} from "../services/billings-service";
export var log = LogManager.getLogger('Billings.Delete');

@autoinject
export class Delete {

  private billing: IBilling;

  constructor(
    private router: Router,
    private billingsService: BillingsService) {
    log.debug('constructor');
  }

  submit(): void {
    this.billingsService.delete(this.billing.id).then(response => {
      if (response.status == 204) {
        this.router.navigateToRoute("billingsIndex");
      } else {
        log.debug('response', response);
      }
    })
  }
  
  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
    this.billingsService.fetch(params.id).then(
      billing => {
        log.debug('billing', billing);
        this.billing = billing;
      }
    );
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
