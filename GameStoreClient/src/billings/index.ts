import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction} from "aurelia-router";
import {IBilling} from "../interfaces/IBilling";
import {BillingsService} from "../services/billings-service";
import {BillingStatusesService} from "../services/billing-statuses-service";
import {CreditCardsService} from "../services/credit-cards-service";
import {IBillingStatus} from "../interfaces/IBillingStatus";
import {ICreditCard} from "../interfaces/ICreditCard";
import DateTimeFormat = Intl.DateTimeFormat;
import DateTimeFormatOptions = Intl.DateTimeFormatOptions;

export var log = LogManager.getLogger('Billings.Index');

@autoinject
export class Index {

  private billings: IBilling[] = [];
  private billingStatuses: IBillingStatus[] = [];
  private creditCards: ICreditCard[] = [];


  constructor(
    private billingsService: BillingsService,
    private billingStatusService: BillingStatusesService,
    private creditCardsService: CreditCardsService
  ) {
    log.debug('constructor');
  }

  generateDateTimeString() {
    this.billings.forEach(billings => {
      console.log(billings.created);
      let date = new Date(billings.created);
      billings.timeString = date.getFullYear().toString() + "/" + this.fixMonth(date.getMonth()).toString() + "/" +
        this.AddLeadingZeroes(date.getDate()).toString() + " " + this.AddLeadingZeroes(date.getHours()).toString()
        + ":" + this.AddLeadingZeroes(date.getMinutes()).toString() + ":" + this.AddTrailingZeroesToSeconds(date.getSeconds()).toString();
    })
  }

  fixMonth(month: number) {
    month += 1;
    if (month <= 9) return "0" + month;
    return month;
  }

  AddLeadingZeroes(date: number) {
    if (date <= 9) return "0" + date;
    return date;
  }

  AddTrailingZeroesToSeconds(seconds: number) {
    if (seconds <= 9) return "0" + seconds;
    return seconds;
  }

  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
    this.billingsService.fetchAll().then(
      jsonData => {
        log.debug('jsonData', jsonData);
        this.billings = jsonData;
      }
    ).then(() => {
      return this.billingStatusService.fetchAll();
    }).then(jsonData => {
      log.debug('jsonData', jsonData);
      this.billingStatuses = jsonData;
    }).then(() => {
      return this.creditCardsService.fetchAll();
    }).then(jsonData => {
      log.debug('jsonData', jsonData);
      this.creditCards = jsonData;
    }).then(() => {
      this.billings.forEach(billing => {
        billing.billingStatus = this.billingStatuses.find(status => status.id == billing.billingStatusId).statusName.toString();
      })
    }).then(() => {
      this.billings.forEach(billing => {
        let cc = this.creditCards.find(card => card.id == billing.creditCardId);
        if (cc == null) cc = {id: 1, creditCardNr: "-", timeString: "", validThru: new Date(), appUserId: 1};
        else billing.creditCard = cc.creditCardNr.toString();
      })
    }).then(() => {
      this.billings.forEach(() => {
        this.generateDateTimeString()
      })
    })
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
