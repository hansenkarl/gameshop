import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {IBilling} from "../interfaces/IBilling";
import {BillingsService} from "../services/billings-service";
import {CreditCardsService} from "../services/credit-cards-service";
import {ICreditCard} from "../interfaces/ICreditCard";

export var log = LogManager.getLogger('Billings.Edit');

@autoinject
export class Edit {

  private billing: IBilling;
  private creditCards: ICreditCard[] = [];
  private creditCard: ICreditCard;

  constructor(
    private router: Router,
    private billingsService: BillingsService,
    private creditCardsService: CreditCardsService) {
    log.debug('constructor');
  }

  // View methods
  submit(): void {
    log.debug('billing', this.billing);
    if (this.creditCard == undefined || this.creditCard.id == undefined || <any>this.creditCard.id == "") {
      alert('Please select a valid credit card!');
      return;
    }
    
    // dummy values
    this.billing.sumWithoutTax = 0.00;
    this.billing.tax = 0.20;
    this.billing.sumTotal = 0.00;
    this.billing.creditCardId = this.creditCard.id;
    this.billingsService.put(this.billing!).then(
      response => {
        if (response.status == 204) {
          this.router.navigateToRoute("billingsIndex");
        } else {
          log.error('Error in response!', response);
        }
      }
    );
  }

  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
    this.billingsService.fetch(params.id).then(billing => {
      log.debug('billing', billing);
      this.billing = billing;
    });
    this.creditCardsService.fetchAll().then(
      jsonData => {
        log.debug('jsonData', jsonData);
        this.creditCards = jsonData;
      }
    );
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
