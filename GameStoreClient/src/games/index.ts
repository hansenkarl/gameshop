import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {IGame} from "../interfaces/IGame";
import {GamesService} from "../services/games-service";
import {IGenre} from "../interfaces/IGenre";
import {GenresService} from "../services/genres-service";
import {IPlatform} from "../interfaces/IPlatform";
import {IPublisher} from "../interfaces/IPublisher";
import {PublishersService} from "../services/publishers-service";
import {PlatformsService} from "../services/platforms-service";
import {IAppRole} from "../interfaces/IAppRole";
import {AppRolesService} from "../services/app-roles-service";
import {IGameInBasket} from "../interfaces/IGameInBasket";
import {IGameInWarehouse} from "../interfaces/IGameInWarehouse";
import {GamesInBasketsService} from "../services/games-in-baskets-service";
import {GamesInWarehousesService} from "../services/games-in-warehouses-service";

export var log = LogManager.getLogger('Games.Index');

@autoinject
export class Index {

  private games: IGame[] = [];
  private genres: IGenre[] = [];
  private platforms: IPlatform[] = [];
  private publishers: IPublisher[] = [];
  private isAdmin: IAppRole[] = [];
  private gamesInBasket: IGameInBasket[] = [];
  private gamesInWarehouses: IGameInWarehouse[] = [];
  
  constructor(
    private gamesService: GamesService,
    private genresService: GenresService,
    private platformsService: PlatformsService,
    private publishersService: PublishersService,
    private appRolesService: AppRolesService,
    private gamesInBasketsService: GamesInBasketsService,
    private gamesInWarehousesService: GamesInWarehousesService
  ) {
    log.debug('constructor');
  }

  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
    this.appRolesService.fetchAll().then(jsonData => {
      log.debug('jsonData', jsonData);
      this.isAdmin = jsonData;
    });
    this.gamesService.fetchAll().then(
      jsonData => {
        log.debug('jsonData', jsonData);
        this.games = jsonData;
      }
    ).then(() => {
      return this.genresService.fetchAll()
    }).then(
      jsonData => {
        log.debug('jsonData', jsonData);
        this.genres = jsonData;
      }
    ).then(() => {
      return this.platformsService.fetchAll()
    }).then(
      jsonData => {
        log.debug('jsonData', jsonData);
        this.platforms = jsonData;
      }
    ).then(() => {
      return this.publishersService.fetchAll()
    }).then(
      jsonData => {
        log.debug('jsonData', jsonData);
        this.publishers = jsonData;
      }
    ).then(() => {
      return this.gamesInBasketsService.fetchAll()
    }).then(
      jsonData => {
        log.debug('jsonData');
        this.gamesInBasket = jsonData;
      }
    ).then(() => {
      return this.gamesInWarehousesService.fetchAll()
    }).then(
      jsonData => {
        log.debug('jsonData');
        this.gamesInWarehouses = jsonData;
      }
    ).then(() => {
      this.games.forEach(game => {
        game.genre = this.genres.find(genre => genre.id == game.genreId).genreName.toString();
        game.platform = this.platforms.find(platform => platform.id == game.platformId).platformName.toString();
        game.publisher = this.publishers.find(publisher => publisher.id == game.publisherId).publisherName.toString();
        game.inBasket = false;
        game.inWarehouse = false;
      })
    }).then(() => {
      this.gamesInWarehouses.forEach(gameInWarehouse => {
        this.games.find(game => game.id == gameInWarehouse.gameId).inWarehouse = true;
      })
    }).then(() => {
      this.gamesInBasket.forEach(gameInBasket => {
        this.games.find(game => game.id == gameInBasket.gameId).inBasket = true;
      })
    })

  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
