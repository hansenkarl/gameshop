import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {IGame} from "../interfaces/IGame";
import {GamesService} from "../services/games-service";
import {IGenre} from "../interfaces/IGenre";
import {IPlatform} from "../interfaces/IPlatform";
import {IPublisher} from "../interfaces/IPublisher";
import {GenresService} from "../services/genres-service";
import {PlatformsService} from "../services/platforms-service";
import {PublishersService} from "../services/publishers-service";

export var log = LogManager.getLogger('Games.Delete');

@autoinject
export class Delete {

  private game: IGame;
  private genre: IGenre;
  private platform: IPlatform;
  private publisher: IPublisher;

  constructor(
    private router: Router,
    private gamesService: GamesService,
    private genresService: GenresService,
    private platformsService: PlatformsService,
    private publishersService: PublishersService
  ) {
    log.debug('constructor');
  }

  submit(): void {
    this.gamesService.delete(this.game.id).then(response => {
      if (response.status == 204) {
        this.router.navigateToRoute("gamesIndex");
      } else {
        log.debug('response', response);
      }
    })
  }

  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
    this.gamesService.fetch(params.id).then(
      game => {
        log.debug('game', game);
        this.game = game;
      }
    ).then(() => {
      log.debug('activate');
      this.genresService.fetch(this.game.genreId).then(genre => {
        log.debug('genre', genre);
        this.game.genre = genre.genreName;
      });
      log.debug('activate');
      this.platformsService.fetch(this.game.platformId).then(platform => {
        log.debug('platform', platform);
        this.game.platform = platform.platformName;
      });
      log.debug('activate');
      this.publishersService.fetch(this.game.publisherId).then(publisher => {
        log.debug('publisher', publisher);
        this.game.publisher = publisher.publisherName;
      });
    });
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
