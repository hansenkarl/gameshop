import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {IGame} from "../interfaces/IGame";
import {GamesService} from "../services/games-service";
import {IGenre} from "../interfaces/IGenre";
import {IPlatform} from "../interfaces/IPlatform";
import {IPublisher} from "../interfaces/IPublisher";
import {GenresService} from "../services/genres-service";
import {PlatformsService} from "../services/platforms-service";
import {PublishersService} from "../services/publishers-service";

export var log = LogManager.getLogger('Games.Create');

@autoinject
export class Create {

  private game: IGame;
  private genre: IGenre;
  private platform: IPlatform;
  private publisher: IPublisher;
  private genres: IGenre[] = [];
  private platforms: IPlatform[] = [];
  private publishers: IPublisher[] = [];

  constructor(
    private router: Router,
    private gamesService: GamesService,
    private genresService: GenresService,
    private platformsService: PlatformsService,
    private publishersService: PublishersService
  ) {
    log.debug('constructor');
  }

  // View methods
  submit(): void {
    log.debug('game', this.game);
    if (this.game == undefined || this.game.gameTitle == undefined || this.game.price == undefined ||
    this.game.pegi == undefined || this.genre.id == undefined || this.platform.id == undefined ||
    this.publisher.id == undefined) {
      alert('Please fill in all the fields!');
      return;
    }
    
    if (this.game.gameTitle.length < 2 || this.game.gameTitle.length > 128) {
      alert('Game\'s title should be at least 2 characters long and should not be longer than 128 characters.');
      return;
    }
    
    if (this.game.price.length > 6){
      alert('We do not sell games more expensive than 999.99€');
      return;
    }
    
    if (this.game.pegi.length < 1 || this.game.pegi.length > 3) {
      alert('Smallest pegi rating can be 3 and biggest can be 18+ !');
      return;
    }
    
    this.game.genreId = this.genre.id;
    this.game.platformId = this.platform.id;
    this.game.publisherId = this.publisher.id;
    this.gamesService.post(this.game).then(
      response => {
        if (response.status == 201) {
          this.router.navigateToRoute("gamesIndex");
        } else {
          log.error('Error in response!', response);
        }
      }
    );
  }

  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
    this.genresService.fetchAll().then(
      jsonData => {
        log.debug('jsonData', jsonData);
        this.genres = jsonData;
      }
    );
    this.platformsService.fetchAll().then(
      jsonData => {
        log.debug('jsonData', jsonData);
        this.platforms = jsonData;
      }
    );
    this.publishersService.fetchAll().then(
      jsonData => {
        log.debug('jsonData', jsonData);
        this.publishers = jsonData;
      }
    );
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
