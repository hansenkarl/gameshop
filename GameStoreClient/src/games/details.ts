import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {IGame} from "../interfaces/IGame";
import {GamesService} from "../services/games-service";
import {GenresService} from "../services/genres-service";
import {PlatformsService} from "../services/platforms-service";
import {PublishersService} from "../services/publishers-service";
import {BasketsService} from "../services/baskets-service";
import {IBasket} from "../interfaces/IBasket";
import {IGameInBasket} from "../interfaces/IGameInBasket";
import {GamesInBasketsService} from "../services/games-in-baskets-service";

export var log = LogManager.getLogger('Games.Details');

@autoinject
export class Details {

  private game: IGame;
  private basketId: number;
  private amount: number;
  private baskets: IBasket[] = [];

  constructor(
    private router: Router,
    private gamesService: GamesService,
    private gamesInBasketsServices: GamesInBasketsService,
    private genresService: GenresService,
    private platformsService: PlatformsService,
    private publishersService: PublishersService,
    private basketsService: BasketsService
  ) {
    log.debug('constructor');
  }

  generateDateTimeString() {
    this.baskets.forEach(basket => {
      let date = new Date(basket.created);
      basket.timeString = date.getFullYear().toString() + "/" + this.fixMonth(date.getMonth()).toString() + "/" +
        this.AddLeadingZeroes(date.getDate()).toString() + " " + this.AddLeadingZeroes(date.getHours()).toString()
        + ":" + this.AddLeadingZeroes(date.getMinutes()).toString() + ":" +
        this.AddTrailingZeroesToSeconds(date.getSeconds()).toString();
    });
  }

  fixMonth(month: number) {
    month += 1;
    if (month <= 9) return "0" + month;
    return month;
  }

  AddLeadingZeroes(date: number) {
    if (date <= 9) return "0" + date;
    return date;
  }

  AddTrailingZeroesToSeconds(seconds: number) {
    if (seconds <= 9) return "0" + seconds;
    return seconds;
  }
  
  // View methods
  submit(): void {
    if (this.amount == undefined) {
      alert("Please fill in all the fields");
      return;
    }
    if (this.amount < 1) {
      alert("Amount must be at least 1!");
      return;
    }
    if (this.amount > 999) {
      alert("Amount must be smaller than 999!");
      return;
    }
    
    let gameinbasket: IGameInBasket = {
      id: undefined,
      amount: this.amount,
      gameId: this.game.id,
      basketId: this.basketId,
      timeString: ""
    };
    this.gamesInBasketsServices.post(gameinbasket).then(
      response => {
        if (response.status == 201) {
          this.router.navigateToRoute("basketsIndex");
        } else {
          log.error('Error in response!', response);
        }
      }
    );
  }

  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
    this.gamesService.fetch(params.id).then(game => {
      log.debug('game', game);
      this.game = game;
    }).then(() => {
      log.debug('activate');
      this.genresService.fetch(this.game.genreId).then(genre => {
        log.debug('genre', genre);
        this.game.genre = genre.genreName;
      });
      log.debug('activate');
      this.platformsService.fetch(this.game.platformId).then(platform => {
        log.debug('platform', platform);
        this.game.platform = platform.platformName;
      });
      log.debug('activate');
      this.publishersService.fetch(this.game.publisherId).then(publisher => {
        log.debug('publisher', publisher);
        this.game.publisher = publisher.publisherName;
      });
      this.basketsService.fetchAll().then(
        jsonData => {
          log.debug('jsonData', jsonData);
          this.baskets = jsonData;
          this.generateDateTimeString();
        }
      );
    });
  }

  canDeactivate() {
    log.debug('canDeactivate'); 
  }

  deactivate() {
    log.debug('deactivate');
  }
}
