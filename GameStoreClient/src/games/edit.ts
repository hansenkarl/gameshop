import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {IGame} from "../interfaces/IGame";
import {GamesService} from "../services/games-service";
import {IGenre} from "../interfaces/IGenre";
import {IPlatform} from "../interfaces/IPlatform";
import {IPublisher} from "../interfaces/IPublisher";
import {GenresService} from "../services/genres-service";
import {PlatformsService} from "../services/platforms-service";
import {PublishersService} from "../services/publishers-service";

export var log = LogManager.getLogger('Games.Edit');

@autoinject
export class Edit {

  private game: IGame;
  private genre: IGenre;
  private platform: IPlatform;
  private publisher: IPublisher;
  private genres: IGenre[] = [];
  private platforms: IPlatform[] = [];
  private publishers: IPublisher[] = [];

  constructor(
    private router: Router,
    private gamesService: GamesService,
    private genresService: GenresService,
    private platformsService: PlatformsService,
    private publishersService: PublishersService
  ) {
    log.debug('constructor');
  }

  // View methods
  submit(): void {
    log.debug('game', this.game);
    this.gamesService.put(this.game!).then(
      response => {
        if (response.status == 204) {
          this.router.navigateToRoute("gamesIndex");
        } else {
          log.error('Error in response!', response);
        }
      }
    );
  }

  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
    this.gamesService.fetch(params.id).then(game => {
        log.debug('game', game);
        this.game = game;
      }
    );
    this.genresService.fetchAll().then(
      jsonData => {
        log.debug('jsonData', jsonData);
        this.genres = jsonData;
      }
    );
    this.platformsService.fetchAll().then(
      jsonData => {
        log.debug('jsonData', jsonData);
        this.platforms = jsonData;
      }
    );
    this.publishersService.fetchAll().then(
      jsonData => {
        log.debug('jsonData', jsonData);
        this.publishers = jsonData;
      }
    );
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
