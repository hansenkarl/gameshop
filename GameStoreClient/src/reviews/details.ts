import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {IReview} from "../interfaces/IReview";
import {ReviewsService} from "../services/reviews-service";
import {IGame} from "../interfaces/IGame";
import {IPlatform} from "../interfaces/IPlatform";
import {IAppUser} from "../interfaces/IAppUser";
import {GamesService} from "../services/games-service";
import {PlatformsService} from "../services/platforms-service";
import {AppUsersService} from "../services/app-users-service";

export var log = LogManager.getLogger('Reviews.Details');

@autoinject
export class Details {

  private review: IReview;
  private game: IGame;
  private platform: IPlatform;
  private appuser: IAppUser;

  constructor(
    private router: Router,
    private reviewsService: ReviewsService,
    private gamesService: GamesService,
    private platformsService: PlatformsService,
    private appUsersService: AppUsersService,
  ) {
    log.debug('constructor');
  }

  generateDateTimeString() {
    let date = new Date(this.review.added);
    this.review.timeString = date.getFullYear().toString() + "/" + this.fixMonth(date.getMonth()).toString() + "/" +
      this.AddLeadingZeroes(date.getDate()).toString() + " " + this.AddLeadingZeroes(date.getHours()).toString()
      + ":" + this.AddLeadingZeroes(date.getMinutes()).toString() + ":" +
      this.AddTrailingZeroesToSeconds(date.getSeconds()).toString();

  }

  fixMonth(month: number) {
    month += 1;
    if (month <= 9) return "0" + month;
    return month;
  }

  AddLeadingZeroes(date: number) {
    if (date <= 9) return "0" + date;
    return date;
  }

  AddTrailingZeroesToSeconds(seconds: number) {
    if (seconds <= 9) return "0" + seconds;
    return seconds;
  }

  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
    this.reviewsService.fetch(params.id).then(review => {
      log.debug('review', review);
      this.review = review;
    }).then(() => {
      this.gamesService.fetch(this.review.gameId).then(game => {
        log.debug('game', game);
        this.review.game = game.gameTitle;
      })
    }).then(() => {
      this.platformsService.fetch(this.review.platformId).then(platform => {
        log.debug('platform', platform);
        this.review.platform = platform.platformName;
      })
    }).then(() => {
      this.appUsersService.fetch(this.review.appUserId).then(appUser => {
        log.debug('appUser', appUser);
        this.review.appuser = appUser.firstName;
      })
    }).then(() => {
      this.generateDateTimeString()
    })
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}

