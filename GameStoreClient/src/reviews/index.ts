import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {IReview} from "../interfaces/IReview";
import {ReviewsService} from "../services/reviews-service";
import {IGame} from "../interfaces/IGame";
import {IPlatform} from "../interfaces/IPlatform";
import {PlatformsService} from "../services/platforms-service";
import {GamesService} from "../services/games-service";
import {IAppUser} from "../interfaces/IAppUser";
import {AppUsersService} from "../services/app-users-service";
import {IAppRole} from "../interfaces/IAppRole";
import {AppRolesService} from "../services/app-roles-service";

export var log = LogManager.getLogger('Reviews.Index');

@autoinject
export class Index {

  private reviews: IReview[] = [];
  private games: IGame[] = [];
  private platforms: IPlatform[] = [];
  private appUsers: IAppUser[] = [];
  private isAdmin: IAppRole[] = [];
  

  constructor(
    private reviewsService: ReviewsService,
    private gamesService: GamesService,
    private platformsService: PlatformsService,
    private appUsersService: AppUsersService,
    private appRolesService: AppRolesService,
  ) {
    log.debug('constructor');
  }

  generateDateTimeString(){
    this.reviews.forEach(review => {
      console.log(review.added);
      let date = new Date(review.added);
      review.timeString = date.getFullYear().toString() + "/" + this.fixMonth(date.getMonth()).toString() + "/" +
        this.AddLeadingZeroes(date.getDate()).toString() + " " + this.AddLeadingZeroes(date.getHours()).toString()
        + ":" + this.AddLeadingZeroes(date.getMinutes()).toString() + ":" +
        this.AddTrailingZeroesToSeconds(date.getSeconds()).toString();
    })
  }

  fixMonth(month: number){
    month += 1;
    if (month <= 9) return "0" + month;
    return month;
  }

  AddLeadingZeroes(date: number){
    if (date <= 9) return "0" + date;
    return date;
  }

  AddTrailingZeroesToSeconds(seconds: number){
    if (seconds <= 9) return "0" + seconds;
    return seconds;
  }
  
  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  }

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
    this.reviewsService.fetchAll().then(
      jsonData => {
        log.debug('jsonData', jsonData);
        this.reviews = jsonData;
      }
    ).then(() => {
        return this.gamesService.fetchAll()
      }
    ).then(jsonData => {
        log.debug('jsonData', jsonData);
        this.games = jsonData;
      }
    ).then(() => {
        return this.platformsService.fetchAll()
    }).then(jsonData => {
        log.debug('jsonData', jsonData);
        this.platforms = jsonData;
      }
    ).then(() => {
      return this.appUsersService.fetchAll()
    }).then(jsonData => {
      log.debug('jsonData', jsonData);
      this.appUsers = jsonData;
    }).then(() => {
      return this.appRolesService.fetchAll()
    }).then(jsonData => {
      log.debug('jsonData', jsonData);
      this.isAdmin = jsonData;
    }).then(() => {
      this.reviews.forEach(review => {
        review.game = this.games.find(game => game.id == review.gameId).gameTitle.toString();
        review.platform = this.platforms.find(platform => platform.id == review.platformId).platformName.toString();
        review.appuser = this.appUsers.find(appUser => appUser.id == review.appUserId).firstName.toString();
      })
    }).then(() => {
      this.generateDateTimeString();
    })
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
