import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {IReview} from "../interfaces/IReview";
import {ReviewsService} from "../services/reviews-service";
import {IGame} from "../interfaces/IGame";
import {IPlatform} from "../interfaces/IPlatform";
import {GamesService} from "../services/games-service";
import {PlatformsService} from "../services/platforms-service";

export var log = LogManager.getLogger('Reviews.Edit');

@autoinject
export class Edit {

  private review: IReview;
  private games: IGame[] = [];
  private platforms: IPlatform[] = [];

  constructor(
    private router: Router,
    private reviewsService: ReviewsService,
    private gamesService: GamesService,
    private platformsService: PlatformsService
  ) {
    log.debug('constructor');
  }

  // View methods
  submit(): void {
    log.debug('review', this.review);

    if (this.review == undefined || this.review.rating == undefined || this.review.comment == undefined) {
      alert('Please fill in all the fields!');
      return;
    }

    if (this.review.rating < 0 || this.review.rating > 10) {
      alert('Please enter a rating between 0 - 10!');
      return;
    }

    if (this.review.comment.length < 2) {
      alert('Please enter a comment longer than 1 character!');
      return;
    }

    if (this.review.comment.length > 4096) {
      alert('Please enter a comment shorter than 4096 characters!')
    }
    
    let rev: any = {
      id: this.review.id,
      rating: parseInt(<any>this.review.rating),
      comment: this.review.comment,
      added: this.review.added,
      gameId: this.review.gameId,
      platformId: this.review.platformId
    };
    this.reviewsService.put(rev!).then(
      response => {
        if (response.status == 204) {
          this.router.navigateToRoute("reviewsIndex");
        } else {
          log.error('Error in response!', response);
        }
      }
    );
  }

  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
    this.reviewsService.fetch(params.id).then(review => {
        log.debug('review', review);
        this.review = review;
      }
    );
    this.gamesService.fetchAll().then(
      jsonData => {
        log.debug('jsonData', jsonData);
        this.games = jsonData;
      }
    );
    this.platformsService.fetchAll().then(
      jsonData => {
        log.debug('jsonData', jsonData);
        this.platforms = jsonData;
      }
    )
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
