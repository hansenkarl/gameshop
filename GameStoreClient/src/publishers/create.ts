import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {IPublisher} from "../interfaces/IPublisher";
import {PublishersService} from "../services/publishers-service";

export var log = LogManager.getLogger('Publishers.Create');

@autoinject
export class Create {

  private publisher: IPublisher;

  constructor(
    private router: Router,
    private publishersService: PublishersService
  ) {
    log.debug('constructor');
  }

  // View methods
  submit(): void {
    log.debug('publisher', this.publisher);
    
    if (this.publisher == undefined){
      alert("Please fill in the publisher name field!");
      return;
    }

    if (this.publisher.publisherName.length < 2){
      alert("Publisher's name must be at least 2 characters.");
      return;
    }

    if (this.publisher.publisherName.length > 128){
      alert("Publisher's name must not be longer than 128 characters.");
      return;
    }
      
    this.publishersService.post(this.publisher).then(
      response => {
        if (response.status == 201) {
          this.router.navigateToRoute("publishersIndex");
        } else {
          log.error('Error in response!', response);
        }
      }
    );
  }
  
  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
