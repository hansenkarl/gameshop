import {LogManager, autoinject} from "aurelia-framework";
import {HttpClient} from "aurelia-fetch-client";
import {AppConfig} from "../app-config";
import {BaseService} from "./base-service";
import {IGenre} from "../interfaces/IGenre";

export var log = LogManager.getLogger('GamesService');

@autoinject
export class GenresService extends BaseService<IGenre> {

  constructor(
    private httpClient: HttpClient,
    private appConfig: AppConfig
  ) {
    super(httpClient, appConfig, 'Genres');
  }
}
