import {LogManager, autoinject} from "aurelia-framework";
import {HttpClient} from "aurelia-fetch-client";
import {AppConfig} from "../app-config";
import {BaseService} from "./base-service";
import {IPlatform} from "../interfaces/IPlatform";

export var log = LogManager.getLogger('PlatformsService');

@autoinject
export class PlatformsService extends BaseService<IPlatform> {

  constructor(
    private httpClient: HttpClient,
    private appConfig: AppConfig
  ) {
    super(httpClient, appConfig, 'Platforms');
  }
}
