import {LogManager, autoinject} from "aurelia-framework";
import {HttpClient} from "aurelia-fetch-client";
import {IAddress} from "../interfaces/IAddress";
import {AppConfig} from "../app-config";
import {BaseService} from "./base-service";

export var log = LogManager.getLogger('AddressesService');

@autoinject
export class AddressesService extends BaseService<IAddress> {
  
  constructor(
    private httpClient: HttpClient,
    private appConfig: AppConfig
  ) {
    super(httpClient, appConfig, 'Addresses');
  }
}
