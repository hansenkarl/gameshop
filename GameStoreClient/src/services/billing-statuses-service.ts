import {LogManager, autoinject} from "aurelia-framework";
import {HttpClient} from "aurelia-fetch-client";
import {AppConfig} from "../app-config";
import {BaseService} from "./base-service";
import {IBillingStatus} from "../interfaces/IBillingStatus";

export var log = LogManager.getLogger('BillingStatusesService');

@autoinject
export class BillingStatusesService extends BaseService<IBillingStatus>{

  constructor(
    private httpClient: HttpClient,
    private appConfig: AppConfig
  ) {
    super(httpClient, appConfig, 'BillingStatuses');
  }
}
