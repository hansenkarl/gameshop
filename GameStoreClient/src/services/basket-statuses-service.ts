import {LogManager, autoinject} from "aurelia-framework";
import {HttpClient} from "aurelia-fetch-client";
import {AppConfig} from "../app-config";
import {BaseService} from "./base-service";
import {IBasketStatus} from "../interfaces/IBasketStatus";

export var log = LogManager.getLogger('BasketStatusesService');

@autoinject
export class BasketStatusesService extends BaseService<IBasketStatus>{

  constructor(
    private httpClient: HttpClient,
    private appConfig: AppConfig
  ) {
    super(httpClient, appConfig, 'BasketStatuses');
  }
}
