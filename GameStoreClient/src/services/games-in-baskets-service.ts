import {LogManager, autoinject} from "aurelia-framework";
import {HttpClient} from "aurelia-fetch-client";
import {AppConfig} from "../app-config";
import {BaseService} from "./base-service";
import {IGameInBasket} from "../interfaces/IGameInBasket";

export var log = LogManager.getLogger('GamesService');

@autoinject
export class GamesInBasketsService extends BaseService<IGameInBasket> {

  constructor(
    private httpClient: HttpClient,
    private appConfig: AppConfig
  ) {
    super(httpClient, appConfig, 'GamesInBaskets');
  }
}
