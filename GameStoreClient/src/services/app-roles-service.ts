import {LogManager, autoinject} from "aurelia-framework";
import {HttpClient} from "aurelia-fetch-client";
import {AppConfig} from "../app-config";
import {BaseService} from "./base-service";
import {IAppRole} from "../interfaces/IAppRole";

export var log = LogManager.getLogger('AppUsersService');

@autoinject
export class AppRolesService extends BaseService<IAppRole> {

  constructor(
    private httpClient: HttpClient,
    private appConfig: AppConfig,
  ) {
    super(httpClient, appConfig, 'AppRoles');
  }
}
