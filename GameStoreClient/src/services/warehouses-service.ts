import {LogManager, autoinject} from "aurelia-framework";
import {HttpClient} from "aurelia-fetch-client";
import {AppConfig} from "../app-config";
import {BaseService} from "./base-service";
import {IWarehouse} from "../interfaces/IWarehouse";

export var log = LogManager.getLogger('GamesService');

@autoinject
export class WarehousesService extends BaseService<IWarehouse> {

  constructor(
    private httpClient: HttpClient,
    private appConfig: AppConfig
  ) {
    super(httpClient, appConfig, 'Warehouses');
  }
}
