import {LogManager, autoinject} from "aurelia-framework";
import {HttpClient} from "aurelia-fetch-client";
import {AppConfig} from "../app-config";
import {BaseService} from "./base-service";
import {IBilling} from "../interfaces/IBilling";

export var log = LogManager.getLogger('BillingService');

@autoinject
export class BillingsService extends BaseService<IBilling>{

  constructor(
    private httpClient: HttpClient,
    private appConfig: AppConfig
  ) {
    super(httpClient, appConfig, 'Billings');
  }
}
