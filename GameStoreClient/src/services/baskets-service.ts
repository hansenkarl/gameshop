import {LogManager, autoinject} from "aurelia-framework";
import {HttpClient} from "aurelia-fetch-client";
import {AppConfig} from "../app-config";
import {BaseService} from "./base-service";
import {IBasket} from "../interfaces/IBasket";

export var log = LogManager.getLogger('BasketsService');

@autoinject
export class BasketsService extends BaseService<IBasket>{

  constructor(
    private httpClient: HttpClient,
    private appConfig: AppConfig
  ) {
    super(httpClient, appConfig, 'Baskets');
  }
}
