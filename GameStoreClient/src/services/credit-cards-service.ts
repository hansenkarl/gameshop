import {LogManager, autoinject} from "aurelia-framework";
import {HttpClient} from "aurelia-fetch-client";
import {AppConfig} from "../app-config";
import {BaseService} from "./base-service";
import {ICreditCard} from "../interfaces/ICreditCard";

export var log = LogManager.getLogger('CreditCardsService');

@autoinject
export class CreditCardsService extends BaseService<ICreditCard>{

  constructor(
    private httpClient: HttpClient,
    private appConfig: AppConfig
  ) {
    super(httpClient, appConfig, 'CreditCards');
  }
}
