import {LogManager, autoinject} from "aurelia-framework";
import {HttpClient} from "aurelia-fetch-client";
import {AppConfig} from "../app-config";
import {BaseService} from "./base-service";
import {IAppUser} from "../interfaces/IAppUser";

export var log = LogManager.getLogger('AppUsersService');

@autoinject
export class AppUsersService extends BaseService<IAppUser> {

  constructor(
    private httpClient: HttpClient,
    private appConfig: AppConfig,
  ) {
    super(httpClient, appConfig, 'AppUsers');
  }
}
