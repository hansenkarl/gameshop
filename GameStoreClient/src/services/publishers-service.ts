import {LogManager, autoinject} from "aurelia-framework";
import {HttpClient} from "aurelia-fetch-client";
import {AppConfig} from "../app-config";
import {BaseService} from "./base-service";
import {IPublisher} from "../interfaces/IPublisher";

export var log = LogManager.getLogger('GamesService');

@autoinject
export class PublishersService extends BaseService<IPublisher> {

  constructor(
    private httpClient: HttpClient,
    private appConfig: AppConfig
  ) {
    super(httpClient, appConfig, 'Publishers');
  }
}
