import {LogManager, autoinject} from "aurelia-framework";
import {HttpClient} from "aurelia-fetch-client";
import {AppConfig} from "../app-config";
import {BaseService} from "./base-service";
import {IGameInWarehouse} from "../interfaces/IGameInWarehouse";

export var log = LogManager.getLogger('GamesService');

@autoinject
export class GamesInWarehousesService extends BaseService<IGameInWarehouse> {

  constructor(
    private httpClient: HttpClient,
    private appConfig: AppConfig
  ) {
    super(httpClient, appConfig, 'GamesInWarehouses');
  }
}
