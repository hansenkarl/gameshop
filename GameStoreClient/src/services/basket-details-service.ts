import {LogManager, autoinject} from "aurelia-framework";
import {HttpClient} from "aurelia-fetch-client";
import {AppConfig} from "../app-config";
import {BaseService} from "./base-service";
import {IBasketDetails} from "../interfaces/IBasketDetails";

export var log = LogManager.getLogger('BasketDetailsService');

@autoinject
export class BasketDetailsService extends BaseService<IBasketDetails>{

  constructor(
    private httpClient: HttpClient,
    private appConfig: AppConfig
  ) {
    super(httpClient, appConfig, 'BasketDetails');
  }
}
