import {LogManager, autoinject} from "aurelia-framework";
import {HttpClient} from "aurelia-fetch-client";
import {IGame} from "../interfaces/IGame";
import {AppConfig} from "../app-config";
import {BaseService} from "./base-service";

export var log = LogManager.getLogger('GamesService');

@autoinject
export class GamesService extends BaseService<IGame> {

  constructor(
    private httpClient: HttpClient,
    private appConfig: AppConfig
  ) {
    super(httpClient, appConfig, 'Games');
  }
}
