import {LogManager, autoinject} from "aurelia-framework";
import {HttpClient} from "aurelia-fetch-client";
import {AppConfig} from "../app-config";
import {BaseService} from "./base-service";
import {IReview} from "../interfaces/IReview";

export var log = LogManager.getLogger('GamesService');

@autoinject
export class ReviewsService extends BaseService<IReview> {

  constructor(
    private httpClient: HttpClient,
    private appConfig: AppConfig
  ) {
    super(httpClient, appConfig, 'Reviews');
  }
}
