import {PLATFORM, LogManager, autoinject} from "aurelia-framework";
import {RouterConfiguration, Router} from "aurelia-router";
import {AppConfig} from "./app-config";

export var log = LogManager.getLogger('MainRouter');

@autoinject
export class MainRouter {

  public router: Router;

  constructor(private appConfig: AppConfig) {
    log.debug('constructor');
  }

  configureRouter(config: RouterConfiguration, router: Router): void {
    log.debug('configureRouter');
    this.router = router;
    config.title = "Gamestore";
    config.map(
      [
        {route: ['', 'home'], name: 'home', moduleId: PLATFORM.moduleName('home'), nav: true, title: 'Home'},

        {route: 'identity/login', name: 'identity' + 'Login', moduleId: PLATFORM.moduleName('identity/login'), nav: false, title: 'Login'},
        {route: 'identity/register', name: 'identity' + 'Register', moduleId: PLATFORM.moduleName('identity/register'), nav: false, title: 'Register'},
        {route: 'identity/logout', name: 'identity' + 'Logout', moduleId: PLATFORM.moduleName('identity/logout'), nav: false, title: 'Logout'},

        {route: ['addresses', 'addresses/index'], name: 'addresses' + 'Index', moduleId: PLATFORM.moduleName('addresses/index'), nav: true, title: 'Addresses'},
        {route: 'addresses/create', name: 'addresses' + 'Create', moduleId: PLATFORM.moduleName('addresses/create'), nav: false, title: 'Addresses Create'},
        {route: 'addresses/edit/:id', name: 'addresses' + 'Edit', moduleId: PLATFORM.moduleName('addresses/edit'), nav: false, title: 'Addresses Edit'},
        {route: 'addresses/details/:id', name: 'addresses' + 'Details', moduleId: PLATFORM.moduleName('addresses/details'), nav: false, title: 'Addresses Details'},
        {route: 'addresses/delete/:id', name: 'addresses' + 'Delete', moduleId: PLATFORM.moduleName('addresses/delete'), nav: false, title: 'Addresses Delete'},
        
        {route: ['baskets', 'baskets/index'], name: 'baskets' + 'Index', moduleId: PLATFORM.moduleName('baskets/index'), nav: true, title: 'Baskets'},
        {route: 'baskets/create', name: 'baskets' + 'Create', moduleId: PLATFORM.moduleName('baskets/create'), nav: false, title: 'Baskets Create'},
        {route: 'baskets/edit/:id', name: 'baskets' + 'Edit', moduleId: PLATFORM.moduleName('baskets/edit'), nav: false, title: 'Baskets Edit'},
        {route: 'basketDetails/:id', name: 'basket' + 'Details', moduleId: PLATFORM.moduleName('baskets/details'), nav: false, title: 'Baskets Details'},
        {route: 'baskets/delete/:id', name: 'baskets' + 'Delete', moduleId: PLATFORM.moduleName('baskets/delete'), nav: false, title: 'Baskets Delete'},

        {route: ['basketstatuses', 'basketstatuses/index'], name: 'basketstatuses' + 'Index', moduleId: PLATFORM.moduleName('basketstatuses/index'), nav: true, title: 'Basket statuses'},
        {route: 'basketstatuses/create', name: 'basketstatuses' + 'Create', moduleId: PLATFORM.moduleName('basketstatuses/create'), nav: false, title: 'Basket statuses Create'},
        {route: 'basketstatuses/edit/:id', name: 'basketstatuses' + 'Edit', moduleId: PLATFORM.moduleName('basketstatuses/edit'), nav: false, title: 'Basket statuses Edit'},
        {route: 'basketstatuses/details/:id', name: 'basketstatuses' + 'Details', moduleId: PLATFORM.moduleName('basketstatuses/details'), nav: false, title: 'Basket statuses Details'},
        {route: 'basketstatuses/delete/:id', name: 'basketstatuses' + 'Delete', moduleId: PLATFORM.moduleName('basketstatuses/delete'), nav: false, title: 'Basket statuses Delete'},
        
        {route: ['billings', 'billings/index'], name: 'billings' + 'Index', moduleId: PLATFORM.moduleName('billings/index'), nav: true, title: 'Billings'},
        {route: 'billings/create', name: 'billings' + 'Create', moduleId: PLATFORM.moduleName('billings/create'), nav: false, title: 'Billings Create'},
        {route: 'billings/edit/:id', name: 'billings' + 'Edit', moduleId: PLATFORM.moduleName('billings/edit'), nav: false, title: 'Billings Edit'},
        {route: 'billings/details/:id', name: 'billings' + 'Details', moduleId: PLATFORM.moduleName('billings/details'), nav: false, title: 'Billings Details'},
        {route: 'billings/delete/:id', name: 'billings' + 'Delete', moduleId: PLATFORM.moduleName('billings/delete'), nav: false, title: 'Billings Delete'},

        {route: ['billingstatuses', 'billingstatuses/index'], name: 'billingstatuses' + 'Index', moduleId: PLATFORM.moduleName('billingstatuses/index'), nav: true, title: 'Billing statuses'},
        {route: 'billingstatuses/create', name: 'billingstatuses' + 'Create', moduleId: PLATFORM.moduleName('billingstatuses/create'), nav: false, title: 'Billing statuses Create'},
        {route: 'billingstatuses/edit/:id', name: 'billingstatuses' + 'Edit', moduleId: PLATFORM.moduleName('billingstatuses/edit'), nav: false, title: 'Billing statuses Edit'},
        {route: 'billingstatuses/details/:id', name: 'billingstatuses' + 'Details', moduleId: PLATFORM.moduleName('billingstatuses/details'), nav: false, title: 'Billing statuses Details'},
        {route: 'billingstatuses/delete/:id', name: 'billingstatuses' + 'Delete', moduleId: PLATFORM.moduleName('billingstatuses/delete'), nav: false, title: 'Billing statuses Delete'},
        
        {route: ['creditcards', 'creditcards/index'], name: 'creditcards' + 'Index', moduleId: PLATFORM.moduleName('creditcards/index'), nav: true, title: 'Credit cards'},
        {route: 'creditcards/create', name: 'creditcards' + 'Create', moduleId: PLATFORM.moduleName('creditcards/create'), nav: false, title: 'Credit cards Create'},
        {route: 'creditcards/edit/:id', name: 'creditcards' + 'Edit', moduleId: PLATFORM.moduleName('creditcards/edit'), nav: false, title: 'Credit cards Edit'},
        {route: 'creditcards/details/:id', name: 'creditcards' + 'Details', moduleId: PLATFORM.moduleName('creditcards/details'), nav: false, title: 'Credit cards Details'},
        {route: 'creditcards/delete/:id', name: 'creditcards' + 'Delete', moduleId: PLATFORM.moduleName('creditcards/delete'), nav: false, title: 'Credit cards Delete'},

        {route: ['games', 'creditcards/index'], name: 'games' + 'Index', moduleId: PLATFORM.moduleName('games/index'), nav: true, title: 'Games'},
        {route: 'games/create', name: 'games' + 'Create', moduleId: PLATFORM.moduleName('games/create'), nav: false, title: 'Games Create'},
        {route: 'games/edit/:id', name: 'games' + 'Edit', moduleId: PLATFORM.moduleName('games/edit'), nav: false, title: 'Games Edit'},
        {route: 'games/details/:id', name: 'games' + 'Details', moduleId: PLATFORM.moduleName('games/details'), nav: false, title: 'Games Details'},
        {route: 'games/delete/:id', name: 'games' + 'Delete', moduleId: PLATFORM.moduleName('games/delete'), nav: false, title: 'Games Delete'},
        
        {route: ['gamesinbaskets', 'gamesinbaskets/index'], name: 'gamesinbaskets' + 'Index', moduleId: PLATFORM.moduleName('gamesinbaskets/index'), nav: true, title: 'Games in baskets'},
        {route: 'gamesinbaskets/create', name: 'gamesinbaskets' + 'Create', moduleId: PLATFORM.moduleName('gamesinbaskets/create'), nav: false, title: 'Games in baskets Create'},
        {route: 'gamesinbaskets/edit/:id', name: 'gamesinbaskets' + 'Edit', moduleId: PLATFORM.moduleName('gamesinbaskets/edit'), nav: false, title: 'Games in baskets Edit'},
        {route: 'gamesinbaskets/details/:id', name: 'gamesinbaskets' + 'Details', moduleId: PLATFORM.moduleName('gamesinbaskets/details'), nav: false, title: 'Games in baskets Details'},
        {route: 'gamesinbaskets/delete/:id', name: 'gamesinbaskets' + 'Delete', moduleId: PLATFORM.moduleName('gamesinbaskets/delete'), nav: false, title: 'Games in baskets Delete'},

        {route: ['gamesinwarehouses', 'gamesinwarehouses/index'], name: 'gamesinwarehouses' + 'Index', moduleId: PLATFORM.moduleName('gamesinwarehouses/index'), nav: true, title: 'Games in warehouses'},
        {route: 'gamesinwarehouses/create', name: 'gamesinwarehouses' + 'Create', moduleId: PLATFORM.moduleName('gamesinwarehouses/create'), nav: false, title: 'Games in warehouses Create'},
        {route: 'gamesinwarehouses/edit/:id', name: 'gamesinwarehouses' + 'Edit', moduleId: PLATFORM.moduleName('gamesinwarehouses/edit'), nav: false, title: 'Games in warehouses Edit'},
        {route: 'gamesinwarehouses/details/:id', name: 'gamesinwarehouses' + 'Details', moduleId: PLATFORM.moduleName('gamesinwarehouses/details'), nav: false, title: 'Games in warehouses Details'},
        {route: 'gamesinwarehouses/delete/:id', name: 'gamesinwarehouses' + 'Delete', moduleId: PLATFORM.moduleName('gamesinwarehouses/delete'), nav: false, title: 'Games in warehouses Delete'},

        {route: ['genres', 'genres/index'], name: 'genres' + 'Index', moduleId: PLATFORM.moduleName('genres/index'), nav: true, title: 'Genres'},
        {route: 'genres/create', name: 'genres' + 'Create', moduleId: PLATFORM.moduleName('genres/create'), nav: false, title: 'Genres Create'},
        {route: 'genres/edit/:id', name: 'genres' + 'Edit', moduleId: PLATFORM.moduleName('genres/edit'), nav: false, title: 'Genres Edit'},
        {route: 'genres/details/:id', name: 'genres' + 'Details', moduleId: PLATFORM.moduleName('genres/details'), nav: false, title: 'Genres Details'},
        {route: 'genres/delete/:id', name: 'genres' + 'Delete', moduleId: PLATFORM.moduleName('genres/delete'), nav: false, title: 'Genres Delete'},
        
        {route: ['platforms', 'platforms/index'], name: 'platforms' + 'Index', moduleId: PLATFORM.moduleName('platforms/index'), nav: true, title: 'Platforms'},
        {route: 'platforms/create', name: 'platforms' + 'Create', moduleId: PLATFORM.moduleName('platforms/create'), nav: false, title: 'Platforms Create'},
        {route: 'platforms/edit/:id', name: 'platforms' + 'Edit', moduleId: PLATFORM.moduleName('platforms/edit'), nav: false, title: 'Platforms Edit'},
        {route: 'platforms/details/:id', name: 'platforms' + 'Details', moduleId: PLATFORM.moduleName('platforms/details'), nav: false, title: 'Platforms Details'},
        {route: 'platforms/delete/:id', name: 'platforms' + 'Delete', moduleId: PLATFORM.moduleName('platforms/delete'), nav: false, title: 'Platforms Delete'},

        {route: ['publishers', 'publishers/index'], name: 'publishers' + 'Index', moduleId: PLATFORM.moduleName('publishers/index'), nav: true, title: 'Publishers'},
        {route: 'publishers/create', name: 'publishers' + 'Create', moduleId: PLATFORM.moduleName('publishers/create'), nav: false, title: 'Publishers Create'},
        {route: 'publishers/edit/:id', name: 'publishers' + 'Edit', moduleId: PLATFORM.moduleName('publishers/edit'), nav: false, title: 'Publishers Edit'},
        {route: 'publishers/details/:id', name: 'publishers' + 'Details', moduleId: PLATFORM.moduleName('publishers/details'), nav: false, title: 'Publishers Details'},
        {route: 'publishers/delete/:id', name: 'publishers' + 'Delete', moduleId: PLATFORM.moduleName('publishers/delete'), nav: false, title: 'Publishers Delete'},

        {route: ['reviews', 'reviews/index'], name: 'reviews' + 'Index', moduleId: PLATFORM.moduleName('reviews/index'), nav: true, title: 'Reviews'},
        {route: 'reviews/create', name: 'reviews' + 'Create', moduleId: PLATFORM.moduleName('reviews/create'), nav: false, title: 'Reviews Create'},
        {route: 'reviews/edit/:id', name: 'reviews' + 'Edit', moduleId: PLATFORM.moduleName('reviews/edit'), nav: false, title: 'Reviews Edit'},
        {route: 'reviews/details/:id', name: 'reviews' + 'Details', moduleId: PLATFORM.moduleName('reviews/details'), nav: false, title: 'Reviews Details'},
        {route: 'reviews/delete/:id', name: 'reviews' + 'Delete', moduleId: PLATFORM.moduleName('reviews/delete'), nav: false, title: 'Reviews Delete'},

        {route: ['warehouses', 'warehouses/index'], name: 'warehouses' + 'Index', moduleId: PLATFORM.moduleName('warehouses/index'), nav: true, title: 'Warehouses'},
        {route: 'warehouses/create', name: 'warehouses' + 'Create', moduleId: PLATFORM.moduleName('warehouses/create'), nav: false, title: 'Warehouses Create'},
        {route: 'warehouses/edit/:id', name: 'warehouses' + 'Edit', moduleId: PLATFORM.moduleName('warehouses/edit'), nav: false, title: 'Warehouses Edit'},
        {route: 'warehouses/details/:id', name: 'warehouses' + 'Details', moduleId: PLATFORM.moduleName('warehouses/details'), nav: false, title: 'Warehouses Details'},
        {route: 'warehouses/delete/:id', name: 'warehouses' + 'Delete', moduleId: PLATFORM.moduleName('warehouses/delete'), nav: false, title: 'Warehouses Delete'},
      ]
    );
  }
}
