import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {IdentityService} from "../services/identity-service";
import {AppConfig} from "../app-config";

export var log = LogManager.getLogger('Identity.Login');

// automatically inject dependencies declared as private constructor parameters
// can be accesed as this.<varname>
@autoinject
export class Login {
  
  //TODO:: clean that up, currently for easy testing
  private email: string = "khanse@ttu.ee";
  private password: string = "Adminkarl";
  
  constructor(
    private identityService: IdentityService,
    private appConfig: AppConfig,
    private router: Router
  ) {
    log.debug('constructor');
  }

  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  }

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
  
  // View methods
  submit():void{
    log.debug("submit", this.email, this.password);
    this.identityService.login(this.email, this.password)
      .then(jwtDTO => {
        if (jwtDTO.token !== undefined) {
          log.debug("submit token", jwtDTO.token);
          this.appConfig.jwt = jwtDTO.token;
          this.router.navigateToRoute('home');
        }
      });
  }
}
