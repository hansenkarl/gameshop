import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {IBasketStatus} from "../interfaces/IBasketStatus";
import {BasketStatusesService} from "../services/basket-statuses-service";

export var log = LogManager.getLogger('BasketStatuses.Create');

@autoinject
export class Create {

  private basketStatus: IBasketStatus;
  
  constructor(
    private router: Router,
    private basketStatusesService: BasketStatusesService
  ) {
    log.debug('constructor');
  }

  // View methods
  submit(): void {
    log.debug('basket', this.basketStatus);
    if (this.basketStatus == undefined ||this.basketStatus.statusName == undefined) {
      alert('Please fill in the status name field!');
      return;
    }
    if (this.basketStatus.statusName.length < 2 || this.basketStatus.statusName.length > 32) {
      alert('Status name must be at least 2 characters long and should not be longer than 32 characters.')
    } 
    this.basketStatusesService.post(this.basketStatus).then(
      response => {
        if (response.status == 201) {
          this.router.navigateToRoute("basketstatusesIndex");
        } else {
          log.error('Error in response!', response);
        }
      }
    );
  }
  
  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
