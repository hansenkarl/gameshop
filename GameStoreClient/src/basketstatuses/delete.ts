import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {IBasketStatus} from "../interfaces/IBasketStatus";
import {BasketStatusesService} from "../services/basket-statuses-service";

export var log = LogManager.getLogger('BasketStatuses.Delete');

@autoinject
export class Delete {

  private basketStatus: IBasketStatus;
  
  constructor(
    private router: Router,
    private basketStatusesService: BasketStatusesService
  ) {
    log.debug('constructor');
  }

  submit(): void {
    this.basketStatusesService.delete(this.basketStatus.id).then(response => {
      if (response.status == 204) {
        this.router.navigateToRoute("basketstatusesIndex");
      } else {
        log.debug('response', response);
      }
    })
  }
  
  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
    this.basketStatusesService.fetch(params.id).then(
      basketStatus => {
        log.debug('basketStatuses', basketStatus);
        this.basketStatus = basketStatus;
      }
    );
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
