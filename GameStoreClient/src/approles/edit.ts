import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {IAppRole} from "../interfaces/IAppRole";
import {AppRolesService} from "../services/app-roles-service";

export var log = LogManager.getLogger('AppRoles.Edit');

@autoinject
export class Edit {

  private appRole: IAppRole;

  constructor(
    private router: Router,
    private appRolesService: AppRolesService
  ) {
    log.debug('constructor');
  }

  // View methods
  submit(): void {
    log.debug('appUser', this.appRole);
    this.appRolesService.put(this.appRole!).then(
      response => {
        if (response.status == 204) {
          this.router.navigateToRoute("approlesIndex");
        } else {
          log.error('Error in response!', response);
        }
      }
    );
  }

  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
    this.appRolesService.fetch(params.id).then(appRoles => {
      log.debug('appRoles', appRoles);
      this.appRole = appRoles;
    })
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
