import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {IWarehouse} from "../interfaces/IWarehouse";
import {WarehousesService} from "../services/warehouses-service";

export var log = LogManager.getLogger('Warehouses.Details');

@autoinject
export class Details {

  private warehouse: IWarehouse;

  constructor(
    private router: Router,
    private warehousesService: WarehousesService
  ) {
    log.debug('constructor');
  }

  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
    this.warehousesService.fetch(params.id).then(warehouse => {
      log.debug('warehouse', warehouse);
      this.warehouse = warehouse;
    })
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
