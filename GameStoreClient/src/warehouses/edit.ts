import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {IWarehouse} from "../interfaces/IWarehouse";
import {WarehousesService} from "../services/warehouses-service";

export var log = LogManager.getLogger('Warehouses.Edit');

@autoinject
export class Edit {

  private warehouse: IWarehouse;

  constructor(
    private router: Router,
    private warehousesService: WarehousesService
  ) {
    log.debug('constructor');
  }

  // View methods
  submit(): void {
    
    if (this.warehouse == undefined || this.warehouse.warehouseName == undefined) {
      alert("Please fill in the warehouse's name!");
      return;
    }

    if (this.warehouse.warehouseName.length < 2) {
      alert("Warehouse name must be at least 2 characters!");
      return;
    }

    if (this.warehouse.warehouseName.length > 64) {
      alert("Warehouse name must be not be longer than 64 characters!");
      return;
    }


    log.debug('warehouse', this.warehouse);
    this.warehousesService.put(this.warehouse!).then(
      response => {
        if (response.status == 204) {
          this.router.navigateToRoute("warehousesIndex");
        } else {
          log.error('Error in response!', response);
        }
      }
    );
  }

  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
    this.warehousesService.fetch(params.id).then(warehouses => {
      log.debug('warehouse', warehouses);
      this.warehouse = warehouses;
    })
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}

