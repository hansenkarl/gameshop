import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {IAppUser} from "../interfaces/IAppUser";
import {AppUsersService} from "../services/app-users-service";

export var log = LogManager.getLogger('AppUsers.Edit');

@autoinject
export class Edit {

  private appUser: IAppUser;

  constructor(
    private router: Router,
    private appUsersService: AppUsersService
  ) {
    log.debug('constructor');
  }

  // View methods
  submit(): void {
    log.debug('appUser', this.appUser);
    this.appUsersService.put(this.appUser!).then(
      response => {
        if (response.status == 204) {
          this.router.navigateToRoute("appusersIndex");
        } else {
          log.error('Error in response!', response);
        }
      }
    );
  }

  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
    this.appUsersService.fetch(params.id).then(appUser => {
      log.debug('appUser', appUser);
      this.appUser = appUser;
    })
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
