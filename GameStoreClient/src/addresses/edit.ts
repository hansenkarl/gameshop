import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction, Router} from "aurelia-router";
import {IAddress} from "../interfaces/IAddress";
import {AddressesService} from "../services/addresses-service";

export var log = LogManager.getLogger('Addresses.Edit');

@autoinject
export class Edit {

  private address: IAddress;

  constructor(
    private router: Router,
    private addressesService: AddressesService
  ) {
    log.debug('constructor');
  }

  // View methods
  submit(): void {
    log.debug('address', this.address);
    if (this.address == undefined || this.address.country == undefined || this.address.county == undefined ||
      this.address.city == undefined || this.address.street == undefined || this.address.houseNr == undefined ||
      this.address.zipCode == undefined) {
      alert('Please fill in all the fields!');
      return;
    }
    if (this.address.country.length < 2 || this.address.country.length > 128) {
      alert('Country must be at least 2 characters long and should not be longer than 128 characters.');
      return;
    }
    if (this.address.county.length < 2 || this.address.county.length > 64) {
      alert('County must be at least 2 characters long and should not be longer than 64 characters.');
      return;
    }
    if (this.address.city.length < 2 || this.address.city.length > 64) {
      alert('City must be at least 2 characters long and should not be longer than 64 characters.');
      return;
    }
    if (this.address.street.length < 2 || this.address.street.length > 32) {
      alert('Street must be at least 2 characters long and should not be longer than 32 characters.');
      return;
    }
    if (this.address.houseNr.length < 1 || this.address.houseNr.length > 16) {
      alert('House number must be at least 1 character long and should not be longer than 16 characters.');
      return;
    }
    if (this.address.zipCode.length < 1 || this.address.zipCode.length > 8) {
      alert('Zip code must be at least 1 character long and should not be longer than 8 characters.');
      return;
    }
    this.addressesService.put(this.address!).then(
      response => {
        if (response.status == 204) {
          this.router.navigateToRoute("addressesIndex");
        } else {
          log.error('Error in response!', response);
        }
      }
    );
  }

  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  };

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
    this.addressesService.fetch(params.id).then(address => {
      log.debug('address', address);
      this.address = address;
    })
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
