import {LogManager, View, autoinject} from "aurelia-framework";
import {RouteConfig, NavigationInstruction} from "aurelia-router";
import {IAddress} from "../interfaces/IAddress";
import {AddressesService} from "../services/addresses-service";

export var log = LogManager.getLogger('Addresses.Index');

// automatically inject dependencies declared as private constructor parameters
// can be accesed as this.<varname>
@autoinject
export class Index {

  private addresses: IAddress[] = [];
  
  constructor(
    private addressesService: AddressesService
  ) {
    log.debug('constructor');
  }

  // View life cycle events
  created(owningView: View, myView: View) {
    log.debug('created');
  }

  bind(bindingContext: Object, overrideContext: Object) {
    log.debug('bind');
  }

  attached() {
    log.debug('attached');
    this.addressesService.fetchAll().then(
      jsonData => {
        log.debug('jsonData', jsonData);
        this.addresses = jsonData;
      }
    );
  }

  detached() {
    log.debug('detached');
  }

  unbind() {
    log.debug('unbind');
  }

  // Router events
  canActivate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('canActivate');
  }

  activate(params: any, routerConfig: RouteConfig, navigationInstruction: NavigationInstruction) {
    log.debug('activate');
  }

  canDeactivate() {
    log.debug('canDeactivate');
  }

  deactivate() {
    log.debug('deactivate');
  }
}
