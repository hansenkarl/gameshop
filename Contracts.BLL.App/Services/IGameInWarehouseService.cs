using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.App.DTO;
using Contracts.BLL.Base.Services;
using Contracts.DAL.App.Repositories;

namespace Contracts.BLL.App.Services
{
    public interface IGameInWarehouseService : IBaseEntityService<GameInWarehouse>,
        IGameInWarehouseRepository<GameInWarehouse>
    {
        new Task<List<GameInWarehouse>> AllAsync();
        new Task<GameInWarehouse> FindAsync(params object[] id);
    }
}