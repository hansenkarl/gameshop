using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.App.DTO;
using Contracts.BLL.Base.Services;
using Contracts.DAL.App.Repositories;

namespace Contracts.BLL.App.Services
{
    public interface IAddressService : IBaseEntityService<Address>, IAddressRepository<Address>
    {
        new Task<List<Address>> AllForUserAsync(int userId);
        new Task<Address> FindForUserAsync(int id, int userId);
        new Task<bool> BelongsToUserAsync(int id, int userId);
    }
}