using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.App.DTO;
using Contracts.BLL.Base.Services;
using Contracts.DAL.App.Repositories;

namespace Contracts.BLL.App.Services
{
    public interface IGenreService : IBaseEntityService<Genre>, IGenreRepository<Genre>
    {
        new Task<List<Genre>> AllAsync();
        new Task<Genre> FindAsync(params object[] id);
        new Task<Genre> GetGenre(int id);
    }
}