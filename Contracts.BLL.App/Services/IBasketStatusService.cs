using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.App.DTO;
using Contracts.BLL.Base.Services;
using Contracts.DAL.App.Repositories;

namespace Contracts.BLL.App.Services
{
    public interface IBasketStatusService : IBaseEntityService<BasketStatus>, IBasketStatusRepository<BasketStatus>
    {
        new Task<List<BasketStatus>> AllAsync();
        new Task<BasketStatus> FindAsync(params object[] id);
    }
}