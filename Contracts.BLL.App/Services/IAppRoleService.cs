using BLL.App.DTO.Identity;
using Contracts.BLL.Base.Services;
using Contracts.DAL.App.Repositories;

namespace Contracts.BLL.App.Services
{
    public interface IAppRoleService : IBaseEntityService<AppRole>, IAppRoleRepository<AppRole>
    {
        
    }
}