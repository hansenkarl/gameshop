using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.App.DTO;
using Contracts.BLL.Base.Services;
using Contracts.DAL.App.Repositories;

namespace Contracts.BLL.App.Services
{
    public interface IBillingService : IBaseEntityService<Billing>, IBillingRepository<Billing>
    {
        new Task<List<Billing>> AllForUserAsync(int userId);
        new Task<Billing> FindForUserAsync(int id, int userId);
        new Task<bool> BelongsToUserAsync(int id, int userId);
        new Task<Billing> GetBilling(int id);
        new Task<Billing> CreateBilling();
    }
}