using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.App.DTO;
using Contracts.BLL.Base.Services;
using Contracts.DAL.App.Repositories;

namespace Contracts.BLL.App.Services
{
    public interface IGameInBasketService : IBaseEntityService<GameInBasket>, IGameInBasketRepository<GameInBasket>
    {
        new Task<List<GameInBasket>> AllForUserAsync(int userId);
        new Task<GameInBasket> FindForUserAsync(int id, int userId);
        new Task<bool> BelongsToUserAsync(int id, int userId);
        new Task<List<GameInBasket>> AllForBasketAsync(int basketId);
    }
}