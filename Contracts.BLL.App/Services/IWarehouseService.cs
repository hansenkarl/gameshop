using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.App.DTO;
using Contracts.BLL.Base.Services;

namespace Contracts.BLL.App.Services
{
    public interface IWarehouseService : IBaseEntityService<Warehouse>
    {
        new Task<List<Warehouse>> AllAsync();
        new Task<Warehouse> FindAsync(params object[] id);
    }
}