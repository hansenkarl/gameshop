using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.App.DTO;
using Contracts.BLL.Base.Services;

namespace Contracts.BLL.App.Services
{
    public interface IPlatformService : IBaseEntityService<Platform>
    {
        new Task<List<Platform>> AllAsync();
        new Task<Platform> FindAsync(params object[] id);
        Task<Platform> GetPlatform(int id);
    }
}