using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.App.DTO;
using Contracts.BLL.Base.Services;

namespace Contracts.BLL.App.Services
{
    public interface ICreditCardService : IBaseEntityService<CreditCard>
    {
        Task<List<CreditCard>> AllForUserAsync(int userId);
        Task<CreditCard> FindForUserAsync(int id, int userId);
        Task<bool> BelongsToUserAsync(int id, int userId);
    }
}