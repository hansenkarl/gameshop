using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.App.DTO;
using Contracts.BLL.Base.Services;
using Contracts.DAL.App.Repositories;

namespace Contracts.BLL.App.Services
{
    public interface IBasketService : IBaseEntityService<Basket>, IBasketRepository<Basket>
    {
        new Task<List<Basket>> AllForUserAsync(int userId);
        new Task<Basket> FindForUserAsync(int id, int userId);
        new Task<bool> BelongsToUserAsync(int id, int userId);
        new Task<Basket> GetBasket(int id);

        new Task<Basket> CreateBasket();

        new Task<List<Basket>> AllNonCheckedOutUserAsync(List<Basket> baskets);
    }
}