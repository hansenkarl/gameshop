using System.Collections.Generic;
using System.Threading.Tasks;
using BLL.App.DTO;
using Contracts.BLL.Base.Services;

namespace Contracts.BLL.App.Services
{
    public interface IPublisherService : IBaseEntityService<Publisher>
    {
        new Task<List<Publisher>> AllAsync();
        new Task<Publisher> FindAsync(params object[] id);
        Task<Publisher> GetPublisher(int id);
    }
}