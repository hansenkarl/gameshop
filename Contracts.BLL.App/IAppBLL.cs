﻿using System;
using Contracts.BLL.App.Services;
using Contracts.BLL.Base;
using Contracts.DAL.App.Repositories;

namespace Contracts.BLL.App
{
    public interface IAppBLL : IBaseBLL
    {
        IAddressService Addresses { get; }
        IAppUserService AppUsers { get; }
        IAppRoleService AppRoles { get; }
        IBasketService Baskets { get; }
        IBasketStatusService BasketStatuses { get; }
        IBillingService Billings { get; }
        IBillingStatusService BillingStatuses { get; }
        ICreditCardService CreditCards { get; }
        IGameService Games { get; }
        IGameInBasketService GameInBaskets { get; }
        IGameInWarehouseService GameInWarehouses { get; }
        IGenreService Genres { get; }
        IPlatformService Platforms { get; }
        IPublisherService Publishers { get; }
        IReviewService Reviews { get; }
        IWarehouseService Warehouses { get; }
    }
}